#include "Gameplay/Card/card_descriptor.hpp"
#include "Gameplay/Card/deck_descriptor.hpp"
#include "Gameplay/Tiles/tile_descriptor.hpp"
#include "Gameplay/game_info_descriptor.hpp"
#include "app_status.hpp"
#include "data_loader.hpp"

#include "UI/Widget/base_widget.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/Widget/widget_renderer.hpp"
#include "UI/Widget/widget_updater.hpp"
#include "UI/interface_resources.hpp"
#include "UI/interface_resources_loader.hpp"
#include "UI/menu_data.hpp"
#include "UI/menu_loader.hpp"
#include "UI/widget_loader.hpp"

#include "UI/Controllers/card_display_controller.hpp"
#include "UI/Controllers/main_menu_controller.hpp"
#include "UI/Controllers/menu_flow_controller.hpp"
#include "UI/Controllers/menu_page.hpp"
#include "UI/Controllers/tile_placement_controller.hpp"

#include <nlc/fundamentals/report_error_to_user.hpp>

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/maths/simple_maths.hpp>
#include <nlc/maths/vec2.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

#include <mdf/compiler.hpp>
#include <mdf/types.hpp>

#include <graphic_engine/graphic_engine.hpp>
#ifndef NDEBUG
#    include <graphic_engine/renderer_2d.hpp>
#    include <graphic_engine/renderer_debug.hpp>
#endif
#include <win_input/window.hpp>

#include <win_input/axis_input.hpp>
#include <win_input/binding.hpp>
#include <win_input/coords2.hpp>
#include <win_input/graphical_report_error.hpp>
#include <win_input/input_set.hpp>
#include <win_input/inputs.hpp>
#ifndef NDEBUG
#    include <inputs_debug/inputs.hpp>
#endif

#include <glad/glad.h>
#ifndef NDEBUG
#    include <imgui.h>
#endif
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

static auto create_window(u32_2 const window_size) -> nlc::scoped<GLFWwindow *> {
    nlc::report_error_to_user(glfwInit() == GLFW_TRUE, "couldn't init render context");

    auto window = create_window_with_context(window_size.x, window_size.y, "PandemyBoard");
    nlc::report_error_to_user(*window != nullptr, "couldn't create window");

    ge::InitInfos const init_info { *window };
    ge::init(init_info);

    return window;
}

static auto data_load(nlc::allocator & alloc, nlc::string_stream & error_stream)
    -> nlc::optional<GameInfoDescriptor> {
    nlc::string_view const filenames[] = { "data/gameplay/hero_card.mdf",
                                           "data/gameplay/monster_card.mdf",
                                           "data/gameplay/deck.mdf",
                                           "data/gameplay/tiles.mdf",
                                           "data/common/tile_enum.mdf" };
    auto file_span = nlc::make_span(filenames);

    auto compil_ctx = mdf::read_and_compile(alloc, file_span);
    auto const & errors = compil_ctx.compilation_result.ctx().errors();
    if (errors.is_empty() == false) {
        nlc::log_entry log_entry {};
        mdf::log_errors(errors, log_entry);
        error_stream << log_entry.c_str();
        return (nlc::null);
    }

    constexpr char const deck_desc_name[] = { "Decks" };

    auto decks =
        load_deck_array(alloc, compil_ctx.compilation_result.root(), deck_desc_name, error_stream);

    if (decks == nlc::null) {
        error_stream << "No decks loaded\n";
        return (nlc::null);
    }

    constexpr char const tile_bag_name[] = { "TileBag" };

    if (compil_ctx.compilation_result.root().has(tile_bag_name) == false) {
        error_stream << "No Tile bag to load\n";
        return (nlc::null);
    }

    auto tile_bag = mdf::load_tile_bag(compil_ctx.compilation_result.root().get_rhs(tile_bag_name),
                                       alloc,
                                       error_stream);
    if (tile_bag == nlc::null) {
        error_stream << "can't find tile bag " << tile_bag_name << "\n";
        return (nlc::null);
    }

    return GameInfoDescriptor(alloc, decks.extract(), tile_bag.extract());
}

static auto load_and_validate_menu(
    nlc::allocator & alloc,
    nlc::string_view const filename,
    nlc::string_view const root_name,
    nlc::span<nlc::string_view const> const object_names,
    InterfaceResources const & interface_resources,
    nlc::function_view<bool(mdf::MenuData const &, nlc::string_stream &)> validator) -> mdf::MenuData {
    nlc::optional<mdf::MenuData> menu;
    nlc::string_stream error_stream(alloc);
    do {
        error_stream.clear();
        error_stream << "Fail to load main menu:\n";
        menu =
            mdf::menu_loader(alloc, filename, root_name, object_names, interface_resources, error_stream);
        bool const menu_validated = (menu != nlc::null) ? validator(*menu, error_stream) : false;
        if (menu_validated == false)
            menu = nlc::null;
    } while (nlc::report_retryable_error_to_user(menu != nlc::null, error_stream.c_str()) !=
             nlc::retryable_error_result::ok);
    return (menu.extract());
}

// This is the real core function of your program: it contains the initialization and the main
// loop
static auto main_function() -> AppStatus {
    nlc::standard_allocator allocator;
    AppStatus application_status = AppStatus::OK;

    nlc::set_report_error_callback(graphical_report_error_callback);

    // inputs
    Inputs::init(allocator);
    defer { Inputs::deinit(); };

    Inputs::set_binding("up", "W");
    Inputs::set_binding("down", "S");
    Inputs::set_binding("left", "A");
    Inputs::set_binding("right", "D");
    Inputs::set_binding("second_left", "Q");
    Inputs::set_binding("second_right", "E");
    Inputs::set_binding("click", Inputs::MouseButton::button_left);
    Inputs::set_binding("action", "ENTER");

    // window
    u32_2 window_size {
        1600u,
        900u,
    };
    f32 min_window_edge = static_cast<f32>(nlc::min(window_size.x, window_size.y));
    auto window = create_window(window_size);
    defer {
        ge::deinit();
        window = { nullptr, [](auto) {} };
        glfwTerminate();
    };
    if (*window == nullptr)
        return AppStatus::Error;

    ge::Camera camera { f32_2::cast(window_size) * 0.5f,
                        { (min_window_edge * 0.5f), -(min_window_edge * 0.5f) },
                        0.0f };

    // data loading
    nlc::optional<GameInfoDescriptor> game_info_descriptor;
    {
        nlc::string_stream error_stream(allocator);
        do {
            error_stream.clear();
            error_stream << "Fail to load game info:\n";
            game_info_descriptor = data_load(allocator, error_stream);
        } while (nlc::report_retryable_error_to_user(game_info_descriptor != nlc::null,
                                                     error_stream.c_str()) !=
                 nlc::retryable_error_result::ok);
    }

    nlc::optional<InterfaceResources> interface_resources;
    {
        nlc::string_stream error_stream(allocator);
        do {
            error_stream.clear();
            error_stream << "Fail to load interface resources:\n";
            interface_resources = mdf::interface_resources_load(allocator, error_stream);
        } while (nlc::report_retryable_error_to_user(interface_resources != nlc::null,
                                                     error_stream.c_str()) !=
                 nlc::retryable_error_result::ok);
    }

    nlc::function_view<bool(mdf::MenuData const &, nlc::string_stream &)> validator =
        [](auto const &, auto &) -> bool { return (true); };
#ifndef NDEBUG
    validator = &MainMenuController::Validate;
#endif
    mdf::MenuData main_menu = load_and_validate_menu(allocator,
                                                     "data/UI/Menu/MainMenu.mdf",
                                                     "MainMenu",
                                                     {},
                                                     *interface_resources,
                                                     validator);

#ifndef NDEBUG
    validator = &CardDisplayController::Validate;
#endif
    mdf::MenuData card_display_menu = load_and_validate_menu(allocator,
                                                             "data/UI/Menu/"
                                                             "CardDisplayMenu.mdf",
                                                             "CardDisplayMenu",
                                                             {},
                                                             *interface_resources,
                                                             validator);

#ifndef NDEBUG
    validator = &TilePlacementController::Validate;
#endif
    static constexpr nlc::string_view tile_dependencies[] = { "TileTemplate", "DraggedTileTemplate" };
    mdf::MenuData tile_placement_menu = load_and_validate_menu(allocator,
                                                               "data/UI/Menu/"
                                                               "TilePlacementMenu.mdf",
                                                               "TilePlacementMenu",
                                                               tile_dependencies,
                                                               *interface_resources,
                                                               validator);

    // Root UI component
    BaseWidgetDescriptor root_desc(allocator,
                                   "Root",
                                   nlc::aabb2::from_pos_and_size({ 0.0f, 0.0f },
                                                                 f32_2::cast(window_size)),
                                   BaseWidgetDescriptor::ScreenSpace::Absolute,
                                   BaseWidgetDescriptor::ScreenSpace::Absolute);
    root_desc.Components.append(nlc::make_unique<ContainerWidgetCompDescriptor>(allocator, allocator));
    BaseWidget root = root_desc.CreateInstance(allocator);

    ContainerWidgetComponent * root_container =
        static_cast<ContainerWidgetComponent *>(root.Components[0].get());

    nlc_assert_msg(root_container, "no root container");
    MenuFlowController flow_controller(allocator,
                                       *root_container,
                                       *game_info_descriptor,
                                       *interface_resources,
                                       application_status);
    MenuFlowController::MenuPageDescriptors menu_pages;

    for (auto i : nlc::range(0, static_cast<usize>(MenuPage::MAX))) {
        new (&menu_pages[i]) MenuFlowController::MenuPageInfo(allocator);
    }
    defer {
        for (auto i : nlc::range(0, static_cast<usize>(MenuPage::MAX))) {
            menu_pages[i].MenuFlowController::MenuPageInfo::~MenuPageInfo();
        }
    };

    {  // MainMenu
        menu_pages[static_cast<usize>(MenuPage::MainMenu)].MenuDescriptor = nlc::move(main_menu);
        menu_pages[static_cast<usize>(MenuPage::MainMenu)].ControllerInstanciator =
            &MakeMainMenuController;
    }
    {  // CardDisplay
        menu_pages[static_cast<usize>(MenuPage::CardDisplay)].MenuDescriptor =
            nlc::move(card_display_menu);
        menu_pages[static_cast<usize>(MenuPage::CardDisplay)].ControllerInstanciator =
            &MakeCardDisplayController;
    }
    {  // Tile Placement
        menu_pages[static_cast<usize>(MenuPage::TilePlacement)].MenuDescriptor =
            nlc::move(tile_placement_menu);
        menu_pages[static_cast<usize>(MenuPage::TilePlacement)].ControllerInstanciator =
            &MakeTilePlacementController;
    }

    flow_controller.Init(menu_pages);

    // Main loop
    auto prev_time = nlc::time::now();
#ifndef NDEBUG
    bool show_interface_debug = false;
    bool show_interface_with_debug = false;
#endif
    while (glfwWindowShouldClose(*window) == false && application_status == AppStatus::OK) {
        ge::begin_frame();
        defer { ge::end_frame(); };
#ifndef NDEBUG
        ImGui::Begin("Interface Debug");
        static f32_2 drag_pos;
        static f32_2 world_pos;
        static f32 circle_radius = 0.05f;
        ImGui::DragFloat2("ScreenPos", reinterpret_cast<float *>(&drag_pos), 5.0f);
        ImGui::DragFloat2("WorldPos", reinterpret_cast<float *>(&world_pos), 0.01f);
        ImGui::InputFloat("Debug circle radius", &circle_radius, 0.01f);
        WindowCoords2 cursor_pos = Inputs::get_cursor_position();
        ImGui::Text("Cursor position %f;%f",
                    static_cast<double>(rm_space(cursor_pos).x),
                    static_cast<double>(rm_space(cursor_pos).y));
        ImGui::Checkbox("Debug input", &activate_inputs_debug);
        ImGui::Checkbox("Debug render", &show_interface_debug);
        if (show_interface_debug) {
            ImGui::Checkbox("Render", &show_interface_with_debug);
        } else {
            show_interface_with_debug = true;
        }

        rdbg::draw_text(".", drag_pos, { 1.0f, 0.0f, 0.0f, 1.0f }, 50.0f, rtxt::TargetSpace::screenspace, 0);

        rdbg::draw_circle(world_pos, circle_radius, { 1.0f, 0.0f, 0.0f, 1.0f });
        display_inputs_debug();

        if (show_interface_debug)
            DebugRender(root);

        ImGui::End();
#endif

        auto const current_time = nlc::time::now();
        auto const dt = nlc::time::seconds(current_time - prev_time);
        prev_time = current_time;

        u32_2 new_window_size = Inputs::get_framebuffer_size(*window);
        // Resize
        if (new_window_size != window_size) {
            window_size = new_window_size;

            min_window_edge = static_cast<float>(nlc::min(window_size.x, window_size.y));

            root_desc.ScreenArea.max = f32_2::cast(window_size);

            camera = { f32_2::cast(window_size) * 0.5f,
                       { (min_window_edge * 0.5f), -(min_window_edge * 0.5f) },
                       0.0f };
        }

        // Inputs
        Inputs::update();

        // Controllers
        flow_controller.Update();

        // UI render
        UpdateLayout({ { 0.0f, 0.0f }, f32_2::cast(window_size) }, root);
#ifndef NDEBUG
        if (show_interface_with_debug)
#endif
            Render(root);

        // Render
        ge::set_view_layout(ge::ViewLayout::single, camera);
        ge::swap_buffer();
        ge::render(dt);

#ifndef NDEBUG
        if (show_interface_debug)
            DebugRender(root);
#endif
        glfwSwapBuffers(*window);

        // Handle events
        glfwPollEvents();
    }
    return (application_status == AppStatus::Exit) ? AppStatus::OK : application_status;
}

auto main() -> int {
    // Loop over `main_function` to ease hot reloading
    AppStatus res;
    do {
        res = main_function();
    } while (res == AppStatus::Restart);

    return static_cast<int>(res);
}
