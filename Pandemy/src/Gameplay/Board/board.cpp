#include "Gameplay/Board/board.hpp"

#include "Gameplay/Tiles/tile_descriptor.hpp"
#include "Gameplay/Tiles/tile_enum.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/maths/vec2_common.hpp>
#include <nlc/meta/basics.hpp>

static auto ConvertPositionToIndex(u8 const x, u8 const y, u8_2 const size) -> u8 {
    return (static_cast<u8>(y * size.x) + x);
}

Board::Board(nlc::allocator & allocator, u8_2 const size)
    : Tiles(nlc::vector<nlc::optional<TileInstance>>::create_and_reserve(allocator, size.x * size.y))
    , Size(size) {}

Board::Board(Board && rhs)
    : Tiles(nlc::move(rhs.Tiles))
    , Size(rhs.Size) {}

auto Board::operator=(Board && rhs) -> Board & {
    if (&rhs == this)
        return *this;

    Tiles = nlc::move(rhs.Tiles);
    Size = rhs.Size;
    return *this;
}

auto Board::operator()(u8 const x, u8 const y) const -> nlc::optional<TileInstance> const & {
    return Tiles[ConvertPositionToIndex(x, y, Size)];
}

auto Board::CanPlaceTile(TileInstance const & tile, u8_2 const position) const -> bool {
    nlc_assert_msg(position.x < Size.x && position.y < Size.y,
                   "Asking to pose a tile outside of the board.",
                   nlc_dump_var(position),
                   nlc_dump_var(Size));
    if (auto const & placed_tile = operator()(position); placed_tile != nlc::null) {
        return (false);
    }

    // testing left tile
    if (position.x > 0) {
        auto const & left_tile = operator()(position.x - 1, position.y);
        if (left_tile != nlc::null &&
            IsTileOpen(*left_tile, TileOpening::Right) != IsTileOpen(tile, TileOpening::Left)) {
            return (false);
        }
    }
    // testing right tile
    if (position.x < Size.x - 1) {
        auto const & right_tile = operator()(position.x + 1, position.y);
        if (right_tile != nlc::null &&
            IsTileOpen(*right_tile, TileOpening::Left) != IsTileOpen(tile, TileOpening::Right)) {
            return (false);
        }
    }
    // testing upper tile
    if (position.y > 0) {
        auto const & up_tile = operator()(position.x, position.y - 1);
        if (up_tile != nlc::null &&
            IsTileOpen(*up_tile, TileOpening::Down) != IsTileOpen(tile, TileOpening::Up)) {
            return (false);
        }
    }
    // testing down tile
    if (position.y < Size.y - 1) {
        auto const & down_tile = operator()(position.x, position.y + 1);
        if (down_tile != nlc::null &&
            IsTileOpen(*down_tile, TileOpening::Up) != IsTileOpen(tile, TileOpening::Down)) {
            return (false);
        }
    }
    return (true);
}

auto Board::PlaceTile(TileInstance const & tile, u8_2 const position) -> void {
    nlc_assert(CanPlaceTile(tile, position));

    Tiles[ConvertPositionToIndex(position.x, position.y, Size)].set(tile);
}

auto Board::RemoveTile(u8_2 const position) -> TileInstance {
    auto & tile = Tiles[ConvertPositionToIndex(position.x, position.y, Size)];
    nlc_assert(tile != nlc::null);
    return (tile.extract());
}
