#include "Gameplay/Tiles/tile_enum.hpp"

#include <nlc/fundamentals/string_stream.hpp>

auto IsOpeningSet(TileOpening const openings, TileOpening const flag) -> bool {
    nlc_assert(IsFlag(flag));
    return ((static_cast<u8>(openings) & static_cast<u8>(flag)) != 0);
}

auto IsFlag(TileOpening const opening) -> bool {
    u8 count = 0;
    if ((static_cast<u8>(opening) & static_cast<u8>(TileOpening::Up)) != 0) {
        ++count;
    }
    if ((static_cast<u8>(opening) & static_cast<u8>(TileOpening::Right)) != 0) {
        ++count;
    }
    if ((static_cast<u8>(opening) & static_cast<u8>(TileOpening::Down)) != 0) {
        ++count;
    }
    if ((static_cast<u8>(opening) & static_cast<u8>(TileOpening::Left)) != 0) {
        ++count;
    }
    return (count == 1);
}

auto OpeningToString(TileOpening const & opening, nlc::allocator & allocator) -> nlc::string {
    if (opening == TileOpening::None)
        return (nlc::string(allocator, "None"));
    if (opening == TileOpening::All)
        return (nlc::string(allocator, "All"));
    nlc::string_stream opening_stream(allocator);
    u8 opening_byte = static_cast<u8>(opening);
    if (opening_byte & static_cast<u8>(TileOpening::Up))
        opening_stream << "Up";
    if (opening_byte & static_cast<u8>(TileOpening::Right))
        opening_stream << "Right";
    if (opening_byte & static_cast<u8>(TileOpening::Down))
        opening_stream << "Down";
    if (opening_byte & static_cast<u8>(TileOpening::Left))
        opening_stream << "Left";
    return (nlc::string(allocator, opening_stream));
}
