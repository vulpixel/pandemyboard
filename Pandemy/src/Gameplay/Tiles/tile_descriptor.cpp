#include "Gameplay/Tiles/tile_descriptor.hpp"

#include "Gameplay/Tiles/tile_enum.hpp"

auto RotateTile(TileInstance & Tile, bool rotate_clockwise) -> void {
    u8 double_opening = static_cast<u8>(static_cast<u8>(Tile.CurrentOpenings) |
                                        static_cast<u8>(Tile.CurrentOpenings) << 4);

    if (rotate_clockwise)
        Tile.CurrentOpenings =
            static_cast<TileOpening>((double_opening >> 3) & static_cast<u8>(TileOpening::All));
    else
        Tile.CurrentOpenings =
            static_cast<TileOpening>((double_opening >> 1) & static_cast<u8>(TileOpening::All));
}

auto IsTileOpen(TileInstance const & tile, TileOpening const direction) -> bool {
    return (IsOpeningSet(tile.CurrentOpenings, direction));
}
