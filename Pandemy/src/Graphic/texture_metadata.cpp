#include "Graphic/texture_metadata.hpp"

#include <graphic_engine/graphic_engine.hpp>
#include <graphic_engine/texture.hpp>

#include <nlc/fundamentals/string_stream.hpp>

namespace Graphic {
TextureMetadata::TextureMetadata(nlc::allocator & alloc,
                                 nlc::string_view path,
                                 nlc::string_stream & error_stream) {
    nlc::optional<ge::Texture> texture = ge::Texture::create_from_file(alloc, path);

    if (texture == nlc::null) {
        error_stream << "Fail to load texture from file " << path << "\n";
        return;
    }
    TextureId = ge::load_texture(*texture);
    if (TextureId.is_invalid()) {
        error_stream << "Fail to send texture " << path << " to gpu\n";
        return;
    }
    AnimId = ge::load_animation_frame(TextureId,
                                      { 0.0f, 0.0f },
                                      { 1.0f, 1.0f },
                                      { 1.0f, -1.0f },
                                      ge::center_anchor);
    if (AnimId.is_invalid()) {
        error_stream << "Fail to create animation from texture " << path << "\n";
        ge::unload_texture(TextureId);
        TextureId = ge::TextureId::invalid();
        return;
    }
    TextureSize = f32_2::cast(texture->size());
    AspectRatio = TextureSize.x / TextureSize.y;
}

TextureMetadata::~TextureMetadata() {
    if (TextureId.is_valid())
        ge::unload_texture(TextureId);
}
}  // namespace Graphic
