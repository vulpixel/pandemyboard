#include "data_loader.hpp"

#include "Gameplay/Tiles/tile_descriptor.hpp"

#include "Graphic/texture_metadata.hpp"

#include <graphic_engine/graphic_engine.hpp>
#include <graphic_engine/ids.hpp>
#include <graphic_engine/texture.hpp>

#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/buffer.hpp>
#include <nlc/dialect/function_view.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>

#include <nlc/fundamentals/string_stream.hpp>

#include <mdf/context.hpp>
#include <mdf/error.hpp>
#include <mdf/types.hpp>

namespace mdf {

static auto load_character(mdf::Object const & character_def,
                           [[maybe_unused]] nlc::allocator & allocator,
                           nlc::string_stream & error_stream)
    -> nlc::optional<CardDescriptor::CardContentType> {
    constexpr nlc::string_view character_desc_attack = { "Attack" };
    constexpr nlc::string_view character_desc_defense = { "Defense" };

    if (character_def.has(character_desc_attack) == false ||
        character_def.get_rhs(character_desc_attack).is<mdf::Kind::integer>() == false) {
        error_stream << "Hero definition attack invalid\n";
        return (nlc::null);
    }
    if (character_def.has(character_desc_defense) == false ||
        character_def.get_rhs(character_desc_defense).is<mdf::Kind::integer>() == false) {
        error_stream << "Hero definition defense invalid\n";
        return (nlc::null);
    }
    i32 const attack = static_cast<i32>(character_def.get<i64>(character_desc_attack));
    i32 const defense = static_cast<i32>(character_def.get<i64>(character_desc_defense));
    return (CharacterDescriptor { attack, defense });
}

static auto load_ability(mdf::Object const & ability_def,
                         [[maybe_unused]] nlc::allocator & allocator,
                         nlc::string_stream & error_stream)
    -> nlc::optional<CardDescriptor::CardContentType> {
    (void)ability_def;
    error_stream << "Abilities are not supported yet.\n";
    return (nlc::null);
}

static auto load_item(mdf::Object const & item_def,
                      [[maybe_unused]] nlc::allocator & allocator,
                      nlc::string_stream & error_stream)
    -> nlc::optional<CardDescriptor::CardContentType> {
    (void)item_def;
    error_stream << "Items are not supported yet.\n";
    return (nlc::null);
}

static auto load_event(mdf::Object const & event_def,
                       [[maybe_unused]] nlc::allocator & allocator,
                       nlc::string_stream & error_stream)
    -> nlc::optional<CardDescriptor::CardContentType> {
    (void)event_def;
    error_stream << "Events are not supported yet.\n";
    return (nlc::null);
}

static auto load_guild_improvement(mdf::Object const & guild_improvement_def,
                                   [[maybe_unused]] nlc::allocator & allocator,
                                   nlc::string_stream & error_stream)
    -> nlc::optional<CardDescriptor::CardContentType> {
    (void)guild_improvement_def;
    error_stream << "Guild improvements are not supported yet.\n";
    return (nlc::null);
}

auto load_card(mdf::Object const & card_def,
               nlc::allocator & allocator,
               nlc::string_stream & error_stream) -> nlc::optional<CardDescriptor> {
    constexpr char const card_desc_name[] = { "Name" };
    constexpr char const card_desc_desc[] = { "Description" };
    constexpr char const card_desc_texture[] = { "Texture" };
    constexpr char const card_desc_cost[] = { "Cost" };
    constexpr char const warning_header[] = { "Card definition description invalid : " };

    if (card_def.has(card_desc_name) == false ||
        card_def.get_rhs(card_desc_name).is<mdf::Kind::string>() == false) {
        error_stream << warning_header << "no name\n";
        return (nlc::null);
    }

    nlc::string name = nlc::string(allocator, card_def.get<nlc::string_view>(card_desc_name));
    if (card_def.has(card_desc_desc) == false ||
        card_def.get_rhs(card_desc_desc).is<mdf::Kind::string>() == false) {
        error_stream << warning_header << name << " no description\n";
        return (nlc::null);
    }
    if (card_def.has(card_desc_texture) == false ||
        card_def.get_rhs(card_desc_texture).is<mdf::Kind::string>() == false) {
        error_stream << warning_header << name << " no texture\n";
        return (nlc::null);
    }
    if (card_def.has(card_desc_cost) &&
        card_def.get_rhs(card_desc_cost).is<mdf::Kind::integer>() == false) {
        error_stream << warning_header << name << " cost must be an integer.\n";
        return (nlc::null);
    }

    nlc::string desc = nlc::string(allocator, card_def.get<nlc::string_view>(card_desc_desc));
    i32 const cost = card_def.has(card_desc_cost) ? card_def.get<i32>(card_desc_cost) : 0;

    auto const texture_path = card_def.get<nlc::string_view>(card_desc_texture);
    Graphic::TextureMetadata texture { allocator, texture_path, error_stream };
    if (texture.IsValid() == false) {
        error_stream << " fail to texture data for " << texture_path << ".\n";
        return (nlc::null);
    }

    using ContentLoader =
        nlc::function_view<nlc::optional<CardDescriptor::CardContentType>(mdf::Object const &,
                                                                          nlc::allocator &,
                                                                          nlc::string_stream &)>;
    nlc::pair<char const *, ContentLoader> const card_type_to_loader[] = {
        { "Character", load_character },
        { "Ability", load_ability },
        { "Item", load_item },
        { "Event", load_event },
        { "GuildImprovement", load_guild_improvement },
    };

    nlc::optional<CardDescriptor::CardContentType> Content;

    for (auto & loader_pair : card_type_to_loader) {
        const nlc::string_view content_type = nlc::string_view::from_c_str(loader_pair.first);
        if (card_def.has(content_type) && card_def.get_rhs(content_type).is<mdf::Kind::object>()) {
            Content =
                loader_pair.second(card_def.get<mdf::Object>(content_type), allocator, error_stream);
            break;
        }
    }
    if (Content == nlc::null) {
        error_stream << warning_header << name << " no content.\n";
        return (nlc::null);
    }
    return (
        CardDescriptor { nlc::move(name), nlc::move(desc), nlc::move(texture), Content.extract(), cost });
}

static auto load_deck(mdf::Object const & deck_obj,
                      nlc::allocator & allocator,
                      nlc::string_stream & error_stream) -> nlc::optional<DeckDescriptor> {
    constexpr char const deck_desc_name[] = { "Name" };
    constexpr char const deck_desc_cards[] = { "Cards" };

    if (deck_obj.has(deck_desc_name) == false || deck_obj.has(deck_desc_cards) == false) {
        error_stream << "deck def invalid\n";
        return (nlc::null);
    }

    if (deck_obj.get_rhs(deck_desc_name).is<mdf::Kind::string>() == false) {
        error_stream << "deck def name not found or invalid\n";
        return (nlc::null);
    }

    nlc::string name = nlc::string(allocator, deck_obj.get<nlc::string_view>(deck_desc_name));

    if (deck_obj.get_rhs(deck_desc_cards).is<mdf::Kind::array>() == false) {
        error_stream << "deck def cards not found or invalid\n";
        return (nlc::null);
    }
    auto const cards_desc = deck_obj.get<mdf::Array>(deck_desc_cards).elements<mdf::Object>();
    nlc::vector<CardDescriptor> cards(allocator);

    cards.reserve(cards_desc.size());
    bool has_error = false;
    for (mdf::Object const & card_obj : cards_desc) {
        nlc::optional<CardDescriptor> card_desc = load_card(card_obj, allocator, error_stream);
        if (card_desc == nlc::null) {
            error_stream << "card loading failed\n";
            has_error = true;
            continue;
        }
        cards.append_no_grow(card_desc.extract());
    }

    if (has_error)
        return (nlc::null);
    return (DeckDescriptor { nlc::move(name), nlc::move(cards) });
}

auto load_deck_array(nlc::allocator & alloc,
                     mdf::Object const & root,
                     nlc::string_view const deck_name,
                     nlc::string_stream & error_stream) -> nlc::optional<nlc::vector<DeckDescriptor>> {
    if (root.has(deck_name) == false) {
        error_stream << "No deck " << deck_name << " to load\n";
        return (nlc::null);
    }
    auto const decks_desc = root.get<mdf::Array>(deck_name).elements<mdf::Object>();

    auto decks = nlc::vector<DeckDescriptor>::create_and_reserve(alloc, decks_desc.size());

    for (auto const & deck_desc : decks_desc) {
        nlc::optional<DeckDescriptor> deck = mdf::load_deck(deck_desc, alloc, error_stream);
        if (deck == nlc::null) {
            error_stream << "can't find deck " << deck_name << "\n";
            return (nlc::null);
        }
        decks.append_no_grow(deck.extract());
    }
    return (decks);
}

static auto load_tile(mdf::Object const & tile_def, nlc::string_stream & error_stream)
    -> nlc::optional<TileDescriptor> {
    constexpr char const tile_desc_openings[] = { "TypeOpening" };

    if (tile_def.has(tile_desc_openings) == false ||
        tile_def.get_rhs(tile_desc_openings).is<mdf::Kind::integer>() == false) {
        error_stream << "Tile definition openings invalid\n";
        return (nlc::null);
    }
    TileOpening openings = static_cast<TileOpening>(tile_def.get<u8>(tile_desc_openings));
    return (TileDescriptor { openings });
}

auto load_tile_bag(mdf::Rhs const & tile_bag_def,
                   nlc::allocator & allocator,
                   nlc::string_stream & error_stream) -> nlc::optional<nlc::vector<TileDescriptor>> {
    if (tile_bag_def.is<mdf::Kind::array>() == false) {
        error_stream << "Tile bag must be an array.\n";
        return (nlc::null);
    }
    mdf::Array::Proxy<mdf::Object> mdf_tiles =
        mdf::unwrap<mdf::Array>(tile_bag_def).elements<mdf::Object>();
    nlc::vector<TileDescriptor> tile_bag(allocator);

    tile_bag.reserve(mdf_tiles.size());

    for (auto const & mdf_tile : mdf_tiles) {
        auto tile = load_tile(mdf_tile, error_stream);
        if (tile != nlc::null)
            tile_bag.append_no_grow(tile.extract());
    }
    return (tile_bag);
}

}  // namespace mdf
