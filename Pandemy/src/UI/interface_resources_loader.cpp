#include "UI/interface_resources_loader.hpp"
#include "UI/interface_resources.hpp"

#include <nlc/algo/find.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/hash.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/meta/basics.hpp>

#include <mdf/compiler.hpp>
#include <mdf/context.hpp>
#include <mdf/error.hpp>
#include <mdf/types.hpp>

#include <graphic_engine/graphic_engine.hpp>
#include <graphic_engine/ids.hpp>
#include <graphic_engine/renderer_text.hpp>

static auto GetTextureFromCache(nlc::allocator & alloc,
                                InterfaceResources::TileTextureCache & cache,
                                nlc::string_view path,
                                nlc::string_stream & error_stream)
    -> nlc::pair<Graphic::TextureView, ge::TextureId> {
    nlc::hash_result hash = nlc::hash(path);
    auto const * result =
        nlc::find_if(nlc::make_span(cache),
                     [hash](nlc::pair<nlc::hash_result, Graphic::TextureMetadata> const & rhs) {
                         return (rhs.first == hash);
                     });
    if (result == nullptr) {
        result = &cache.append(hash, Graphic::TextureMetadata { alloc, path, error_stream });
    }
    return { Graphic::TextureView { result->second }, result->second.TextureId };
}

namespace mdf {
static auto font_load(nlc::allocator & allocator,
                      mdf::Object const & font_obj,
                      nlc::string_stream & error_stream) -> nlc::optional<Font> {
    static constexpr nlc::string_view name_key = "Name";
    static constexpr nlc::string_view path_key = "Path";
    static constexpr nlc::string_view warning_header = "Font definition invalid : ";

    if (font_obj.has(name_key) == false) {
        error_stream << warning_header << "no name.\n";
        return (nlc::null);
    }
    if (font_obj.get_rhs(name_key).is<mdf::Kind::string>() == false) {
        error_stream << warning_header << "name parameter must be a string.\n";
        return (nlc::null);
    }
    if (font_obj.has(path_key) == false) {
        error_stream << warning_header << "no path.\n";
        return (nlc::null);
    }
    if (font_obj.get_rhs(path_key).is<mdf::Kind::string>() == false) {
        error_stream << warning_header << "path parameter must be a string.\n";
        return (nlc::null);
    }

    ge::FontId const font_id = rtxt::load_font(allocator, font_obj.get<nlc::string_view>(path_key));
    if (font_id.is_invalid()) {
        error_stream << warning_header << "fail to load font located in "
                     << font_obj.get<nlc::string_view>(path_key) << "\n";
        return (nlc::null);
    }
    return (Font(allocator, font_obj.get<nlc::string_view>(name_key), font_id));
}

auto interface_resources_load(nlc::allocator & allocator, nlc::string_stream & error_stream)
    -> nlc::optional<InterfaceResources> {
    static constexpr nlc::string_view resource_file_name[] = { "data/UI/"
                                                               "InterfaceResources.mdf",
                                                               "data/common/"
                                                               "tile_enum.mdf" };
    auto compil_ctx = mdf::read_and_compile(allocator, nlc::make_span(resource_file_name));
    auto const & errors = compil_ctx.compilation_result.ctx().errors();
    if (errors.is_empty() == false) {
        nlc::log_entry log_entry {};
        mdf::log_errors(errors, log_entry);
        error_stream << log_entry.c_str();
        return (nlc::null);
    }

    constexpr char const resource_name[] = { "InterfaceResources" };

    if (compil_ctx.compilation_result.root().has(resource_name) == false) {
        error_stream << "No object named " << resource_name << " to load\n";
        return (nlc::null);
    }
    auto const & resource_obj_rhs = compil_ctx.compilation_result.root().get_rhs(resource_name);
    if (resource_obj_rhs.is<mdf::Kind::object>() == false) {
        error_stream << "Object definition invalid\n";
        return (nlc::null);
    }
    mdf::Object const & resource_obj = mdf::unwrap<mdf::Object>(resource_obj_rhs);

    constexpr char const warning_header[] = "MenuResources definition invalid : ";
    nlc::vector<Font> fonts(allocator);
    {
        constexpr char const fonts_name[] = "Fonts";

        if (resource_obj.has(fonts_name) == false) {
            error_stream << warning_header << "no fonts.\n";
            return (nlc::null);
        }
        auto const & fonts_rhs = resource_obj.get_rhs(fonts_name);

        if (fonts_rhs.is<mdf::Kind::array>() == false) {
            error_stream << warning_header << "fonts must be an array.\n";
            return (nlc::null);
        }

        auto const & elements = mdf::unwrap<mdf::Array>(fonts_rhs).elements<mdf::Object>();
        fonts.reserve(elements.size());
        for (auto const & element : elements) {
            auto font = font_load(allocator, element, error_stream);

            if (font == nlc::null) {
                continue;
            }
            fonts.append_no_grow(font.extract());
        }
    }

    if (fonts.is_empty()) {
        error_stream << warning_header << "no font loaded.\n";
        return (nlc::null);
    }

    usize default_index = 0u;
    {
        constexpr nlc::string_view const default_font_name = "DefaultFont";
        if (resource_obj.has(default_font_name) == false) {
            error_stream << warning_header << "no default font.\n";
            return (nlc::null);
        }
        if (resource_obj.get_rhs(default_font_name).is<mdf::Kind::string>() == false) {
            error_stream << warning_header << "default font parameter must be a string.\n";
            return (nlc::null);
        }
        nlc::string_view default_font = resource_obj.get<nlc::string_view>(default_font_name);

        bool has_succeed = false;
        for (usize i = 0, size = fonts.size(); i < size; ++i) {
            if (fonts[i].Name == default_font) {
                default_index = i;
                has_succeed = true;
                break;
            }
        }
        if (has_succeed == false) {
            error_stream << warning_header << "default font " << default_font << " not found\n";
        }
    }

    // tile texture
    InterfaceResources::TileTextureCache tile_textures_cache { allocator };
    InterfaceResources::TileTextureViews tile_textures_view;
    {
        bool has_error = false;
        constexpr nlc::string_view const tile_texture_name = "TileTextures";
        if (resource_obj.has(tile_texture_name) == false) {
            error_stream << warning_header << "no tile textures.\n";
            return (nlc::null);
        }
        auto const & tile_rhs = resource_obj.get_rhs(tile_texture_name);
        if (tile_rhs.is<mdf::Kind::map>() == false) {
            error_stream << warning_header
                         << "tile textures must be a map of TileOpening to Texture path.\n";
            return (nlc::null);
        }

        for (auto & tile_view : tile_textures_view) {
            tile_view.Texture.AnimId = ge::AnimationFrameId::invalid();
        }
        auto const & elements = mdf::unwrap<mdf::Map>(tile_rhs).elements<u8, mdf::Object>();

        constexpr nlc::string_view const tile_texture_path_name = "Texture";
        constexpr nlc::string_view const tile_texture_rotation_name = "Rotation";

        enum class TileRotation {
            _0_degrees = 0,
            _90_degrees = 1,
            _180_degrees = 2,
            _270_degrees = 3,
        };
        constexpr nlc::radians rotation_to_rads[] = { 0.0_rad,
                                                      nlc::half_pi<float>,
                                                      nlc::pi<float>,
                                                      -nlc::half_pi<float> };
        for (auto const [opening, texture_data] : elements) {
            if (texture_data.has(tile_texture_path_name) == false) {
                error_stream << warning_header << "no tile texture path in texture object.\n";
                has_error = true;
                continue;
            }
            if (texture_data.get_rhs(tile_texture_path_name).is<mdf::Kind::string>() == false) {
                error_stream << warning_header << "texture path must be a string.\n";
                has_error = true;
                continue;
            }
            if (texture_data.has(tile_texture_rotation_name) == false) {
                error_stream << warning_header << "no tile texture rotation in texture object.\n";
                has_error = true;
                continue;
            }
            if (texture_data.get_rhs(tile_texture_rotation_name).is<mdf::Kind::integer>() == false) {
                error_stream << warning_header << "texture rotation must be a value.\n";
                has_error = true;
                continue;
            }
            nlc::string_view texture_path = texture_data.get<nlc::string_view>(tile_texture_path_name);
            TileRotation texture_rotation =
                static_cast<TileRotation>(texture_data.get<u8>(tile_texture_rotation_name));

            auto [tile_view, texture_id] =
                GetTextureFromCache(allocator, tile_textures_cache, texture_path, error_stream);

            tile_view.AnimId = ge::load_animation_frame(texture_id,
                                                        { 0.0f, 0.0f },
                                                        { 1.0f, 1.0f },
                                                        { 1.0f, -1.0f },
                                                        ge::center_anchor);

            has_error |= (tile_view.IsValid() == false);

            tile_textures_view[opening].Texture = tile_view;
            tile_textures_view[opening].Rotation = rotation_to_rads[static_cast<u8>(texture_rotation)];
        }

        nlc_assert(tile_textures_view.size() > 0);
        if (tile_textures_view[0].Texture.IsValid()) {
            error_stream << warning_header
                         << "Tile texture opening for TileOpening::None is valid but "
                            "unnecessary.\n";
            has_error = true;
        }
        for (u8 i = 1; i < tile_textures_view.size(); ++i) {
            if (tile_textures_view[i].Texture.IsValid() == false) {
                error_stream << warning_header << "Tile texture opening for "
                             << OpeningToString(static_cast<TileOpening>(i), allocator)
                             << " is missing.\n";
                has_error = true;
            }
        }
        if (has_error) {
            return (nlc::null);
        }
    }
    return (InterfaceResources(nlc::move(fonts),
                               default_index,
                               nlc::move(tile_textures_cache),
                               nlc::move(tile_textures_view)));
}
}  // namespace mdf
