#include "UI/widget_loader.hpp"

#include "UI/Widget/base_widget.hpp"
#include "UI/Widget/base_widget_component.hpp"
#include "UI/Widget/button_widget.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/Widget/draggable_widget.hpp"
#include "UI/Widget/grid_widget.hpp"
#include "UI/Widget/text_widget.hpp"
#include "UI/Widget/texture_widget.hpp"

#include <nlc/fundamentals/report_error_to_user.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include "UI/interface_resources.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/expected.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/maths/vec4.hpp>
#include <nlc/meta/basics.hpp>

#include <mdf/context.hpp>
#include <mdf/error.hpp>
#include <mdf/types.hpp>

namespace mdf {
// fwd declare
auto load_widget_object(mdf::Object const & widget_obj,
                        nlc::allocator & allocator,
                        InterfaceResources const & interface_resources,
                        nlc::string_stream & error_stream) -> nlc::optional<BaseWidgetDescriptor>;

// helpers
template<mdf::Kind Kind, typename ReturnType, typename SubType>
static auto get_value_or_default(mdf::Object const & obj,
                                 nlc::string_view const key,
                                 ReturnType const & default_value) -> ReturnType {
    bool const has_value = obj.has(key) && obj.get_rhs(key).is<Kind>();
    return ((has_value) ? static_cast<ReturnType>(obj.get<SubType>(key)) : default_value);
}

// implementation
static auto load_button(mdf::Object const & button_obj,
                        nlc::allocator & allocator,
                        InterfaceResources const & interface_resources,
                        nlc::string_stream & error_stream)
    -> nlc::unique_ptr<BaseWidgetCompDescriptor> {
    constexpr char const warning_header[] = "ButtonComponent definition invalid : ";
    constexpr char const idle_name[] = "IdleWidget";
    constexpr char const selected_name[] = "SelectedWidget";
    constexpr char const pressed_name[] = "PressedWidget";
    constexpr char const disabled_name[] = "DisabledWidget";
    constexpr char const status_name[] = "DefaultStatus";

    if (button_obj.has(idle_name) == false ||
        button_obj.get_rhs(idle_name).is<mdf::Kind::object>() == false) {
        error_stream << warning_header << "no idle widget.\n";
        return (nullptr);
    }

    auto const & idle_obj = button_obj.get<mdf::Object>(idle_name);
    nlc::optional<BaseWidgetDescriptor> idle_desc =
        load_widget_object(idle_obj, allocator, interface_resources, error_stream);
    if (idle_desc == nlc::null) {
        error_stream << warning_header << "fail to load idle widget descriptor.\n";
        return (nullptr);
    }

    auto const widget_loader = [&warning_header,
                                &button_obj,
                                &allocator,
                                &idle_obj,
                                &interface_resources,
                                &error_stream](nlc::string_view const widget_name) {
        if (button_obj.has(widget_name) && button_obj.get_rhs(widget_name).is<mdf::Kind::object>()) {
            auto const & widget_obj = button_obj.get<mdf::Object>(widget_name);
            nlc::optional<BaseWidgetDescriptor> widget_desc =
                load_widget_object(widget_obj, allocator, interface_resources, error_stream);
            if (widget_desc != nlc::null)
                return (widget_desc.extract());
            else
                error_stream << warning_header << "fail to load" << widget_name
                             << " widget descriptor.\n";
        }
        return (load_widget_object(idle_obj, allocator, interface_resources, error_stream).extract());
    };
    BaseWidgetDescriptor selected_desc = widget_loader(selected_name);
    BaseWidgetDescriptor pressed_desc = widget_loader(pressed_name);
    BaseWidgetDescriptor disabled_desc = widget_loader(disabled_name);

    ButtonWidgetStatus default_status = ButtonWidgetStatus::Idle;
    if (button_obj.has(status_name) && button_obj.get_rhs(status_name).is<mdf::Kind::integer>()) {
        default_status = static_cast<ButtonWidgetStatus>(button_obj.get<u8>(status_name));
        if (default_status >= ButtonWidgetStatus::SIZE) {
            error_stream << warning_header << "default status superior to enum SIZE ("
                         << static_cast<u8>(default_status) << ".\n";
            default_status = ButtonWidgetStatus::Disabled;
        }
    }
    return (nlc::make_unique<ButtonWidgetCompDescriptor>(allocator,
                                                         idle_desc.extract(),
                                                         nlc::move(selected_desc),
                                                         nlc::move(pressed_desc),
                                                         nlc::move(disabled_desc),
                                                         default_status));
}

static auto load_draggable(mdf::Object const &,
                           nlc::allocator & allocator,
                           InterfaceResources const &,
                           nlc::string_stream &) -> nlc::unique_ptr<BaseWidgetCompDescriptor> {
    return (nlc::make_unique<DraggableWidgetCompDescriptor>(allocator));
}

static auto load_texture(mdf::Object const & texture_obj,
                         nlc::allocator & allocator,
                         InterfaceResources const &,
                         nlc::string_stream & error_stream)
    -> nlc::unique_ptr<BaseWidgetCompDescriptor> {
    constexpr char const warning_header[] = "TextureComponent definition invalid : ";
    constexpr char const path_name[] = "Path";
    constexpr char const fill_mode_name[] = "FillMode";
    constexpr char const layer_name[] = "Layer";

    if (texture_obj.has(path_name) == false ||
        texture_obj.get_rhs(path_name).is<mdf::Kind::string>() == false) {
        error_stream << warning_header << "no path.\n";
        return (nullptr);
    }

    auto const & path_view = texture_obj.get<nlc::string_view>(path_name);
    if (path_view.is_empty()) {
        error_stream << warning_header << "path is empty.\n";
        return (nullptr);
    }

    TextureFillMode fill_mode = TextureFillMode::KeepRatio;
    if (texture_obj.has(fill_mode_name) &&
        texture_obj.get_rhs(fill_mode_name).is<mdf::Kind::integer>()) {
        fill_mode = static_cast<TextureFillMode>(texture_obj.get<u8>(fill_mode_name));
        if (fill_mode >= TextureFillMode::SIZE) {
            error_stream << warning_header << "fill mode superior to enum SIZE ("
                         << static_cast<u8>(fill_mode) << ".\n";
            fill_mode = TextureFillMode::KeepRatio;
        }
    }
    TextureLayer layer = TextureLayer::Background;
    auto layer_data = texture_obj.try_get<u8>(layer_name);
    if (layer_data.succeeded()) {
        layer = static_cast<TextureLayer>(layer_data.get());
    } else if (layer_data.failed_with<InvalidFieldType>()) {
        error_stream << warning_header << layer_name << " must be an integer.\n";
        return (nullptr);
    }
    auto texture = nlc::make_unique<TextureWidgetCompDescriptor>(allocator,
                                                                 allocator,
                                                                 path_view,
                                                                 fill_mode,
                                                                 layer,
                                                                 error_stream);
    if (texture->Texture != nlc::null && texture->Texture->IsValid() == false) {
        error_stream << warning_header << "fail to load texture data for " << path_view << ".\n";
        return (nullptr);
    }
    return (texture);
}

static auto load_text(mdf::Object const & text_obj,
                      nlc::allocator & allocator,
                      InterfaceResources const & interface_resources,
                      nlc::string_stream & error_stream) -> nlc::unique_ptr<BaseWidgetCompDescriptor> {
    constexpr nlc::string_view text_key = "Text";
    constexpr nlc::string_view color_key = "Color";
    constexpr nlc::string_view font_name_key = "FontName";
    constexpr nlc::string_view font_size_key = "FontSize";
    constexpr nlc::string_view h_align_key = "HorizontalAlign";
    constexpr nlc::string_view v_align_key = "VerticalAlign";

    if (text_obj.has(text_key) == false || text_obj.get_rhs(text_key).is<mdf::Kind::string>() == false) {
        error_stream << "TextComponent definition invalid : no text.";
        return (nullptr);
    }
    nlc::string_view text_value = text_obj.get<nlc::string_view>(text_key);

    f32_4 color = TextWidgetCompDescriptor::DefaultColor;
    if (text_obj.has(color_key) && text_obj.get_rhs(color_key).is<mdf::Kind::array>()) {
        auto const color_elements = text_obj.get<mdf::Array>(color_key).elements<float>();
        if (color_elements.size() == 4) {
            color = { color_elements[0], color_elements[1], color_elements[2], color_elements[3] };
        }
    }
    usize font_index = interface_resources.DefaultFontIndex;
    if (text_obj.has(font_name_key)) {
        if (text_obj.get_rhs(font_name_key).is<mdf::Kind::string>() == false) {
            error_stream << "TextComponent definition invalid : font name must be a string "
                            "(default font will be used).\n";
        } else {
            nlc::string_view font_name = text_obj.get<nlc::string_view>(font_name_key);
            bool has_succeed = false;

            for (usize i : nlc::range(0, interface_resources.Fonts.size())) {
                if (interface_resources.Fonts[i].Name == font_name) {
                    font_index = i;
                    has_succeed = true;
                    break;
                }
            }
            if (has_succeed == false) {
                error_stream << "TextComponent definition invalid : font " << font_name
                             << " not found (default font will be used).\n";
            }
        }
    }

    HorizontalAlignment const h_align =
        get_value_or_default<mdf::Kind::integer, HorizontalAlignment, u8>(
            text_obj,
            h_align_key,
            TextWidgetCompDescriptor::DefaultHorizontalAlignment);
    VerticalAlignment const v_align = get_value_or_default<mdf::Kind::integer, VerticalAlignment, u8>(
        text_obj,
        v_align_key,
        TextWidgetCompDescriptor::DefaultVerticalAlignment);
    float const font_size =
        get_value_or_default<mdf::Kind::floating, float, float>(text_obj,
                                                                font_size_key,
                                                                TextWidgetCompDescriptor::DefaultFontSize);

    return (nlc::make_unique<TextWidgetCompDescriptor>(allocator,
                                                       allocator,
                                                       text_value,
                                                       color,
                                                       interface_resources.Fonts[font_index].Id,
                                                       font_size,
                                                       h_align,
                                                       v_align));
}

static auto load_grid(mdf::Object const & grid_obj,
                      nlc::allocator & allocator,
                      InterfaceResources const & interface_resources,
                      nlc::string_stream & error_stream) -> nlc::unique_ptr<BaseWidgetCompDescriptor> {
    constexpr nlc::string_view error_header = "GridComponent definition invalid : ";
    constexpr nlc::string_view children_key = "Children";
    constexpr nlc::string_view horizontal_order_key = "HorizontalOrder";
    constexpr nlc::string_view vertical_order_key = "VerticalOrder";
    constexpr nlc::string_view dimension_key = "MainDimension";
    constexpr nlc::string_view fill_key = "FillPolicy";
    constexpr nlc::string_view column_key = "MaxColumns";
    constexpr nlc::string_view line_key = "MaxLines";
    constexpr nlc::string_view v_padding_key = "VerticalPaddingTemplate";
    constexpr nlc::string_view h_padding_key = "HorizontalPaddingTemplate";

    bool has_error = false;

    auto dimension = grid_obj.try_get<u8>(dimension_key);

    has_error |= dimension.failed();
    if (dimension.failed_with<mdf::KeyNotFound>())
        error_stream << error_header << "no " << dimension_key << " defined. Must be a integer.\n";
    else if (dimension.failed_with<mdf::InvalidFieldType>())
        error_stream << error_header << dimension_key << " must be a integer.\n";
    if (dimension.get() != static_cast<u8>(ListLayout::Horizontal)) {
        has_error = true;
        error_stream << error_header << "grid dimension only support Horizontal.\n";
    }

    auto vertical_order = grid_obj.try_get<u8>(vertical_order_key);

    has_error |= vertical_order.failed();
    if (vertical_order.failed_with<mdf::KeyNotFound>())
        error_stream << error_header << "no " << vertical_order_key << " defined. Must be a integer.\n";
    else if (vertical_order.failed_with<mdf::InvalidFieldType>())
        error_stream << error_header << vertical_order_key << " must be a integer.\n";
    if (vertical_order.get() != static_cast<u8>(VerticalLayoutOrdering::TopToBottom)) {
        has_error = true;
        error_stream << error_header << "grid vertical order only support Top to Bottom.\n";
    }

    auto horizontal_order = grid_obj.try_get<u8>(horizontal_order_key);

    has_error |= horizontal_order.failed();
    if (horizontal_order.failed_with<mdf::KeyNotFound>())
        error_stream << error_header << "no " << horizontal_order_key
                     << " defined. Must be a integer.\n";
    else if (horizontal_order.failed_with<mdf::InvalidFieldType>())
        error_stream << error_header << horizontal_order_key << " must be a integer.\n";
    if (horizontal_order.get() != static_cast<u8>(HorizontalLayoutOrdering::LeftToRight)) {
        has_error = true;
        error_stream << error_header << "grid horizontal order only support Left To Right.\n";
    }

    auto fill_policy = grid_obj.try_get<u8>(fill_key);

    has_error |= fill_policy.failed();
    if (fill_policy.failed_with<mdf::KeyNotFound>())
        error_stream << error_header << "no " << fill_key << " defined. Must be a integer.\n";
    else if (fill_policy.failed_with<mdf::InvalidFieldType>())
        error_stream << error_header << fill_key << " must be a integer.\n";

    auto max_columns = grid_obj.try_get<usize>(column_key);

    has_error |= max_columns.failed();
    if (max_columns.failed_with<mdf::KeyNotFound>())
        error_stream << error_header << "no " << column_key << " defined. Must be a integer.\n";
    else if (max_columns.failed_with<mdf::InvalidFieldType>())
        error_stream << error_header << column_key << " must be a integer.\n";

    auto max_lines = grid_obj.try_get<usize>(line_key);

    has_error |= max_lines.failed();
    if (max_lines.failed_with<mdf::KeyNotFound>())
        error_stream << error_header << "no " << line_key << " defined. Must be a integer.\n";
    else if (max_lines.failed_with<mdf::InvalidFieldType>())
        error_stream << error_header << line_key << " must be a integer.\n";
    if (has_error)
        return (nullptr);

    auto grid = make_unique<GridWidgetCompDescriptor>(
        allocator,
        allocator,
        static_cast<ListLayout>(dimension.get()),
        static_cast<VerticalLayoutOrdering>(vertical_order.get()),
        static_cast<HorizontalLayoutOrdering>(horizontal_order.get()),
        static_cast<FillPolicy>(fill_policy.get()),
        max_columns.get(),
        max_lines.get());

    if (grid_obj.has(v_padding_key)) {
        if (grid_obj.get_rhs(v_padding_key).is<mdf::Kind::object>()) {
            nlc::optional<BaseWidgetDescriptor> padding_template =
                load_widget_object(grid_obj.get<mdf::Object>(v_padding_key),
                                   allocator,
                                   interface_resources,
                                   error_stream);
            grid.get()->VerticalPaddingDescriptor = nlc::move(padding_template);
            if (grid.get()->VerticalPaddingDescriptor != nlc::null &&
                grid.get()->VerticalPaddingDescriptor->SizeSpace !=
                    BaseWidgetDescriptor::ScreenSpace::Absolute) {
                error_stream << error_header << v_padding_key
                             << " should use Absolute space for layout alignment\n";
                return (nullptr);
            }

            error_stream << error_header << "padding is not yet supported.\n";
            return (nullptr);
        } else {
            error_stream << error_header << v_padding_key << " should be a widget.\n";
            return (nullptr);
        }
    }

    if (grid_obj.has(h_padding_key)) {
        if (grid_obj.get_rhs(h_padding_key).is<mdf::Kind::object>()) {
            nlc::optional<BaseWidgetDescriptor> padding_template =
                load_widget_object(grid_obj.get<mdf::Object>(h_padding_key),
                                   allocator,
                                   interface_resources,
                                   error_stream);
            grid.get()->HorizontalPaddingDescriptor = nlc::move(padding_template);
            if (grid.get()->HorizontalPaddingDescriptor != nlc::null &&
                grid.get()->HorizontalPaddingDescriptor->SizeSpace !=
                    BaseWidgetDescriptor::ScreenSpace::Absolute) {
                error_stream << error_header << h_padding_key
                             << " should use Absolute "
                                "space for layout "
                                "alignment\n";
                return (nullptr);
            }

            error_stream << error_header << "padding is not yet supported.\n";
            return (nullptr);
        } else {
            error_stream << error_header << h_padding_key << " should be a widget.\n";
            return (nullptr);
        }
    }

    if (grid_obj.has(children_key) == false ||
        grid_obj.get_rhs(children_key).is<mdf::Kind::array>() == false) {
        return (grid);
    }

    auto const children_desc = grid_obj.get<mdf::Array>(children_key).elements<mdf::Object>();

    grid.get()->Children.reserve(children_desc.size());
    for (mdf::Object const & widget_obj : children_desc) {
        nlc::optional<BaseWidgetDescriptor> widget =
            load_widget_object(widget_obj, allocator, interface_resources, error_stream);
        if (widget != nlc::null)
            grid.get()->Children.append(widget.extract());
    }
    return (grid);
}

static auto load_container(mdf::Object const & container_obj,
                           nlc::allocator & allocator,
                           InterfaceResources const & interface_resources,
                           nlc::string_stream & error_stream)
    -> nlc::unique_ptr<BaseWidgetCompDescriptor> {
    constexpr nlc::string_view error_header = "ContainerComponent definition invalid : ";
    constexpr nlc::string_view children_key = "Children";
    constexpr nlc::string_view horizontal_order_key = "HorizontalOrder";
    constexpr nlc::string_view vertical_order_key = "VerticalOrder";
    constexpr nlc::string_view layout_key = "Layout";
    constexpr nlc::string_view fill_key = "FillPolicy";
    constexpr nlc::string_view padding_key = "PaddingTemplate";

    auto container = make_unique<ContainerWidgetCompDescriptor>(allocator, allocator);

    if (container_obj.has(horizontal_order_key)) {
        if (container_obj.get_rhs(horizontal_order_key).is<mdf::Kind::integer>()) {
            container.get()->HorizontalOrder =
                static_cast<HorizontalLayoutOrdering>(container_obj.get<u8>(horizontal_order_key));
        } else {
            error_stream << error_header << horizontal_order_key << " should be an integer.\n";
            return (nullptr);
        }
    }
    if (container_obj.has(vertical_order_key)) {
        if (container_obj.get_rhs(vertical_order_key).is<mdf::Kind::integer>()) {
            container.get()->VerticalOrder =
                static_cast<VerticalLayoutOrdering>(container_obj.get<u8>(vertical_order_key));
        } else {
            error_stream << error_header << vertical_order_key << " should be an integer.\n";
            return (nullptr);
        }
    }
    if (container_obj.has(layout_key)) {
        if (container_obj.get_rhs(layout_key).is<mdf::Kind::integer>()) {
            container.get()->Layout = static_cast<ListLayout>(container_obj.get<u8>(layout_key));
        } else {
            error_stream << error_header << layout_key << " should be an integer.\n";
            return (nullptr);
        }
    }
    if (container_obj.has(fill_key)) {
        if (container_obj.get_rhs(fill_key).is<mdf::Kind::integer>()) {
            container.get()->Fill = static_cast<FillPolicy>(container_obj.get<u8>(fill_key));
        } else {
            error_stream << error_header << fill_key << " should be an integer.\n";
            return (nullptr);
        }
    }
    if (container_obj.has(padding_key)) {
        if (container_obj.get_rhs(padding_key).is<mdf::Kind::object>()) {
            nlc::optional<BaseWidgetDescriptor> padding_template =
                load_widget_object(container_obj.get<mdf::Object>(padding_key),
                                   allocator,
                                   interface_resources,
                                   error_stream);
            container.get()->PaddingDescriptor = nlc::move(padding_template);
            if (container.get()->PaddingDescriptor != nlc::null &&
                container.get()->PaddingDescriptor->SizeSpace !=
                    BaseWidgetDescriptor::ScreenSpace::Absolute) {
                error_stream << error_header
                             << "padding should use Absolute space for layout alignment\n";
                return (nullptr);
            }
        } else {
            error_stream << error_header << padding_key << " should be a widget.\n";
            return (nullptr);
        }
    }

    if (container_obj.has(children_key) == false ||
        container_obj.get_rhs(children_key).is<mdf::Kind::array>() == false) {
        return (container);
    }

    auto const children_desc = container_obj.get<mdf::Array>(children_key).elements<mdf::Object>();

    container.get()->Children.reserve(children_desc.size());
    for (mdf::Object const & widget_obj : children_desc) {
        nlc::optional<BaseWidgetDescriptor> widget =
            load_widget_object(widget_obj, allocator, interface_resources, error_stream);
        if (widget != nlc::null)
            container.get()->Children.append(widget.extract());
    }
    return (container);
}

auto load_widget_object(mdf::Object const & widget_obj,
                        nlc::allocator & allocator,
                        InterfaceResources const & interface_resources,
                        nlc::string_stream & error_stream) -> nlc::optional<BaseWidgetDescriptor> {
    constexpr char const name_widget_name[] = "Name";
    constexpr char const screen_position_name[] = "ScreenPosition";
    constexpr char const size_name[] = "Size";
    constexpr char const position_space_name[] = "PositionSpace";
    constexpr char const size_space_name[] = "SizeSpace";
    constexpr char const component_name[] = "Components";

    if (widget_obj.has(screen_position_name) == false ||
        widget_obj.get_rhs(screen_position_name).is<mdf::Kind::array>() == false) {
        error_stream << "Widget definition invalid : no screen position (must be an array of 2 "
                        "elements).\n";
        return (nlc::null);
    }
    if (widget_obj.has(size_name) == false ||
        widget_obj.get_rhs(size_name).is<mdf::Kind::array>() == false) {
        error_stream << "Widget definition invalid : no size (must be an array of 2 elements).\n";
        return (nlc::null);
    }

    // name
    bool has_name = (widget_obj.has(name_widget_name) &&
                     widget_obj.get_rhs(name_widget_name).is<mdf::Kind::string>());
    nlc::string_view widget_name =
        (has_name) ? widget_obj.get<nlc::string_view>(name_widget_name) : "Unnamed";

    // screen postion
    auto const screen_position_desc =
        widget_obj.get<mdf::Array>(screen_position_name).elements<float>();
    if (screen_position_desc.size() != 2) {
        error_stream << "Widget definition invalid : position has more or less than 2 elements.\n";
        return (nlc::null);
    }
    // size
    auto const size_desc = widget_obj.get<mdf::Array>(size_name).elements<float>();
    if (size_desc.size() != 2) {
        error_stream << "Widget definition invalid : size has more or less than 2 elements.\n";
        return (nlc::null);
    }
    // screen space
    BaseWidgetDescriptor::ScreenSpace position_space = BaseWidgetDescriptor::ScreenSpace::Relative;
    BaseWidgetDescriptor::ScreenSpace size_space = BaseWidgetDescriptor::ScreenSpace::Relative;
    if (widget_obj.has(position_space_name) &&
        widget_obj.get_rhs(position_space_name).is<mdf::Kind::integer>()) {
        position_space =
            static_cast<BaseWidgetDescriptor::ScreenSpace>(widget_obj.get<u8>(position_space_name));
    }
    if (position_space == BaseWidgetDescriptor::ScreenSpace::Relative &&
        (screen_position_desc[0] > 1.0f || screen_position_desc[0] < 0.0f ||
         screen_position_desc[1] > 1.0f || screen_position_desc[1] < 0.0f)) {
        error_stream << "Widget definition invalid(" << widget_name
                     << ") : relative position not included in [0, 1] : " << screen_position_desc[0]
                     << " / " << screen_position_desc[1] << "\n";
    }

    if (widget_obj.has(size_space_name) &&
        widget_obj.get_rhs(size_space_name).is<mdf::Kind::integer>()) {
        size_space =
            static_cast<BaseWidgetDescriptor::ScreenSpace>(widget_obj.get<u8>(size_space_name));
    }
    if (size_space == BaseWidgetDescriptor::ScreenSpace::Relative &&
        (size_desc[0] > 1.0f || size_desc[0] < 0.0f || size_desc[1] > 1.0f || size_desc[1] < 0.0f)) {
        error_stream << "Widget definition invalid(" << widget_name
                     << ") : relative size not included in [0 << 1] : " << size_desc[0] << " / "
                     << size_desc[1] << "\n";
    }

    BaseWidgetDescriptor ret_widget(allocator,
                                    widget_name,
                                    nlc::aabb2::from_pos_and_size({ screen_position_desc[0],
                                                                    screen_position_desc[1] },
                                                                  { size_desc[0], size_desc[1] }),
                                    position_space,
                                    size_space);

    // components
    if (widget_obj.has(component_name)) {
        if (widget_obj.get_rhs(component_name).is<mdf::Kind::array>() == false) {
            error_stream << "Widget definition invalid : components must be an array of widget.\n";
            return (nlc::null);
        }
        auto const component_desc = widget_obj.get<mdf::Array>(component_name).elements<mdf::Object>();

        ret_widget.Components.reserve(component_desc.size());

        using ComponentLoader =
            nlc::function_view<nlc::unique_ptr<BaseWidgetCompDescriptor>(mdf::Object const &,
                                                                         nlc::allocator &,
                                                                         InterfaceResources const &,
                                                                         nlc::string_stream &)>;
        nlc::pair<char const *, ComponentLoader> const component_name_to_loader[] = {
            { "ContainerComponent", load_container }, { "DraggableComponent", load_draggable },
            { "TextComponent", load_text },           { "ButtonComponent", load_button },
            { "TextureComponent", load_texture },     { "GridComponent", load_grid },
        };

        for (mdf::Object const & component_obj : component_desc) {
            component_obj.for_each([&component_name_to_loader,
                                    &ret_widget,
                                    &allocator,
                                    &interface_resources,
                                    &error_stream](nlc::string_view name, mdf::Rhs const & rhs) {
                if (rhs.is<mdf::Kind::object>() == false) {
                    error_stream << "Widget definition invalid : trying to use a non-object as "
                                    "component\n";
                    return;
                }
                for (auto & loader_pair : component_name_to_loader) {
                    const nlc::string_view component_type =
                        nlc::string_view::from_c_str(loader_pair.first);
                    if (name == component_type) {
                        auto component = loader_pair.second(mdf::unwrap<mdf::Object>(rhs),
                                                            allocator,
                                                            interface_resources,
                                                            error_stream);
                        if (component.get() == nullptr) {
                            error_stream << "Widget definition invalid : fail to produce component "
                                            "of type "
                                         << loader_pair.first << "\n";
                            return;
                        }
                        ret_widget.Components.append(nlc::move(component));
                        break;
                    }
                }
            });
        }
    }
    return (ret_widget);
}

auto load_widget(mdf::Rhs const & widget_def,
                 nlc::allocator & allocator,
                 InterfaceResources const & interface_resources,
                 nlc::string_stream & error_stream) -> nlc::optional<BaseWidgetDescriptor> {
    if (widget_def.is<mdf::Kind::object>() == false) {
        error_stream << "widget definition inexistant\n";
        return (nlc::null);
    }
    mdf::Object const & widget_obj = mdf::unwrap<mdf::Object>(widget_def);

    return (load_widget_object(widget_obj, allocator, interface_resources, error_stream));
}

}  // namespace mdf
