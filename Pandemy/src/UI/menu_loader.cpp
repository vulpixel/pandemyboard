#include "UI/menu_loader.hpp"
#include "UI/interface_resources.hpp"
#include "UI/menu_data.hpp"
#include "UI/widget_loader.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/string_stream.hpp>

#include <mdf/compiler.hpp>
#include <mdf/types.hpp>

namespace mdf {
auto menu_loader(nlc::allocator & alloc,
                 nlc::string_view const filename,
                 nlc::string_view const root_name,
                 nlc::span<nlc::string_view const> const object_names,
                 InterfaceResources const & interface_resources,
                 nlc::string_stream & error_stream) -> nlc::optional<MenuData> {
    nlc::vector<BaseWidgetDescriptor> widget_desc(alloc);

    constexpr nlc::string_view mandatory_files[] = { "data/UI/InterfaceEnums.mdf" };
    nlc::vector<nlc::string_view> all_files(alloc);
    // compute all files to compile together
    all_files.reserve(nlc::meta::array_len<decltype(mandatory_files)> + 1);
    all_files.append_from_no_grow(mandatory_files);
    all_files.append_no_grow(filename);

    auto compil_ctx = mdf::read_and_compile(alloc, nlc::make_span(all_files));
    auto const & errors = compil_ctx.compilation_result.ctx().errors();
    if (errors.is_empty() == false) {
        nlc::log_entry log_entry {};
        mdf::log_errors(errors, log_entry);
        error_stream << log_entry.c_str();
        return (nlc::null);
    }

    if (compil_ctx.compilation_result.root().has(root_name) == false) {
        error_stream << "No root " << root_name << " to load\n";
        return (nlc::null);
    }
    auto root_widget = mdf::load_widget(compil_ctx.compilation_result.root().get_rhs(root_name),
                                        alloc,
                                        interface_resources,
                                        error_stream);
    if (root_widget == nlc::null) {
        return (nlc::null);
    }

    widget_desc.reserve(object_names.size());
    for (nlc::string_view const object_name : object_names) {
        if (compil_ctx.compilation_result.root().has(object_name) == false) {
            error_stream << "No object " << object_name << " to load\n";
            return (nlc::null);
        }

        auto widget = mdf::load_widget(compil_ctx.compilation_result.root().get_rhs(object_name),
                                       alloc,
                                       interface_resources,
                                       error_stream);
        if (widget == nlc::null) {
            return (nlc::null);
        }
        widget_desc.append_no_grow(widget.extract());
    }
    return (MenuData { root_widget.extract(), nlc::move(widget_desc) });
}
}  // namespace mdf
