#include "UI/Controllers/main_menu_controller.hpp"
#include "UI/Widget/base_widget.hpp"
#include "UI/Widget/base_widget_component.hpp"
#include "UI/Widget/button_widget.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/menu_data.hpp"

#include <nlc/algo/find.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>

#include <win_input/axis_input.hpp>
#include <win_input/input_set.hpp>
#include <win_input/inputs.hpp>
#include <win_input/simple_input.hpp>

// Helpers
namespace {
static constexpr nlc::string_view start_button_name = "StartButton";
static constexpr nlc::string_view card_display_button_name = "CardDisplayButton";
static constexpr nlc::string_view options_button_name = "OptionsButton";
static constexpr nlc::string_view exit_button_name = "ExitButton";
static constexpr nlc::string_view menu_button_container_name = "MenuButtonContainer";
}  // namespace

auto MakeMainMenuController(nlc::allocator & alloc,
                            [[maybe_unused]] GameInfoDescriptor const & game_info,
                            [[maybe_unused]] InterfaceResources const & interface_resources,
                            BaseWidget & menu_widget,
                            [[maybe_unused]] nlc::span<BaseWidgetDescriptor const> const dependencies)
    -> nlc::unique_ptr<MainMenuController> {
    auto * const root_container = FindComponentInWidget<ContainerWidgetComponent>(menu_widget);
    nlc_assert(root_container);

    BaseWidget * const menu_button_container_widget =
        FindWidgetInList(nlc::make_span(root_container->Children), menu_button_container_name);
    nlc_assert(menu_button_container_widget);
    auto * const button_container =
        FindComponentInWidget<ContainerWidgetComponent>(*menu_button_container_widget);
    nlc_assert(button_container);

    BaseWidget * const start_widget =
        FindWidgetInList(nlc::make_span(button_container->Children), start_button_name);
    BaseWidget * const card_display_widget =
        FindWidgetInList(nlc::make_span(button_container->Children), card_display_button_name);
    BaseWidget * const options_widget =
        FindWidgetInList(nlc::make_span(button_container->Children), options_button_name);
    BaseWidget * const exit_widget =
        FindWidgetInList(nlc::make_span(button_container->Children), exit_button_name);
    nlc_assert(start_widget && options_widget && exit_widget);

    nlc::unique_ptr<MainMenuController> main_menu_controller =
        nlc::make_unique<MainMenuController>(alloc,
                                             alloc,
                                             *start_widget,
                                             *card_display_widget,
                                             *options_widget,
                                             *exit_widget);
    return (main_menu_controller);
}

// MainMenuController
#ifndef NDEBUG
auto MainMenuController::Validate(mdf::MenuData const & menu_descriptor,
                                  nlc::string_stream & error_stream) -> bool {
    static constexpr nlc::string_view validate_header = "MainMenuController validator : ";

    auto const * const root_container =
        FindComponentInDescriptor<ContainerWidgetCompDescriptor>(menu_descriptor.RootDescriptor);
    if (root_container == nullptr) {
        error_stream << validate_header << "no container in root widget named "
                     << menu_descriptor.RootDescriptor.Name << "\n";
        return (false);
    }

    BaseWidgetDescriptor const * menu_button_container_widget =
        FindWidgetInList(nlc::make_span(root_container->Children), menu_button_container_name);
    if (menu_button_container_widget == nullptr) {
        error_stream << validate_header << "no widget named " << menu_button_container_name
                     << " in root container\n";
        return (false);
    }

    auto const * const button_container =
        FindComponentInDescriptor<ContainerWidgetCompDescriptor>(*menu_button_container_widget);
    if (root_container == nullptr) {
        error_stream << validate_header << "no container in root widget named "
                     << menu_button_container_widget->Name << "\n";
        return (false);
    }

    BaseWidgetDescriptor const * const start_widget =
        FindWidgetInList(nlc::make_span(button_container->Children), start_button_name);
    BaseWidgetDescriptor const * const card_display_widget =
        FindWidgetInList(nlc::make_span(button_container->Children), card_display_button_name);
    BaseWidgetDescriptor const * const options_widget =
        FindWidgetInList(nlc::make_span(button_container->Children), options_button_name);
    BaseWidgetDescriptor const * const exit_widget =
        FindWidgetInList(nlc::make_span(button_container->Children), exit_button_name);
    if (start_widget == nullptr) {
        error_stream << validate_header << "no widget named " << start_button_name
                     << " in button container\n";
    }
    if (card_display_widget == nullptr) {
        error_stream << validate_header << "no widget named " << card_display_button_name
                     << " in button container\n";
    }
    if (options_widget == nullptr) {
        error_stream << validate_header << "no widget named " << options_button_name
                     << " in button container\n";
    }
    if (exit_widget == nullptr) {
        error_stream << validate_header << "no widget named " << exit_button_name
                     << " in button container\n";
    }

    bool validated = true;

    validated &= (start_widget != nullptr && ButtonController::Validate(*start_widget, error_stream));
    validated &= (card_display_widget != nullptr &&
                  ButtonController::Validate(*card_display_widget, error_stream));
    validated &=
        (options_widget != nullptr && ButtonController::Validate(*options_widget, error_stream));
    validated &= (exit_widget != nullptr && ButtonController::Validate(*exit_widget, error_stream));
    return (validated);
}
#endif
auto MainMenuController::CreateButtonController(MainMenuController & controller,
                                                nlc::allocator & alloc,
                                                Inputs::AxisId input) -> ButtonController {
    nlc::vector<ButtonController::WidgetToFunction> actions(alloc);

    actions.reserve(3);
    actions.append_no_grow(ButtonController::WidgetToFunction {
        .Widget = controller.StartWidget,
        .Action = nlc::function<void()> { alloc, [&controller]() { controller.GoToNext(); } } });
    actions.append_no_grow(ButtonController::WidgetToFunction {
        .Widget = controller.CardDisplayWidget,
        .Action = nlc::function<void()> { alloc, [&controller]() { controller.GoToCardDisplay(); } } });
    actions.append_no_grow(
        ButtonController::WidgetToFunction { .Widget = controller.OptionsWidget,
                                             .Action = nlc::function<void()> { alloc, []() {} } });
    actions.append_no_grow(ButtonController::WidgetToFunction {
        .Widget = controller.ExitWidget,
        .Action = nlc::function<void()> { alloc, [&controller]() { controller.GoToExit(); } } });
    return { alloc, nlc::make_span(actions), input };
}

MainMenuController::MainMenuController(nlc::allocator & alloc,
                                       BaseWidget & start_widget,
                                       BaseWidget & card_display_widget,
                                       BaseWidget & options_widget,
                                       BaseWidget & exit_widget)
    : BaseController(alloc)
    , MainMenuSet(Inputs::create_input_set("MainMenuControls"))
    , UpDownId(Inputs::register_axis(MainMenuSet, "up", "down", Inputs::AxisMode::preempt))
    , ActionId(Inputs::register_input(MainMenuSet,
                                      "action",
                                      [this](Inputs::Action action, GLFWwindow *) {
                                          ProcessAction(action);
                                      }))
    , ClickId(Inputs::register_input(MainMenuSet,
                                     "click",
                                     [this](Inputs::Action action, GLFWwindow *) {
                                         ProcessMouseAction(action);
                                     }))
    , StartWidget(start_widget)
    , CardDisplayWidget(card_display_widget)
    , OptionsWidget(options_widget)
    , ExitWidget(exit_widget)
    , Buttons(CreateButtonController(*this, alloc, UpDownId)) {
    Inputs::set_active(MainMenuSet, true);

    ActiveControllers.append(&Buttons);
}

MainMenuController::~MainMenuController() {
    Inputs::set_active(MainMenuSet, false);
    Inputs::unregister_input(ClickId);
    Inputs::unregister_input(ActionId);
    Inputs::unregister_axis(UpDownId);
    Inputs::destroy_input_set(MainMenuSet);
}

auto MainMenuController::ProcessInput() -> void {
    if (ShouldDiscardInput())
        return;

    BaseController::ProcessInput();
}

auto MainMenuController::ProcessAction(Inputs::Action action) -> void {
    Buttons.ProcessAction(action);
}

auto MainMenuController::ProcessMouseAction(Inputs::Action action) -> void {
    Buttons.ProcessMouseAction(action);
}

auto MainMenuController::GoToNext() -> void { TargetPage = MenuPage::TilePlacement; }

auto MainMenuController::GoToCardDisplay() -> void { TargetPage = MenuPage::CardDisplay; }

auto MainMenuController::GoToExit() -> void { TargetPage = MenuPage::Exit; }
