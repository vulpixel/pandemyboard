#include "UI/Controllers/card_widget_controller.hpp"
#include "Gameplay/Card/Components/character_desc.hpp"
#include "Gameplay/Card/card_descriptor.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/Widget/text_widget.hpp"
#include "UI/Widget/texture_widget.hpp"

#include <nlc/algo/find.hpp>
#include <nlc/containers/vector.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/fundamentals/to_string.hpp>

namespace {
static constexpr nlc::string_view name_widget_name = "CardName";
static constexpr nlc::string_view attack_widget_name = "Attack";
static constexpr nlc::string_view defense_widget_name = "Defense";
static constexpr nlc::string_view texture_widget_name = "CardTexture";
}  // namespace

auto MakeCardWidgetController(nlc::allocator & alloc, BaseWidget & card_widget)
    -> nlc::unique_ptr<CardWidgetController> {
    auto * const container = FindComponentInWidget<ContainerWidgetComponent>(card_widget);
    nlc_assert(container);

    auto const text_name_comp =
        FindComponentInNamedWidget<TextWidgetComponent>(nlc::make_span(container->Children),
                                                        name_widget_name);

    auto const text_attack_comp =
        FindComponentInNamedWidget<TextWidgetComponent>(nlc::make_span(container->Children),
                                                        attack_widget_name);

    auto const text_defense_comp =
        FindComponentInNamedWidget<TextWidgetComponent>(nlc::make_span(container->Children),
                                                        defense_widget_name);

    auto const texture_comp =
        FindComponentInNamedWidget<TextureWidgetComponent>(nlc::make_span(container->Children),
                                                           texture_widget_name);

    nlc_assert(text_name_comp.succeeded() && texture_comp.succeeded() &&
               text_attack_comp.succeeded() && text_defense_comp.succeeded());

    nlc::unique_ptr<CardWidgetController> card_controller =
        make_unique<CardWidgetController>(alloc,
                                          alloc,
                                          *text_name_comp.get(),
                                          *text_attack_comp.get(),
                                          *text_defense_comp.get(),
                                          *texture_comp.get(),
                                          nullptr);
    return (card_controller);
}

#ifndef NDEBUG
auto CardWidgetController::Validate(BaseWidgetDescriptor const & descriptor,
                                    nlc::string_stream & error_stream) -> bool {
    static constexpr nlc::string_view validate_header = "CardWidgetController validator : ";
    ContainerWidgetCompDescriptor const * const root_container =
        FindComponentInDescriptor<ContainerWidgetCompDescriptor>(descriptor);
    if (root_container == nullptr) {
        error_stream << validate_header << "no root container\n";
        return (false);
    }

    // widgets
    bool validated = true;
    auto validate_error = [&validated, &error_stream](auto const & find_result,
                                                      nlc::string_view widget_name) {
        if (find_result.failed()) {
            validated = false;
            if (find_result.template failed_with<ErrorWidgetNotFound>())
                error_stream << validate_header << "no widget " << widget_name << "\n";
            else if (find_result.template failed_with<ErrorComponentNotFound>())
                error_stream << validate_header << "no component in widget " << widget_name << "\n";
            else
                nlc_assert_msg(false,
                               "unknow error from FindComponentInNamedWidget :",
                               find_result.get_error_name());
        }
    };

    auto const text_name_comp =
        FindComponentInNamedDescriptor<TextWidgetCompDescriptor>(nlc::make_span(root_container->Children),
                                                                 name_widget_name);
    validate_error(text_name_comp, name_widget_name);

    auto const text_attack_comp =
        FindComponentInNamedDescriptor<TextWidgetCompDescriptor>(nlc::make_span(root_container->Children),
                                                                 attack_widget_name);
    validate_error(text_attack_comp, attack_widget_name);

    auto const text_defense_comp =
        FindComponentInNamedDescriptor<TextWidgetCompDescriptor>(nlc::make_span(root_container->Children),
                                                                 defense_widget_name);
    validate_error(text_defense_comp, defense_widget_name);

    auto const texture_comp =
        FindComponentInNamedDescriptor<TextureWidgetCompDescriptor>(nlc::make_span(
                                                                        root_container->Children),
                                                                    texture_widget_name);
    validate_error(texture_comp, texture_widget_name);
    return (validated);
}
#endif

CardWidgetController::CardWidgetController(nlc::allocator & alloc,
                                           TextWidgetComponent & text_name_comp,
                                           TextWidgetComponent & text_attack_comp,
                                           TextWidgetComponent & text_defense_comp,
                                           TextureWidgetComponent & texture_comp,
                                           CardDescriptor const * const descriptor)
    : Allocator(alloc)
    , NameTextComponent(text_name_comp)
    , AttackTextComponent(text_attack_comp)
    , DefenseTextComponent(text_defense_comp)
    , TextureComponent(texture_comp)
    , Descriptor(descriptor) {}

auto CardWidgetController::SetDescriptor(CardDescriptor const & descriptor) -> void {
    Descriptor = &descriptor;
    NameTextComponent.Text = nlc::string(Allocator, descriptor.Name);
    TextureComponent.TextureView = descriptor.Texture;

    if (descriptor.Content.contains<CharacterDescriptor>()) {
        auto const & hero = descriptor.Content.as<CharacterDescriptor>();
        AttackTextComponent.Text = nlc::to_string(Allocator, hero.Attack);
        DefenseTextComponent.Text = nlc::to_string(Allocator, hero.Defense);
    }
}
