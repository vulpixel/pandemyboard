#include "UI/Controllers/button_controller.hpp"
#include "UI/Widget/base_widget.hpp"
#include "UI/Widget/button_widget.hpp"

#include <nlc/dialect/range.hpp>
#include <nlc/fundamentals/string_stream.hpp>

#include <win_input/inputs.hpp>

#ifndef NDEBUG
auto ButtonController::Validate(BaseWidgetDescriptor const & descriptor,
                                nlc::string_stream & error_stream) -> bool {
    if (FindComponentInDescriptor<ButtonWidgetCompDescriptor>(descriptor) == nullptr) {
        error_stream << "Button controller for widget named " << descriptor.Name
                     << " has no component Button\n";
        return (false);
    }
    return (true);
}
#endif

auto ButtonController::Create(nlc::allocator & alloc,
                              nlc::span<WidgetToFunction> actions,
                              Inputs::AxisId input) -> ButtonController {
    return (ButtonController { alloc, actions, input });
}

ButtonController::ButtonController(nlc::allocator & alloc,
                                   nlc::span<WidgetToFunction> button_data,
                                   Inputs::AxisId up_down_id)
    : BaseController(alloc)
    , ButtonData(alloc)
    , UpDownId(up_down_id) {
    nlc_assert_msg(button_data.size(), "Creating instance of button controller with 0 button.");

    ButtonData.reserve(button_data.size());
    for (auto & button_datum : button_data) {
        auto * const component = FindComponentInWidget<ButtonWidgetComponent>(button_datum.Widget);
        nlc_assert(component);
        ButtonData.append_no_grow(button_datum.Widget, component, nlc::move(button_datum.Action));
    }
}

ButtonController::~ButtonController() {}

auto ButtonController::ProcessInput() -> void {
    nlc_assert_msg(!ButtonData.is_empty(), "ProcessInput called on an uninitialized ButtonController");

    if (ShouldDiscardInput())
        return;

    if (UpDownId.is_valid()) {
        if (Inputs::get_value(UpDownId) < 0) {
            if (HasSelected())
                SetCurrentStatus(ButtonWidgetStatus::Idle);
            for ([[maybe_unused]] auto i : nlc::range(0, ButtonData.size())) {
                if (HasSelected())
                    CurrentlySelected = (*CurrentlySelected + 1) % ButtonData.size();
                else
                    CurrentlySelected = 0u;
                if (SetCurrentStatus(ButtonWidgetStatus::Selected))
                    break;
            }
            AcknowInput();
        } else if (Inputs::get_value(UpDownId) > 0) {
            if (HasSelected())
                SetCurrentStatus(ButtonWidgetStatus::Idle);
            for ([[maybe_unused]] auto i : nlc::range(0, ButtonData.size())) {
                if (HasSelected())
                    CurrentlySelected =
                        (*CurrentlySelected + ButtonData.size() - 1) % ButtonData.size();
                else
                    CurrentlySelected = 0u;
                if (SetCurrentStatus(ButtonWidgetStatus::Selected))
                    break;
            }
            AcknowInput();
        }
    }

    ProcessMouse();
}

auto ButtonController::ProcessMouse() -> void {
    nlc_assert_msg(!ButtonData.is_empty(), "ProcessMouse called on an uninitialized ButtonController");
    auto const cursor_pos = Inputs::get_cursor_position();

    if (cursor_pos != LastMousePos) {
        for (auto i : nlc::range(0, ButtonData.size())) {
            if (ButtonData[i].Button->CurrentStatus != ButtonWidgetStatus::Disabled &&
                ButtonData[i].Button->CurrentStatus != ButtonWidgetStatus::Pressed &&
                ButtonData[i].Widget.ScreenArea.contains(rm_space(cursor_pos))) {
                if (HasSelected())
                    SetCurrentStatus(ButtonWidgetStatus::Idle);
                CurrentlySelected = i;
                SetCurrentStatus(ButtonWidgetStatus::Selected);
                break;
            }
        }
        if (HasSelected() && GetCurrentWidget().ScreenArea.contains(rm_space(cursor_pos)) == false) {
            SetCurrentStatus(ButtonWidgetStatus::Idle);
            CurrentlySelected = nlc::null;
        }
    }
    LastMousePos = cursor_pos;
}

auto ButtonController::ProcessMouseAction(Inputs::Action action) -> void {
    nlc_assert_msg(!ButtonData.is_empty(),
                   "ProcessMouseAction called on an uninitialized ButtonController");

    auto const cursor_pos = Inputs::get_cursor_position();

    if (HasSelected() && GetCurrentWidget().ScreenArea.contains(rm_space(cursor_pos))) {
        ProcessAction(action);
    }
}

auto ButtonController::ProcessAction(Inputs::Action action) -> void {
    nlc_assert_msg(!ButtonData.is_empty(), "ProcessAction called on an uninitialized ButtonController");

    if (action == Inputs::Action::press && HasSelected()) {
        SetCurrentStatus(ButtonWidgetStatus::Pressed);
    }
    if (action == Inputs::Action::release && HasSelected()) {
        SetCurrentStatus(ButtonWidgetStatus::Selected);
        GetCurrentAction()();
    }
}

auto ButtonController::SetCurrentStatus(ButtonWidgetStatus status) -> bool {
    nlc_assert_msg(CurrentlySelected != nlc::null, "Calling set current on a non-selected button.");
    if (GetCurrentComponent().CurrentStatus == ButtonWidgetStatus::Disabled)
        return (false);
    GetCurrentComponent().CurrentStatus = status;
    return (true);
}
