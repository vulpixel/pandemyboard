#include "UI/Controllers/menu_flow_controller.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/menu_data.hpp"

#include "UI/Controllers/main_menu_controller.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/range.hpp>

MenuFlowController::MenuPageInfo::MenuPageInfo(nlc::allocator & alloc)
    : MenuDescriptor({ BaseWidgetDescriptor { alloc }, nlc::vector<BaseWidgetDescriptor> { alloc } })
    , ControllerInstanciator(alloc, [](auto &, auto &, auto &, auto &, auto) {
        nlc_assert_msg(false, "Calling an unset controller instanciator.");
        return (nullptr);
    }) {}

MenuFlowController::MenuFlowController(nlc::allocator & alloc,
                                       ContainerWidgetComponent & root_container,
                                       GameInfoDescriptor const & game_info,
                                       InterfaceResources const & interface_resources,
                                       AppStatus & app_status)
    : ApplicationStatus(app_status)
    , GameInfo(game_info)
    , InterfaceRes(interface_resources)
    , RootContainer(root_container)
    , Allocator(alloc) {
    for (auto i : nlc::range(0, static_cast<usize>(MenuPage::MAX))) {
        new (&MenuDescriptors[i]) MenuPageInfo(Allocator);
    }
}

MenuFlowController::~MenuFlowController() {
    for (auto i : nlc::range(0, static_cast<usize>(MenuPage::MAX))) {
        MenuDescriptors[i].~MenuPageInfo();
    }
}

auto MenuFlowController::Init(MenuPageDescriptors & menu_pages) -> void {
#ifndef NDEBUG
    bInitialized = true;
#endif
    SetDescriptors(menu_pages);
    SetMenu(MenuPage::MainMenu);
}

auto MenuFlowController::Update() -> void {
    CheckInitialized();
    nlc_assert(CurrentController != nullptr);
    CurrentController->ProcessInput();
    auto const & target_menu_page = CurrentController->GetTargetPage();
    if (target_menu_page != nlc::null) {
        if (*target_menu_page != MenuPage::Exit) {
            SetMenu(*target_menu_page);
        } else {
            ApplicationStatus = AppStatus::Exit;
        }
    }
}

auto MenuFlowController::SetDescriptors(MenuPageDescriptors & menu_pages) -> void {
    CheckInitialized();
    for (auto i : nlc::range(0, static_cast<usize>(MenuPage::MAX))) {
        MenuDescriptors[i] = nlc::move(menu_pages[i]);
    }
}

auto MenuFlowController::SetMenu(MenuPage page) -> void {
    CheckInitialized();
    RootContainer.Children.clear();

    nlc_assert_msg(page < MenuPage::MAX, "Set menu on a page out of MenuPage size.");
    auto const & TargetMenuDescriptor = MenuDescriptors[static_cast<usize>(page)];
    BaseWidget & menu_instance = RootContainer.Children.append(
        TargetMenuDescriptor.MenuDescriptor.RootDescriptor.CreateInstance(Allocator));

    CurrentController = nullptr;
    CurrentController = TargetMenuDescriptor.ControllerInstanciator(
        Allocator,
        GameInfo,
        InterfaceRes,
        menu_instance,
        nlc::make_span(TargetMenuDescriptor.MenuDescriptor.Dependencies));
}

auto MenuFlowController::CheckInitialized() -> void {
#ifndef NDEBUG
    nlc_assert_msg(bInitialized, "Menu flow controller not initalized before update");
#endif
}
