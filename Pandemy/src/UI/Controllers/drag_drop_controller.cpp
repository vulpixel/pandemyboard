#include "UI/Controllers/drag_drop_controller.hpp"

#include "UI/Widget/container_widget.hpp"
#include "UI/Widget/draggable_widget.hpp"

#include <nlc/algo/find.hpp>

#include <graphic_engine/graphic_engine.hpp>
#include <win_input/inputs.hpp>

DragDropController::DragDropController(nlc::allocator & alloc,
                                       ContainerWidgetComponent & dummy_container,
                                       nlc::vector<BaseWidget> const & draggables,
                                       nlc::function<BaseWidget(BaseWidget const &)> && OnDrag,
                                       nlc::function<void(BaseWidget &&, f32_2)> && OnDrop)
    : BaseController(alloc)
    , DraggableWidgets(draggables)
    , DummyContainer(dummy_container)
    , Dummy(nullptr)
    , OnDragCallback(nlc::move(OnDrag))
    , OnDropCallback(nlc::move(OnDrop)) {}

DragDropController::~DragDropController() {}

auto DragDropController::ProcessInput() -> void {
    if (ShouldDiscardInput())
        return;

    ProcessMouse();
}

auto DragDropController::ProcessMouse() -> void {
    if (Dummy == nullptr)
        return;

    MoveWidget(*Dummy, rm_space(Inputs::get_cursor_position()));
}

auto DragDropController::ProcessMouseAction(Inputs::Action action) -> void {
    f32_2 const mouse_pos = rm_space(Inputs::get_cursor_position());
    if (action == Inputs::Action::press) {
        for (auto const & Draggable : DraggableWidgets) {
            if (Draggable.ScreenArea.contains(mouse_pos)) {
                Dummy = &DummyContainer.Children.append(OnDragCallback(Draggable));
                break;
            }
        }
    } else {
        for (usize i = 0; i < DummyContainer.Children.size(); ++i) {
            if (&DummyContainer.Children[i] == Dummy) {
                OnDropCallback(DummyContainer.Children.extract_unordered(i), mouse_pos);
                break;
            }
        }
        Dummy = nullptr;
    }
}
