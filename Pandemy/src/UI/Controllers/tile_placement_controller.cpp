#include "UI/Controllers/tile_placement_controller.hpp"

#include "Gameplay/game_info_descriptor.hpp"

#include "UI/Controllers/button_controller.hpp"
#include "UI/Widget/base_widget.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/Widget/grid_widget.hpp"
#include "UI/Widget/texture_widget.hpp"
#include "UI/interface_resources.hpp"
#include "UI/menu_data.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/pointer.hpp>

#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>

#include <nlc/meta/numeric_limits.hpp>

namespace {
constexpr nlc::string_view back_button_name = "BackButton";
constexpr nlc::string_view board_name = "Board";
constexpr nlc::string_view tile_bag_name = "TileBag";
// Tile Template
constexpr nlc::string_view tile_template_name = "TileTemplate";
constexpr nlc::string_view dragged_tile_template_name = "DraggedTileTemplate";
constexpr nlc::string_view tile_template_base_name = "BaseTexture";
constexpr nlc::string_view tile_template_placed_name = "PlacedTexture";
constexpr nlc::string_view tile_template_invalid_name = "InvalidTexture";
}  // namespace

static auto FindTileInstanceFromWidgetId(BaseWidget::ID target_id,
                                         nlc::span<nlc::pair<BaseWidget::ID, TileInstance>> widget_to_tile_list)
    -> TileInstance & {
    auto const target_to_tile_instance =
        nlc::pointer(nlc::find_if(widget_to_tile_list, [target_id](auto const & widget_to_tile) {
            return (widget_to_tile.first == target_id);
        }));
    return (target_to_tile_instance->second);
}

static auto UpdateTextureFromOpenings(InterfaceResources const & interface_resources,
                                      BaseWidget & widget,
                                      TileOpening const openings) -> void {
    auto const main_container =
        nlc::pointer { FindComponentInWidget<ContainerWidgetComponent>(widget) };
    auto const base_texture_widget =
        nlc::pointer { FindWidgetInList(nlc::make_span(main_container->Children),
                                        tile_template_base_name) };
    auto const base_texture =
        nlc::pointer { FindComponentInWidget<TextureWidgetComponent>(*base_texture_widget) };
    auto const placed_texture_widget =
        nlc::pointer { FindWidgetInList(nlc::make_span(main_container->Children),
                                        tile_template_placed_name) };
    auto const placed_texture =
        nlc::pointer { FindComponentInWidget<TextureWidgetComponent>(*placed_texture_widget) };
    auto const invalid_texture_widget =
        nlc::pointer { FindWidgetInList(nlc::make_span(main_container->Children),
                                        tile_template_invalid_name) };
    auto const invalid_texture =
        nlc::pointer { FindComponentInWidget<TextureWidgetComponent>(*invalid_texture_widget) };
    auto const & texture_view = interface_resources.OpeningToTextures[static_cast<u8>(openings)];
    auto const set_texture = [&texture_view](auto & tex_comp) {
        tex_comp->TextureView = texture_view.Texture;
        tex_comp->Rotation = texture_view.Rotation;
    };
    set_texture(base_texture);
    set_texture(invalid_texture);
    set_texture(placed_texture);
    invalid_texture_widget->IsDisplayable = false;
    placed_texture_widget->IsDisplayable = false;
}

static auto UpdateTileLayer(BaseWidget & tile, TextureLayer layer) -> void {
    auto const main_container = nlc::pointer { FindComponentInWidget<ContainerWidgetComponent>(tile) };
    auto const base_texture =
        FindComponentInNamedWidget<TextureWidgetComponent>(nlc::make_span(main_container->Children),
                                                           tile_template_base_name);
    nlc_assert(base_texture.succeeded());
    base_texture.get()->SetLayer(layer);
}

auto MakeTilePlacementController(nlc::allocator & alloc,
                                 GameInfoDescriptor const & game_info,
                                 InterfaceResources const & interface_resources,
                                 BaseWidget & menu_widget,
                                 nlc::span<BaseWidgetDescriptor const> const dependencies)
    -> nlc::unique_ptr<TilePlacementController> {
    auto const main_container =
        nlc::pointer { FindComponentInWidget<ContainerWidgetComponent>(menu_widget) };
    auto const back_widget =
        nlc::pointer { FindWidgetInList(nlc::make_span(main_container->Children), back_button_name) };
    auto const tile_template =
        nlc::pointer { FindWidgetInList(nlc::make_span(dependencies), tile_template_name) };
    auto const dragged_tile_template =
        nlc::pointer { FindWidgetInList(nlc::make_span(dependencies), dragged_tile_template_name) };

    auto const board_widget =
        nlc::pointer { FindWidgetInList(nlc::make_span(main_container->Children), board_name) };

    auto const tile_bag_grid =
        FindComponentInNamedWidget<GridWidgetComponent>(nlc::make_span(main_container->Children),
                                                        tile_bag_name);
    return (nlc::make_unique<TilePlacementController>(alloc,
                                                      alloc,
                                                      interface_resources,
                                                      nlc::make_span(game_info.TileBag),
                                                      *back_widget.get(),
                                                      *board_widget,
                                                      *tile_bag_grid.get(),
                                                      *tile_template,
                                                      *dragged_tile_template));
}

#ifndef NDEBUG
auto TilePlacementController::Validate(mdf::MenuData const & menu_descriptor,
                                       nlc::string_stream & error_stream) -> bool {
    static constexpr nlc::string_view validate_header = "TilePlacementController validator error: ";
    auto const * const main_container =
        FindComponentInDescriptor<ContainerWidgetCompDescriptor>(menu_descriptor.RootDescriptor);
    if (main_container == nullptr) {
        error_stream << validate_header << "no container in root widget\n";
        return (false);
    }
    bool validated = true;
    auto validate_error = [&validated, &error_stream](auto const & find_result,
                                                      nlc::string_view widget_name) {
        if (find_result.failed()) {
            validated = false;
            if (find_result.template failed_with<ErrorWidgetNotFound>())
                error_stream << validate_header << "no widget " << widget_name << "\n";
            else if (find_result.template failed_with<ErrorComponentNotFound>())
                error_stream << validate_header << "no component in widget " << widget_name << "\n";
            else
                nlc_assert_msg(false,
                               "unknow error from FindComponentInNamedWidget :",
                               find_result.get_error_name());
        }
    };
    auto const * const back_widget =
        FindWidgetInList(nlc::make_span(main_container->Children), back_button_name);

    if (back_widget == nullptr) {
        error_stream << validate_header << "no widget named " << back_button_name << "\n";
    }
    validated &= back_widget && ButtonController::Validate(*back_widget, error_stream);

    auto const board_container =
        FindComponentInNamedDescriptor<ContainerWidgetCompDescriptor>(nlc::make_span(
                                                                          main_container->Children),
                                                                      board_name);
    validate_error(board_container, board_name);
    auto const tile_bag_grid =
        FindComponentInNamedDescriptor<GridWidgetCompDescriptor>(nlc::make_span(main_container->Children),
                                                                 tile_bag_name);
    validate_error(tile_bag_grid, tile_bag_name);
    auto const tile_template_container =
        FindComponentInNamedDescriptor<ContainerWidgetCompDescriptor>(nlc::make_span(
                                                                          menu_descriptor.Dependencies),
                                                                      tile_template_name);
    validate_error(tile_template_container, tile_template_name);
    if (tile_template_container.succeeded()) {
        auto const base_texture = FindComponentInNamedDescriptor<TextureWidgetCompDescriptor>(
            nlc::make_span(tile_template_container.get()->Children),
            tile_template_base_name);
        validate_error(base_texture, tile_template_base_name);

        auto const placed_texture = FindComponentInNamedDescriptor<TextureWidgetCompDescriptor>(
            nlc::make_span(tile_template_container.get()->Children),
            tile_template_placed_name);
        validate_error(placed_texture, tile_template_placed_name);

        auto const invalid_texture = FindComponentInNamedDescriptor<TextureWidgetCompDescriptor>(
            nlc::make_span(tile_template_container.get()->Children),
            tile_template_invalid_name);
        validate_error(invalid_texture, tile_template_invalid_name);
    }
    auto const dragged_tile_template_container =
        FindComponentInNamedDescriptor<ContainerWidgetCompDescriptor>(nlc::make_span(
                                                                          menu_descriptor.Dependencies),
                                                                      dragged_tile_template_name);
    validate_error(tile_template_container, dragged_tile_template_name);
    if (dragged_tile_template_container.succeeded()) {
        auto const base_texture = FindComponentInNamedDescriptor<TextureWidgetCompDescriptor>(
            nlc::make_span(dragged_tile_template_container.get()->Children),
            tile_template_base_name);
        validate_error(base_texture, tile_template_base_name);

        auto const placed_texture = FindComponentInNamedDescriptor<TextureWidgetCompDescriptor>(
            nlc::make_span(dragged_tile_template_container.get()->Children),
            tile_template_placed_name);
        validate_error(placed_texture, tile_template_placed_name);

        auto const invalid_texture = FindComponentInNamedDescriptor<TextureWidgetCompDescriptor>(
            nlc::make_span(dragged_tile_template_container.get()->Children),
            tile_template_invalid_name);
        validate_error(invalid_texture, tile_template_invalid_name);
    }
    return (validated);
}
#endif

auto TilePlacementController::CreateBackButtonController(TilePlacementController & controller,
                                                         nlc::allocator & alloc,
                                                         BaseWidget & back_button_widget)
    -> ButtonController {
    ButtonController::WidgetToFunction button_data = {
        .Widget = back_button_widget,
        .Action = nlc::function<void()> { alloc, [&controller]() { controller.GoToMainMenu(); } }
    };
    return (ButtonController::Create(alloc, button_data, Inputs::AxisId::invalid()));
}

TilePlacementController::TilePlacementController(nlc::allocator & alloc,
                                                 InterfaceResources const & interface_resources,
                                                 nlc::span<TileDescriptor const> tiles,
                                                 BaseWidget & back_button,
                                                 BaseWidget & board_widget,
                                                 GridWidgetComponent & tile_bag,
                                                 BaseWidgetDescriptor const & tile_template,
                                                 BaseWidgetDescriptor const & dragged_tile_template)
    : BaseController(alloc)
    , TilePlacementSet(Inputs::create_input_set("TilePlacementControls"))
    , ClickId(Inputs::register_input(TilePlacementSet,
                                     "click",
                                     [this](Inputs::Action action, GLFWwindow *) {
                                         ProcessMouseAction(action);
                                     }))
    , RotationId(
          Inputs::register_axis(TilePlacementSet, "second_right", "second_left", Inputs::AxisMode::preempt))
    , Alloc(alloc)
    , InterfaceRes(interface_resources)
    , WidgetToTile(alloc)
    , CurrentDrag()
    , BoardWidget(board_widget)
    , Board(*nlc::pointer { FindComponentInWidget<ContainerWidgetComponent>(board_widget) })
    , TileBag(tile_bag)
    , TileTemplate(tile_template)
    , DraggedTileTemplate(dragged_tile_template)
    , BackButtonController(CreateBackButtonController(*this, alloc, back_button))
    , DragController(DragDropController::Create(
          alloc,
          Board,
          TileBag.Children,
          [this](BaseWidget const & target) { return (BeginDrag(target)); },
          [this](BaseWidget && dropped, f32_2 position) { EndDrag(nlc::move(dropped), position); })) {
    Inputs::set_active(TilePlacementSet, true);
    ActiveControllers.append(&BackButtonController);
    ActiveControllers.append(&DragController);

    FillTileBag(tiles);
    Board.Children.clear();
}

TilePlacementController::~TilePlacementController() {
    Inputs::set_active(TilePlacementSet, false);
    Inputs::unregister_input(ClickId);
    Inputs::unregister_axis(RotationId);
    Inputs::destroy_input_set(TilePlacementSet);
}

auto TilePlacementController::ProcessInput() -> void {
    BaseController::ProcessInput();
    if (ShouldDiscardInput())
        return;
    if (CurrentDrag != nlc::null && Inputs::get_value(RotationId) != 0.0f) {
        if (Inputs::get_value(RotationId) < 0.0f) {
            RotateTile(CurrentDrag->Tile, false);
        } else if (Inputs::get_value(RotationId) > 0.0f) {
            RotateTile(CurrentDrag->Tile, true);
        }
        auto const CurrentDraggedWidget =
            nlc::pointer { FindWidgetInListFromId(CurrentDrag->WidgetId,
                                                  nlc::make_span(Board.Children)) };
        UpdateTextureFromOpenings(InterfaceRes, *CurrentDraggedWidget, CurrentDrag->Tile.CurrentOpenings);
        AcknowInput();
    }
}

auto TilePlacementController::ProcessMouseAction(Inputs::Action action) -> void {
    BackButtonController.ProcessMouseAction(action);
    DragController.ProcessMouseAction(action);
}

auto TilePlacementController::GoToMainMenu() -> void { TargetPage = MenuPage::MainMenu; }

auto TilePlacementController::BeginDrag(BaseWidget const & target) -> BaseWidget {
    nlc_assert_msg(CurrentDrag == nlc::null,
                   "CurrentlyDraggedIndex already set, restart a drag without a drop.");
    auto const drag_index = static_cast<usize>(&target - &TileBag.Children.front());
    nlc_assert_msg(drag_index < TileBag.Children.size(), "CurrentlyDraggedIndex invalid.");
    TileBag.Children[drag_index].IsDisplayable = false;
    BaseWidget dummy = DraggedTileTemplate.CreateInstance(Alloc);
    CurrentDrag.set(drag_index,
                    dummy.GetId(),
                    TileInstance(FindTileInstanceFromWidgetId(target.GetId(),
                                                              nlc::make_span(WidgetToTile))));

    UpdateTileLayer(dummy, TextureLayer::AlwaysOnTop);
    UpdateTextureFromOpenings(InterfaceRes, dummy, CurrentDrag->Tile.CurrentOpenings);
    return dummy;
}

auto TilePlacementController::EndDrag(BaseWidget && dropped, f32_2 position) -> void {
    nlc_assert(CurrentDrag != nlc::null);
    nlc_assert_msg(CurrentDrag->BagIndex < TileBag.Children.size(),
                   "Dropping a widget with invalid CurrentlyDraggedIndex.");
    if (BoardWidget.ScreenArea.contains(position)) {
        UpdateTileLayer(dropped, TextureLayer::Background);
        Board.Children.append(nlc::move(dropped));
        // add board tile instance
        TileBag.Children.remove_ordered(CurrentDrag->BagIndex);
    } else {
        auto & origin_instance =
            FindTileInstanceFromWidgetId(TileBag.Children[CurrentDrag->BagIndex].GetId(),
                                         nlc::make_span(WidgetToTile));

        origin_instance.CurrentOpenings = CurrentDrag->Tile.CurrentOpenings;
        UpdateTextureFromOpenings(InterfaceRes,
                                  TileBag.Children[CurrentDrag->BagIndex],
                                  origin_instance.CurrentOpenings);
        TileBag.Children[CurrentDrag->BagIndex].IsDisplayable = true;
    }
    nlc_assert(nlc::find_if(nlc::make_span(TileBag.Children), [&dropped](BaseWidget const & widget) {
                   return (&widget == &dropped);
               }) == nullptr);
    CurrentDrag = nlc::null;
}

auto TilePlacementController::FillTileBag(nlc::span<TileDescriptor const> tiles) -> void {
    nlc_assert(tiles.is_empty() == false);
    TileBag.Children.clear();

    WidgetToTile.reserve(tiles.size());
    auto fill_grid_template =
        nlc::vector<nlc::pointer<BaseWidgetDescriptor const>>::create_and_fill(Alloc,
                                                                               &TileTemplate,
                                                                               tiles.size());
    PopulateGridWidget<nlc::pointer<BaseWidgetDescriptor const>>(TileBag,
                                                                 nlc::make_span(fill_grid_template),
                                                                 Alloc);
    nlc_assert_msg(TileBag.Children.size() >= tiles.size(),
                   nlc_dump_var(TileBag.Children.size()),
                   nlc_dump_var(tiles.size()));
    usize tile_bag_index = 0;
    for (auto const & tile_desc : tiles) {
        while (GridElementIsPadding(TileBag, TileBag.Children[tile_bag_index])) {
            ++tile_bag_index;
            nlc_assert(tile_bag_index < TileBag.Children.size());
        }
        BaseWidget & tile_widget = TileBag.Children[tile_bag_index];
        UpdateTextureFromOpenings(InterfaceRes, tile_widget, tile_desc.Openings);
        WidgetToTile.append_no_grow(tile_widget.GetId(), TileInstance(tile_desc));
        ++tile_bag_index;
    }
}
