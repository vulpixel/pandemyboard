#include "UI/Controllers/card_display_controller.hpp"
#include "UI/Widget/base_widget.hpp"
#include "UI/Widget/base_widget_component.hpp"
#include "UI/Widget/button_widget.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/menu_data.hpp"

#include "Gameplay/game_info_descriptor.hpp"

#include <nlc/algo/find.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>

#include <win_input/axis_input.hpp>
#include <win_input/input_set.hpp>
#include <win_input/inputs.hpp>
#include <win_input/simple_input.hpp>

namespace {
constexpr nlc::string_view back_button_name = "BackButton";
constexpr nlc::string_view focused_card_name = "FocusCard";
constexpr nlc::string_view card_container_name = "CardContainer";
constexpr nlc::string_view deck_tabs_name = "DeckTabs";

constexpr nlc::string_view template_name = "DeckButtonTemplate";

}  // namespace

auto MakeCardDisplayController(nlc::allocator & alloc,
                               GameInfoDescriptor const & game_info,
                               [[maybe_unused]] InterfaceResources const & interface_resources,
                               BaseWidget & menu_widget,
                               [[maybe_unused]] nlc::span<BaseWidgetDescriptor const> const dependencies)
    -> nlc::unique_ptr<CardDisplayController> {
    auto const main_container =
        nlc::pointer { FindComponentInWidget<ContainerWidgetComponent>(menu_widget) };
    auto const back_widget =
        nlc::pointer { FindWidgetInList(nlc::make_span(main_container->Children), back_button_name) };

    auto const card_container =
        FindComponentInNamedWidget<ContainerWidgetComponent>(nlc::make_span(main_container->Children),
                                                             card_container_name);
    auto const deck_tabs =
        FindComponentInNamedWidget<ContainerWidgetComponent>(nlc::make_span(main_container->Children),
                                                             deck_tabs_name);

    nlc_assert(card_container.succeeded() && deck_tabs.succeeded());
    auto const focused_card =
        nlc::pointer { FindWidgetInList(nlc::make_span(card_container.get()->Children),
                                        focused_card_name) };

    nlc::span<BaseWidget> const cards = nlc::make_span(card_container.get()->Children);
    usize focused_card_index = static_cast<usize>(focused_card.get() - cards.begin());

    nlc::unique_ptr<CardDisplayController> card_display_controller =
        nlc::make_unique<CardDisplayController>(alloc,
                                                alloc,
                                                cards,
                                                focused_card_index,
                                                *deck_tabs.get(),
                                                nlc::make_span(game_info.Decks),
                                                *back_widget);

    return (card_display_controller);
}

#ifndef NDEBUG
auto CardDisplayController::Validate(mdf::MenuData const & menu_descriptor,
                                     nlc::string_stream & error_stream) -> bool {
    static constexpr nlc::string_view validate_header = "CardDisplayController validator error : ";
    auto const * const main_container =
        FindComponentInDescriptor<ContainerWidgetCompDescriptor>(menu_descriptor.RootDescriptor);
    if (main_container == nullptr) {
        error_stream << validate_header << "no container in root widget\n";
        return (false);
    }
    bool validated = true;
    auto validate_error = [&validated, &error_stream](auto const & find_result,
                                                      nlc::string_view widget_name) {
        if (find_result.failed()) {
            validated = false;
            if (find_result.template failed_with<ErrorWidgetNotFound>())
                error_stream << validate_header << "no widget " << widget_name << "\n";
            else if (find_result.template failed_with<ErrorComponentNotFound>())
                error_stream << validate_header << "no component in widget " << widget_name << "\n";
            else
                nlc_assert_msg(false,
                               "unknow error from FindComponentInNamedWidget :",
                               find_result.get_error_name());
        }
    };

    auto const * const back_widget =
        FindWidgetInList(nlc::make_span(main_container->Children), back_button_name);

    if (back_widget == nullptr) {
        error_stream << validate_header << "no widget named " << back_button_name << "\n";
    }
    validated &= back_widget && ButtonController::Validate(*back_widget, error_stream);

    auto const card_container =
        FindComponentInNamedDescriptor<ContainerWidgetCompDescriptor>(nlc::make_span(
                                                                          main_container->Children),
                                                                      card_container_name);
    validate_error(card_container, card_container_name);
    auto const deck_tabs =
        FindComponentInNamedDescriptor<ContainerWidgetCompDescriptor>(nlc::make_span(
                                                                          main_container->Children),
                                                                      deck_tabs_name);
    validate_error(deck_tabs, deck_tabs_name);

    // Cards in CardContainer
    auto * const focused_card =
        FindWidgetInList(nlc::make_span(card_container.get()->Children), focused_card_name);

    if (focused_card == nullptr) {
        error_stream << validate_header << "no widget named " << focused_card_name
                     << " in card container\n";
    }
    validated &= focused_card != nullptr;
    for (auto const & child : card_container.get()->Children) {
        validated &= CardWidgetController::Validate(child, error_stream);
    }

    // Deck template
    if (deck_tabs.succeeded()) {
        auto const * const template_button =
            FindWidgetInList(nlc::make_span(deck_tabs.get()->Children), template_name);
        if (template_button == nullptr) {
            error_stream << validate_header << "no widget named " << deck_tabs_name
                         << " in deck types container\n";
        }
        validated &= template_button && ButtonController::Validate(*template_button, error_stream);
    }

    return (validated);
}
#endif

static auto FindDeckTemplateDescriptor(ContainerWidgetComponent & tabs)
    -> BaseWidgetDescriptor const & {
    auto const template_widget = FindWidgetInList(nlc::make_span(tabs.Children), template_name);
    nlc_assert(template_widget && template_widget->Descriptor);
    return (*(template_widget->Descriptor));
}

auto CardDisplayController::CreateBackButtonController(CardDisplayController & controller,
                                                       nlc::allocator & alloc,
                                                       BaseWidget & back_button_widget)
    -> ButtonController {
    ButtonController::WidgetToFunction button_data = {
        .Widget = back_button_widget,
        .Action = nlc::function<void()> { alloc, [&controller]() { controller.GoToMainMenu(); } }
    };
    return (ButtonController { alloc, nlc::span { button_data }, Inputs::AxisId::invalid() });
}

auto CardDisplayController::CreateDeckTabsController(nlc::allocator & alloc,
                                                     ContainerWidgetComponent & deck_tabs,
                                                     nlc::span<DeckDescriptor const> decks,
                                                     BaseWidgetDescriptor const & deck_tab_template,
                                                     CardDisplayController & controller,
                                                     Inputs::AxisId input) -> ButtonController {
    deck_tabs.Children.clear();
    deck_tabs.Children.reserve(decks.size());

    nlc::vector<ButtonController::WidgetToFunction> buttons(alloc);
    nlc::vector<nlc::pointer<BaseWidgetDescriptor const>> widgets(alloc);

    buttons.reserve(decks.size());
    widgets.reserve(decks.size());
    for ([[maybe_unused]] usize i : nlc::range(0, decks.size())) {
        widgets.append_no_grow(&deck_tab_template);
    }
    auto widget_span =
        nlc::span_cast<nlc::pointer<BaseWidgetDescriptor const> const>(make_span(widgets));
    PopulateContainerWidget(deck_tabs, widget_span, alloc);
    usize index = 0;
    for (auto & deck_widget : deck_tabs.Children) {
        ButtonWidgetComponent * const deck_button =
            FindComponentInWidget<ButtonWidgetComponent>(deck_widget);

        if (deck_button == nullptr)
            continue;

        UpdateButtonText(*deck_button, decks[index].Name);

        buttons.append_no_grow(deck_widget,
                               nlc::function<void()> { alloc, [&controller, index]() {
                                                          controller.SwitchDeckIndex(index);
                                                      } });
        ++index;
    }
    nlc_assert(index == decks.size());

    return { alloc, nlc::make_span(buttons), input };
}

CardDisplayController::CardDisplayController(nlc::allocator & alloc,
                                             nlc::span<BaseWidget> const cards,
                                             usize focused_card_index,
                                             ContainerWidgetComponent & deck_tabs,
                                             nlc::span<DeckDescriptor const> decks,
                                             BaseWidget & back_button)
    : BaseController(alloc)
    // Controls
    , CardDisplaySet(Inputs::create_input_set("CardDisplayControls"))
    , CardLeftRightId(Inputs::register_axis(CardDisplaySet, "right", "left", Inputs::AxisMode::preempt))
    , DeckLeftRightId(
          Inputs::register_axis(CardDisplaySet, "second_right", "second_left", Inputs::AxisMode::preempt))
    , ActionId(Inputs::register_input(CardDisplaySet,
                                      "action",
                                      [this](Inputs::Action action, GLFWwindow *) {
                                          ProcessAction(action);
                                      }))
    , ClickId(Inputs::register_input(CardDisplaySet,
                                     "click",
                                     [this](Inputs::Action action, GLFWwindow *) {
                                         ProcessMouseAction(action);
                                     }))
    // Deck desc
    , DeckDesc(nlc::move(decks))
    // Buttons
    , BackButtonWidget(back_button)
    , Buttons(CardDisplayController::CreateBackButtonController(*this, alloc, BackButtonWidget))
    // Deck & Cards
    , DeckTabTemplate(FindDeckTemplateDescriptor(deck_tabs))
    , DeckTabs(deck_tabs)
    , DeckTabsButtons(CreateDeckTabsController(alloc,
                                               DeckTabs,
                                               DeckDesc,
                                               DeckTabTemplate,
                                               *this,
                                               Inputs::AxisId::invalid()))
    , AllCards(cards)
    , CardControllers(alloc)
    , FocusedCardIndex(focused_card_index)
    , Allocator(alloc) {
    Inputs::set_active(CardDisplaySet, true);

    ActiveControllers.append(&Buttons);
    ActiveControllers.append(&DeckTabsButtons);

    CardControllers.reserve(AllCards.size());
    for (auto & card : AllCards) {
        CardControllers.append_no_grow(MakeCardWidgetController(Allocator, card));
    }
    SetCurrentCard(CurrentCardId);
}

CardDisplayController::~CardDisplayController() {
    Inputs::set_active(CardDisplaySet, false);
    Inputs::unregister_input(ClickId);
    Inputs::unregister_input(ActionId);
    Inputs::unregister_axis(CardLeftRightId);
    Inputs::unregister_axis(DeckLeftRightId);
    Inputs::destroy_input_set(CardDisplaySet);
}

auto CardDisplayController::ProcessInput() -> void {
    if (ShouldDiscardInput())
        return;

    BaseController::ProcessInput();

    usize const old_card_id = CurrentCardId;
    if (Inputs::get_value(CardLeftRightId) < 0.0f)  // left
    {
        CurrentCardId = static_cast<usize>(nlc::max(static_cast<int>(CurrentCardId) - 1, 0));
        AcknowInput();
    } else if (Inputs::get_value(CardLeftRightId) > 0.0f)  // right
    {
        CurrentCardId = nlc::min((CurrentCardId + 1), DeckDesc[CurrentDeckId].Cards.size() - 1);
        AcknowInput();
    }
    if (old_card_id != CurrentCardId)
        SetCurrentCard(CurrentCardId);
    usize const old_deck_id = CurrentDeckId;
    if (Inputs::get_value(DeckLeftRightId) < 0.0f)  // left
    {
        CurrentDeckId = static_cast<usize>(nlc::max(static_cast<int>(CurrentDeckId) - 1, 0));
        AcknowInput();
    } else if (Inputs::get_value(DeckLeftRightId) > 0.0f)  // right
    {
        CurrentDeckId = nlc::min((CurrentDeckId + 1), DeckDesc.size() - 1);
        AcknowInput();
    }
    if (old_deck_id != CurrentDeckId)
        SwitchDeckIndex(CurrentDeckId);
}

auto CardDisplayController::SetCurrentCard(usize index) -> void {
    CurrentCardId = index;
    auto const & current_deck = DeckDesc[CurrentDeckId];
    for (usize i = 0; auto & card_controller : CardControllers) {
        int card_index = static_cast<int>(CurrentCardId + (i - FocusedCardIndex));
        AllCards[i].IsDisplayable = false;
        if (card_index >= 0 && static_cast<usize>(card_index) < current_deck.Cards.size()) {
            AllCards[i].IsDisplayable = true;
            card_controller->SetDescriptor(current_deck.Cards[static_cast<usize>(card_index)]);
        }
        ++i;
    }
}

auto CardDisplayController::ProcessAction(Inputs::Action action) -> void {
    Buttons.ProcessAction(action);
    // DeckTabsButtons.ProcessAction(action);
}

auto CardDisplayController::ProcessMouseAction(Inputs::Action action) -> void {
    Buttons.ProcessMouseAction(action);
    DeckTabsButtons.ProcessMouseAction(action);
}

auto CardDisplayController::GoToMainMenu() -> void { TargetPage = MenuPage::MainMenu; }

auto CardDisplayController::SwitchDeckIndex(usize new_index) -> void {
    if (CurrentDeckId == new_index)
        return;
    CurrentDeckId = new_index;
    CurrentCardId = 0;
    SetCurrentCard(CurrentCardId);
}
