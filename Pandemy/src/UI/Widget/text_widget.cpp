#include "UI/Widget/text_widget.hpp"
#include "UI/Widget/base_widget_component.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/fundamentals/allocator.hpp>

#include <typeinfo>

usize const TextWidgetComponent::LocalTypeHash = typeid(TextWidgetComponent).hash_code();

auto TextWidgetCompDescriptor::CreateInstance(nlc::allocator & alloc) const
    -> nlc::unique_ptr<BaseWidgetComponent> {
    return (nlc::make_unique<TextWidgetComponent>(alloc, alloc, *this));
}

static auto CopyTextComponent(nlc::allocator & alloc,
                              TextWidgetComponent const & source,
                              TextWidgetComponent & target) -> void {
    target.Text = source.Text.copy(alloc);
    target.Color = source.Color;
    target.FontSize = source.FontSize;
}

TextWidgetComponent::TextWidgetComponent(nlc::allocator & alloc,
                                         TextWidgetCompDescriptor const & descriptor)
    : BaseWidgetComponent(LocalTypeHash,
                          { [](nlc::aabb2 screen_space, BaseWidgetComponent & source) {
                              source.AssertIsSameType<TextWidgetComponent>();
                              return (UpdateLayout(screen_space,
                                                   static_cast<TextWidgetComponent &>(source)));
                          } },
                          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
                              source.AssertIsSameType<TextWidgetComponent>();
                              Render(position, static_cast<TextWidgetComponent &>(source));
                          } },
                          { [](nlc::allocator & allocator,
                               BaseWidgetComponent const & source,
                               BaseWidgetComponent & target) {
                              target.AssertIsSameType<TextWidgetComponent>();
                              source.AssertCopyIsSameType<TextWidgetComponent>();
                              CopyTextComponent(allocator,
                                                static_cast<TextWidgetComponent const &>(source),
                                                static_cast<TextWidgetComponent &>(target));
                          } }
#ifndef NDEBUG
                          ,
                          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
                              source.AssertIsSameType<TextWidgetComponent>();
                              DebugRender(position, static_cast<TextWidgetComponent &>(source));
                          } }
#endif
                          )
    , Descriptor(descriptor)
    , Text(alloc, descriptor.Text)
    , Color(descriptor.Color)
    , FontSize(descriptor.FontSize) {
}
