#include "UI/Widget/widget_updater.hpp"

#include "UI/Widget/alignment.hpp"
#include "UI/Widget/button_widget.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/Widget/grid_widget.hpp"
#include "UI/Widget/layout.hpp"
#include "UI/Widget/text_widget.hpp"
#include "UI/Widget/texture_widget.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/maths/simple_maths.hpp>
#include <nlc/maths/vec2_common.hpp>

#include <graphic_engine/font_types.hpp>
#include <graphic_engine/renderer_text.hpp>

auto UpdateLayout(nlc::aabb2 const render_space, BaseWidget & widget) -> LayoutSizes {
    nlc_assert_msg(widget.Descriptor, "Updating a widget with no descriptor", nlc_dump_var(widget.Name));
    if (widget.IsStatic) {
        if (widget.Descriptor->PositionSpace == BaseWidgetDescriptor::ScreenSpace::Absolute &&
            widget.Descriptor->SizeSpace == BaseWidgetDescriptor::ScreenSpace::Absolute)
            widget.ScreenArea = widget.Descriptor->ScreenArea;
        else {
            if (widget.Descriptor->PositionSpace == BaseWidgetDescriptor::ScreenSpace::Absolute) {
                widget.ScreenArea.min = widget.Descriptor->ScreenArea.min;
            } else {
                widget.ScreenArea.min =
                    nlc::lerp(render_space.min, render_space.max, widget.Descriptor->ScreenArea.min);
            }
            if (widget.Descriptor->SizeSpace == BaseWidgetDescriptor::ScreenSpace::Absolute) {
                widget.ScreenArea.max = widget.ScreenArea.min + widget.Descriptor->ScreenArea.max;
            } else {
                widget.ScreenArea.max =
                    nlc::lerp(render_space.min, render_space.max, widget.Descriptor->ScreenArea.max);
            }
        }
    }
    if (widget.Components.is_empty()) {
        return { widget.ScreenArea.size(), widget.ScreenArea.size() };
    }
    f32_2 constrained_size { 0.0f, 0.0f };
    f32_2 opti_size { 0.0f, 0.0f };
    for (nlc::unique_ptr<BaseWidgetComponent> & comp : widget.Components) {
        LayoutSizes const comp_size = comp->LayoutUpdater(widget.ScreenArea, *(comp.get()));
        constrained_size = nlc::max(comp_size.ConstrainedSize, constrained_size);
        opti_size = nlc::max(comp_size.OptimalSize, opti_size);
    }
    return { .ConstrainedSize = ((widget.IsStatic) ? constrained_size : widget.ScreenArea.size()),
             .OptimalSize = ((widget.IsStatic) ? opti_size : widget.ScreenArea.size()) };
}

static auto ComputeConstrainedSize(f32_2 const opti_size, f32_2 const render_size) -> f32_2 {
    f32_2 const ratio = render_size / opti_size;

    return (nlc::min(ratio.x, ratio.y) * opti_size);
}

auto UpdateLayout(nlc::aabb2 const render_space, TextWidgetComponent & text_component) -> LayoutSizes {
    rtxt::FontMetrics const & font_metrics = rtxt::get_font_metrics(text_component.Descriptor.FontId);

    f32_2 text_bbox = rtxt::compute_text_bbox(text_component.Text,
                                              text_component.Descriptor.FontId,
                                              text_component.FontSize,
                                              rtxt::TargetSpace::screenspace)
                          .size();
    text_bbox.y = (font_metrics.ascender - font_metrics.descender) * text_component.FontSize;
    return { .ConstrainedSize = ComputeConstrainedSize(text_bbox, render_space.size()),
             .OptimalSize = text_bbox };
}

enum class SizeType : u8 {
    Constrained = 0,
    Optimal = 1,
};

struct LayoutData {
    ListLayout Layout;
    HorizontalLayoutOrdering HOrdering;
    VerticalLayoutOrdering VOrdering;
    FillPolicy Fill;
    SizeType SizeToUse;
};

static auto ComputeListElementSize(nlc::aabb2 const render_space,
                                   LayoutData const & layout,
                                   usize child_count,
                                   BaseWidget & child) -> LayoutSizes {
    switch (layout.Fill) {
        case FillPolicy::Fill: {
            usize_2 const scale_factor = (layout.Layout == ListLayout::Horizontal)
                                             ? usize_2 { child_count, 1 }
                                             : usize_2 { 1, child_count };
            f32_2 const scale = render_space.size() / f32_2::cast(scale_factor);
            nlc::aabb2 const new_render_space = nlc::aabb2::from_pos_and_size(render_space.min, scale);
            return { .ConstrainedSize = UpdateLayout(new_render_space, child).ConstrainedSize,
                     .OptimalSize = scale };
        }
        case FillPolicy::FitChildrenSize: return UpdateLayout(render_space, child);
    }
    unreachable;
}

// /!\ Possible improvement
// Should manage padding with a padding ID list as argument
// Paddings has fixed size
// so each compute list element size should receive as render space
// Full size - (padding size * padding number)
// padding should be place with fixed size
static auto UpdateHorizontalLayout(nlc::aabb2 const render_space,
                                   LayoutData const & layout,
                                   nlc::span<BaseWidget> children,
                                   usize slot_number) -> LayoutSizes {
    f32_2 constrained_filled_size { 0.0f, 0.0f };
    f32_2 opti_filled_size { 0.0f, 0.0f };
    float & width_offset = (layout.SizeToUse == SizeType::Constrained) ? constrained_filled_size.x
                                                                       : opti_filled_size.x;
    auto const h_align = layout.HOrdering;
    auto const v_align = layout.VOrdering;

    if (h_align == HorizontalLayoutOrdering::NoOrder)
        slot_number = 1;
    for (usize i = 0, size = children.size(); i < size; ++i) {
        nlc::aabb2 target_space = render_space;
        BaseWidget & child =
            children[(h_align == HorizontalLayoutOrdering::RightToLeft) ? size - i - 1 : i];
        LayoutSizes const child_size =
            ComputeListElementSize(render_space, layout, slot_number, child);
        f32_2 const element_used_size = (layout.SizeToUse == SizeType::Constrained)
                                            ? child_size.ConstrainedSize
                                            : child_size.OptimalSize;
        switch (h_align) {
            case HorizontalLayoutOrdering::LeftToRight:
                target_space.min.x = render_space.min.x + width_offset;
                target_space.max.x = target_space.min.x + element_used_size.x;
                break;
            case HorizontalLayoutOrdering::RightToLeft:
                target_space.max.x = render_space.max.x - width_offset;
                target_space.min.x = target_space.max.x - element_used_size.x;
                break;
            case HorizontalLayoutOrdering::NoOrder: break;
        }
        switch (v_align) {
            case VerticalLayoutOrdering::TopToBottom:
                target_space.min.y = render_space.min.y;
                target_space.max.y = target_space.min.y + element_used_size.y;
                break;
            case VerticalLayoutOrdering::BottomToTop:
                target_space.max.y = render_space.max.y;
                target_space.min.y = target_space.max.y - element_used_size.y;
                break;
            case VerticalLayoutOrdering::NoOrder: break;
        }
        UpdateLayout(target_space, child);
        if (h_align != HorizontalLayoutOrdering::NoOrder) {
            opti_filled_size.x += child_size.OptimalSize.x;
            constrained_filled_size.x += child_size.ConstrainedSize.x;
        } else {
            opti_filled_size.x = nlc::max(opti_filled_size.x, child_size.OptimalSize.x);
            constrained_filled_size.x =
                nlc::max(constrained_filled_size.x, child_size.ConstrainedSize.x);
        }
        opti_filled_size.y = nlc::max(opti_filled_size.y, child_size.OptimalSize.y);
        constrained_filled_size.y = nlc::max(constrained_filled_size.y, child_size.ConstrainedSize.y);
    }
    return { .ConstrainedSize = constrained_filled_size, .OptimalSize = opti_filled_size };
}

static auto UpdateVerticalLayout(nlc::aabb2 const render_space,
                                 LayoutData const & layout,
                                 nlc::span<BaseWidget> children,
                                 usize slot_number) -> LayoutSizes {
    f32_2 constrained_filled_size { 0.0f, 0.0f };
    f32_2 opti_filled_size { 0.0f, 0.0f };
    auto const h_order = layout.HOrdering;
    auto const v_order = layout.VOrdering;

    if (v_order == VerticalLayoutOrdering::NoOrder)
        slot_number = 1;
    for (usize i = 0, size = children.size(); i < size; ++i) {
        nlc::aabb2 target_space = render_space;
        BaseWidget & child =
            children[(v_order == VerticalLayoutOrdering::BottomToTop) ? size - i - 1 : i];
        LayoutSizes const child_size =
            ComputeListElementSize(render_space, layout, slot_number, child);
        switch (h_order) {
            case HorizontalLayoutOrdering::LeftToRight:
                target_space.min.x = render_space.min.x;
                target_space.max.x = target_space.min.x + child_size.OptimalSize.x;
                break;
            case HorizontalLayoutOrdering::RightToLeft:
                target_space.max.x = render_space.max.x;
                target_space.min.x = target_space.max.x - child_size.OptimalSize.x;
                break;
            case HorizontalLayoutOrdering::NoOrder: break;
        }
        switch (v_order) {
            case VerticalLayoutOrdering::TopToBottom:
                target_space.min.y = render_space.min.y + opti_filled_size.y;
                target_space.max.y = target_space.min.y + child_size.OptimalSize.y;
                break;
            case VerticalLayoutOrdering::BottomToTop:
                target_space.max.y = render_space.max.y - opti_filled_size.y;
                target_space.min.y = target_space.max.y - child_size.OptimalSize.y;
                break;
            case VerticalLayoutOrdering::NoOrder: break;
        }
        UpdateLayout(target_space, child);
        if (v_order != VerticalLayoutOrdering::NoOrder) {
            opti_filled_size.y += child_size.OptimalSize.y;
            constrained_filled_size.y += child_size.ConstrainedSize.y;
        } else {
            opti_filled_size.y = nlc::max(opti_filled_size.y, child_size.OptimalSize.y);
            constrained_filled_size.y =
                nlc::max(constrained_filled_size.y, child_size.ConstrainedSize.y);
        }
        opti_filled_size.x = nlc::max(opti_filled_size.x, child_size.OptimalSize.x);
        constrained_filled_size.x = nlc::max(constrained_filled_size.x, child_size.ConstrainedSize.x);
    }
    return { .ConstrainedSize = constrained_filled_size, .OptimalSize = opti_filled_size };
}

auto UpdateLayout(nlc::aabb2 const render_space, ContainerWidgetComponent & container_widget)
    -> LayoutSizes {
    LayoutData const container_layout {
        container_widget.Descriptor.Layout,
        container_widget.Descriptor.HorizontalOrder,
        container_widget.Descriptor.VerticalOrder,
        container_widget.Descriptor.Fill,
        SizeType::Optimal,
    };
    switch (container_widget.Descriptor.Layout) {
        case ListLayout::Vertical: {
            return (UpdateVerticalLayout(render_space,
                                         container_layout,
                                         nlc::make_span(container_widget.Children),
                                         container_widget.Children.size()));
        }
        case ListLayout::Horizontal: {
            return (UpdateHorizontalLayout(render_space,
                                           container_layout,
                                           nlc::make_span(container_widget.Children),
                                           container_widget.Children.size()));
        }
    }
    unreachable;
}

auto UpdateLayout(nlc::aabb2 const render_space, GridWidgetComponent & grid_widget) -> LayoutSizes {
    f32_2 constrained_filled_size { 0.0f, 0.0f };
    f32_2 opti_filled_size { 0.0f, 0.0f };
    LayoutData const grid_layout_data {
        grid_widget.Descriptor.MainDimension,
        grid_widget.Descriptor.HOrdering,
        grid_widget.Descriptor.VOrdering,
        grid_widget.Descriptor.Fill,
        SizeType::Constrained,
    };
    nlc_assert(grid_widget.Descriptor.MainDimension == ListLayout::Horizontal);
    usize const lines = (grid_widget.GetElementCount() + grid_widget.Descriptor.MaxColumns - 1) /
                        grid_widget.Descriptor.MaxColumns;
    float const line_height =
        render_space.size().y / static_cast<float>(grid_widget.Descriptor.MaxLines);
    usize start_index = 0u;
    for (usize i = 0; i < lines; ++i) {
        nlc::aabb2 line_space { render_space };

        // as top to bottom we only advence from line to line
        line_space.min.y += constrained_filled_size.y;
        line_space.max.y = line_space.min.y + line_height;

        usize const total_line_children =
            (grid_widget.Descriptor.HorizontalPaddingDescriptor != nlc::null)
                ? (grid_widget.Descriptor.MaxColumns * 2 - 1)
                : (grid_widget.Descriptor.MaxColumns);

        LayoutSizes sizes =
            UpdateHorizontalLayout(line_space,
                                   grid_layout_data,
                                   nlc::span<BaseWidget>(grid_widget.Children.begin() + start_index,
                                                         nlc::min(total_line_children,
                                                                  grid_widget.Children.size() -
                                                                      start_index)),
                                   grid_widget.Descriptor.MaxColumns);
        constrained_filled_size += sizes.ConstrainedSize;
        opti_filled_size += sizes.OptimalSize;
        start_index += total_line_children;
        if (grid_widget.Descriptor.VerticalPaddingDescriptor != nlc::null && i != lines - 1) {
            // niques
            ++start_index;
        }
    }
    return { .ConstrainedSize = constrained_filled_size, .OptimalSize = opti_filled_size };
}

auto UpdateLayout(nlc::aabb2 const render_space, ButtonWidgetComponent & button_widget) -> LayoutSizes {
    return UpdateLayout(render_space,
                        button_widget.StatusWidgets[static_cast<u8>(button_widget.CurrentStatus)]);
}

auto UpdateLayout(nlc::aabb2 const render_space, TextureWidgetComponent & texture_widget)
    -> LayoutSizes {
    return { .ConstrainedSize = (texture_widget.Descriptor.FillMode == TextureFillMode::KeepRatio)
                                    ? ComputeConstrainedSize(texture_widget.TextureView.TextureSize,
                                                             render_space.size())
                                    : render_space.size(),
             .OptimalSize = texture_widget.TextureView.TextureSize };
}
