#include "UI/Widget/base_widget.hpp"

#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>

#include <nlc/maths/vec2.hpp>

BaseWidget::ID BaseWidget::CurrentId = static_cast<BaseWidget::ID>(0u);

auto BaseWidgetDescriptor::CreateInstance(nlc::allocator & alloc) const -> BaseWidget {
    BaseWidget widget(alloc, *this);

    widget.Components.reserve(Components.size());
    for (auto const & component : Components) {
        widget.Components.append(component->CreateInstance(alloc));
    }

    return (widget);
}

auto MoveWidget(BaseWidget & widget_to_move, f32_2 new_position) -> void {
    widget_to_move.IsStatic = false;
    f32_2 size = widget_to_move.ScreenArea.size();
    widget_to_move.ScreenArea.min = new_position - size / 2.0f;
    widget_to_move.ScreenArea.max = widget_to_move.ScreenArea.min + size;
}

auto DuplicateWidget(nlc::allocator & alloc, BaseWidget const & source) -> BaseWidget {
    BaseWidget target = source.Descriptor->CreateInstance(alloc);
    nlc_assert_msg(target.Components.size() == source.Components.size(),
                   "Duplicate only works on widget with the same components.");
    for (usize i = 0; i < target.Components.size(); ++i) {
        target.Components[i]->CopyComponentData(alloc, *(source.Components[i]), *(target.Components[i]));
    }
    return (target);
}
