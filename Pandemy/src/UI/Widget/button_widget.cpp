#include "UI/Widget/button_widget.hpp"

#include "UI/Widget/text_widget.hpp"
#include "UI/Widget/widget_renderer.hpp"
#include "UI/Widget/widget_updater.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/range.hpp>

#include <graphic_engine/graphic_engine.hpp>

#include <typeinfo>

usize const ButtonWidgetComponent::LocalTypeHash = typeid(ButtonWidgetComponent).hash_code();

auto ButtonWidgetCompDescriptor::CreateInstance(nlc::allocator & alloc) const
    -> nlc::unique_ptr<BaseWidgetComponent> {
    auto button = nlc::make_unique<ButtonWidgetComponent>(alloc, alloc, *this);

    button->StatusWidgets[static_cast<u8>(ButtonWidgetStatus::Idle)] =
        IdleWidget.CreateInstance(alloc);
    button->StatusWidgets[static_cast<u8>(ButtonWidgetStatus::Selected)] =
        SelectedWidget.CreateInstance(alloc);
    button->StatusWidgets[static_cast<u8>(ButtonWidgetStatus::Pressed)] =
        PressedWidget.CreateInstance(alloc);
    button->StatusWidgets[static_cast<u8>(ButtonWidgetStatus::Disabled)] =
        DisabledWidget.CreateInstance(alloc);
    return (button);
}

static auto CopyButton(nlc::allocator & alloc,
                       ButtonWidgetComponent const & source,
                       ButtonWidgetComponent & target) -> void {
    for (auto i : nlc::range(0, static_cast<u8>(ButtonWidgetStatus::SIZE))) {
        target.StatusWidgets[i].~BaseWidget();
        new (&target.StatusWidgets[i]) BaseWidget(alloc);
        target.StatusWidgets[i] = source.StatusWidgets[i].Descriptor->CreateInstance(alloc);
    }
}

ButtonWidgetComponent::ButtonWidgetComponent(nlc::allocator & alloc,
                                             ButtonWidgetCompDescriptor const & descriptor)
    : BaseWidgetComponent(LocalTypeHash,
                          { [](nlc::aabb2 screen_space, BaseWidgetComponent & source) {
                              source.AssertIsSameType<ButtonWidgetComponent>();
                              return (UpdateLayout(screen_space,
                                                   static_cast<ButtonWidgetComponent &>(source)));
                          } },
                          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
                              source.AssertIsSameType<ButtonWidgetComponent>();
                              Render(position, static_cast<ButtonWidgetComponent &>(source));
                          } },
                          { [](nlc::allocator & allocator,
                               BaseWidgetComponent const & source,
                               BaseWidgetComponent & target) {
                              target.AssertIsSameType<ButtonWidgetComponent>();
                              source.AssertCopyIsSameType<ButtonWidgetComponent>();
                              CopyButton(allocator,
                                         static_cast<ButtonWidgetComponent const &>(source),
                                         static_cast<ButtonWidgetComponent &>(target));
                          } }
#ifndef NDEBUG
                          ,
                          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
                              DebugRender(position, static_cast<ButtonWidgetComponent &>(source));
                          } }
#endif
                          )
    , Descriptor(descriptor)
    , CurrentStatus(descriptor.DefaultStatus)
    , Allocator(&alloc) {
    for (auto i : nlc::range(0, static_cast<u8>(ButtonWidgetStatus::SIZE))) {
        new (&StatusWidgets[i]) BaseWidget(*Allocator);
    }
}

ButtonWidgetComponent::ButtonWidgetComponent(ButtonWidgetComponent && rhs)
    : BaseWidgetComponent(LocalTypeHash,
                          nlc::move(rhs.LayoutUpdater),
                          nlc::move(rhs.Renderer),
                          nlc::move(rhs.CopyComponentData)
#ifndef NDEBUG
                              ,
                          nlc::move(rhs.DebugRenderer)
#endif
                              )
    , Descriptor(nlc::move(rhs.Descriptor))
    , Allocator(rhs.Allocator) {
    for (auto i : nlc::range(0, static_cast<u8>(ButtonWidgetStatus::SIZE))) {
        new (&StatusWidgets[i]) BaseWidget(*Allocator);
        StatusWidgets[i] = nlc::move(rhs.StatusWidgets[i]);
    }
}

ButtonWidgetComponent::~ButtonWidgetComponent() {
    for (auto i : nlc::range(0, static_cast<u8>(ButtonWidgetStatus::SIZE))) {
        StatusWidgets[i].~BaseWidget();
    }
}

// UTILS

auto UpdateButtonText(ButtonWidgetComponent & button, nlc::string_view new_text) -> void {
    for (auto i : nlc::range(0, static_cast<u8>(ButtonWidgetStatus::SIZE))) {
        TextWidgetComponent * const button_text_component =
            FindComponentInWidget<TextWidgetComponent>(button.StatusWidgets[i]);
        if (button_text_component == nullptr)
            continue;
        button_text_component->Text = nlc::string(*button.Allocator, new_text);
    }
}
