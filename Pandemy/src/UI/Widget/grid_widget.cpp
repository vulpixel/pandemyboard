#include "UI/Widget/grid_widget.hpp"
#include "UI/Widget/base_widget_component.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>

#include <typeinfo>

usize const GridWidgetComponent::LocalTypeHash = typeid(GridWidgetComponent).hash_code();

auto GridWidgetCompDescriptor::CreateInstance(nlc::allocator & alloc) const
    -> nlc::unique_ptr<BaseWidgetComponent> {
    auto grid = nlc::make_unique<GridWidgetComponent>(alloc, alloc, *this);

    // PopulateGridWidget(*grid, nlc::make_span(Children), alloc);
    return (grid);
}

static void CopyGrid(nlc::allocator & alloc,
                     GridWidgetComponent const & source,
                     GridWidgetComponent & target) {
    target.Children.clear();
    target.Children.reserve(source.Children.size());
    for (auto const & child : source.Children) {
        target.Children.append_no_grow(DuplicateWidget(alloc, child));
    }
}

GridWidgetComponent::GridWidgetComponent(nlc::allocator & alloc,
                                         GridWidgetCompDescriptor const & descriptor)
    : BaseWidgetComponent(LocalTypeHash,
                          { [](nlc::aabb2 screen_space, BaseWidgetComponent & source) {
                              source.AssertIsSameType<GridWidgetComponent>();
                              return (UpdateLayout(screen_space,
                                                   static_cast<GridWidgetComponent &>(source)));
                          } },
                          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
                              source.AssertIsSameType<GridWidgetComponent>();
                              Render(position, static_cast<GridWidgetComponent &>(source));
                          } },
                          { [](nlc::allocator & allocator,
                               BaseWidgetComponent const & source,
                               BaseWidgetComponent & target) {
                              target.AssertIsSameType<GridWidgetComponent>();
                              source.AssertCopyIsSameType<GridWidgetComponent>();
                              CopyGrid(allocator,
                                       static_cast<GridWidgetComponent const &>(source),
                                       static_cast<GridWidgetComponent &>(target));
                          } }
#ifndef NDEBUG
                          ,
                          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
                              source.AssertIsSameType<GridWidgetComponent>();
                              DebugRender(position, static_cast<GridWidgetComponent &>(source));
                          } }
#endif
                          )
    , Descriptor(descriptor)
    , Children(alloc)
    , PaddingIds(alloc) {
}
GridWidgetComponent::GridWidgetComponent(GridWidgetComponent && rhs)
    : BaseWidgetComponent(LocalTypeHash,
                          nlc::move(rhs.LayoutUpdater),
                          nlc::move(rhs.Renderer),
                          nlc::move(rhs.CopyComponentData)
#ifndef NDEBUG
                              ,
                          nlc::move(rhs.DebugRenderer)
#endif
                              )
    , Descriptor(nlc::move(rhs.Descriptor))
    , Children(nlc::move(rhs.Children))
    , PaddingIds(nlc::move(rhs.PaddingIds)) {
}

[[maybe_unused]] static auto RemoveWidgetFromIds(nlc::vector<BaseWidget> & widgets,
                                                 nlc::span<BaseWidget::ID const> const ids) -> void {
    for (auto const id : ids) {
        auto const * const element =
            nlc::find_if(nlc::make_span(widgets),
                         [id](BaseWidget const & widget) { return (id == widget.GetId()); });
        if (element)
            widgets.remove_ordered(static_cast<usize>(element - widgets.begin()));
    }
}

auto GridElementIsPadding(GridWidgetComponent const & grid, BaseWidget const & element) -> bool {
    return (nlc::find_if(nlc::make_span(grid.PaddingIds), [&element](auto const padding_id) {
        return (padding_id == element.GetId());
    }));
}
