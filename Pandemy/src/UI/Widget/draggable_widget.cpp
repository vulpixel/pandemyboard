#include "UI/Widget/draggable_widget.hpp"
#include "UI/Widget/layout_sizes.hpp"

#include <typeinfo>

usize const DraggableWidgetComponent::LocalTypeHash = typeid(DraggableWidgetComponent).hash_code();

auto DraggableWidgetCompDescriptor::CreateInstance(nlc::allocator & alloc) const
    -> nlc::unique_ptr<BaseWidgetComponent> {
    return (nlc::make_unique<DraggableWidgetComponent>(alloc, alloc, *this));
}

DraggableWidgetComponent::DraggableWidgetComponent(nlc::allocator &,
                                                   DraggableWidgetCompDescriptor const &)
    : BaseWidgetComponent(LocalTypeHash,
                          { [](nlc::aabb2, BaseWidgetComponent &) {
                              return LayoutSizes { { 0.0f, 0.0f }, { 0.0f, 0.0f } };
                          } },
                          { [](nlc::aabb2, BaseWidgetComponent &) {} },
                          { [](nlc::allocator &, BaseWidgetComponent const &, BaseWidgetComponent &) {} }
#ifndef NDEBUG
                          ,
                          { [](nlc::aabb2, BaseWidgetComponent &) {} }
#endif
      ) {
}
