#include "UI/Widget/base_widget.hpp"
#include "UI/Widget/button_widget.hpp"
#include "UI/Widget/container_widget.hpp"
#include "UI/Widget/grid_widget.hpp"
#include "UI/Widget/text_widget.hpp"
#include "UI/Widget/texture_widget.hpp"

#include <nlc/maths/float_cmp.hpp>

#include <graphic_engine/font_types.hpp>
#include <graphic_engine/graphic_engine.hpp>
#include <graphic_engine/ids.hpp>
#include <graphic_engine/renderer_text.hpp>

#ifndef NDEBUG
#    include <imgui.h>

#    include <glad/glad.h>
#    include <graphic_engine/ogl_shader.hpp>
#    include <graphic_engine/renderer_debug.hpp>
#endif

void Render(BaseWidget const & widget) {
    if (widget.IsDisplayable == false)
        return;
    for (nlc::unique_ptr<BaseWidgetComponent> const & comp : widget.Components) {
        comp->Renderer(widget.ScreenArea, *(comp.get()));
    }
}

// Comp render
void Render(nlc::aabb2 const render_space, TextWidgetComponent const & text_widget) {
    rtxt::FontMetrics const & font_metrics = rtxt::get_font_metrics(text_widget.Descriptor.FontId);

    float const text_width = rtxt::compute_text_bbox(text_widget.Text,
                                                     text_widget.Descriptor.FontId,
                                                     text_widget.FontSize,
                                                     rtxt::TargetSpace::screenspace)
                                 .size()
                                 .x;
    float const descender = font_metrics.descender * text_widget.FontSize;
    float const ascender = font_metrics.ascender * text_widget.FontSize;
    float const text_height = ascender - descender;

    f32_2 const draw_pos {
        [=, h_align = text_widget.Descriptor.HorizontalAlign] {
            switch (h_align) {
                case HorizontalAlignment::Center:
                    return (render_space.min.x + (render_space.size().x - text_width) * 0.5f);
                case HorizontalAlignment::Left: return (render_space.min.x);
                case HorizontalAlignment::Right: return (render_space.max.x - text_width);
            }
            unreachable;
        }(),
        [=, v_align = text_widget.Descriptor.VerticalAlign] {
            switch (v_align) {
                case VerticalAlignment::Center:
                    return (render_space.min.y + (render_space.size().y - text_height) * 0.5f) +
                           ascender;
                case VerticalAlignment::Top: return (render_space.min.y + ascender);
                case VerticalAlignment::Bottom: return (render_space.max.y + descender);
            }
            unreachable;
        }()
    };

    rtxt::draw_text({ text_widget.Color,
                      draw_pos,
                      text_widget.Text,
                      text_widget.FontSize,
                      text_widget.Descriptor.FontId },
                    rtxt::TargetSpace::screenspace);
}

void Render([[maybe_unused]] nlc::aabb2 const render_space,
            ContainerWidgetComponent const & container_widget) {
    for (BaseWidget const & child : container_widget.Children) {
        Render(child);
    }
}

void Render([[maybe_unused]] nlc::aabb2 const render_space, GridWidgetComponent const & grid_widget) {
    for (BaseWidget const & child : grid_widget.Children) {
        Render(child);
    }
}

void Render([[maybe_unused]] nlc::aabb2 const render_space,
            ButtonWidgetComponent const & button_widget) {
    Render(button_widget.StatusWidgets[static_cast<u8>(button_widget.CurrentStatus)]);
}

void Render(nlc::aabb2 const render_space, TextureWidgetComponent const & texture_widget) {
    nlc_assert_msg(texture_widget.TextureView.IsValid(), "Trying to render an invalid AnimId");
    f32_2 scale = render_space.max - render_space.min;

    if (texture_widget.Descriptor.FillMode == TextureFillMode::KeepRatio) {
        float const dest_aspect = scale.x / scale.y;

        if (texture_widget.TextureView.AspectRatio > dest_aspect) {
            scale.y = scale.x / texture_widget.TextureView.AspectRatio;
        } else {
            scale.x = scale.y * texture_widget.TextureView.AspectRatio;
        }

        nlc_assert_msg(nlc::are_floats_close_ulp_wise(scale.x / scale.y,
                                                      texture_widget.TextureView.AspectRatio,
                                                      4),
                       "Aspect ratio failed to be kept. Result: ",
                       nlc_dump_var(scale.x / scale.y),
                       nlc_dump_var(texture_widget.TextureView.AspectRatio));
    }

    ge::append_actor({ .position = render_space.min + (render_space.max - render_space.min) * 0.5f,
                       .scale = scale,
                       .color = { 1.0f, 1.0f, 1.0f, 1.0f },
                       .animation_frame = texture_widget.TextureView.AnimId,
                       .orientation = nlc::rm_unit(texture_widget.Rotation),
                       .layer = texture_widget.Layer });
}

#ifndef NDEBUG
void DebugRender(BaseWidget const & Widget) {
    auto * viewport = ImGui::GetMainViewport();
    ImDrawList * const draw_list = ImGui::GetBackgroundDrawList(viewport);
    if (draw_list) {
        ImU32 const color = ImColor(ImVec4(0.0f, 1.0f, 0.0f, 1.0f));
        draw_list->AddRect(reinterpret_cast<ImVec2 const &>(Widget.ScreenArea.min),
                           reinterpret_cast<ImVec2 const &>(Widget.ScreenArea.max),
                           color);
    }
    for (nlc::unique_ptr<BaseWidgetComponent> const & comp : Widget.Components) {
        comp->DebugRenderer(Widget.ScreenArea, *(comp.get()));
    }
}

// Comp render
void DebugRender(nlc::aabb2 const render_space, TextWidgetComponent const & text_widget) {
    rdbg::draw_text(text_widget.Text,
                    render_space.min,
                    text_widget.Color,
                    text_widget.FontSize,
                    rtxt::TargetSpace::screenspace);
}

void DebugRender([[maybe_unused]] nlc::aabb2 const render_space,
                 ContainerWidgetComponent const & container_widget) {
    for (BaseWidget const & child : container_widget.Children) {
        DebugRender(child);
    }
}

void DebugRender([[maybe_unused]] nlc::aabb2 const render_space,
                 GridWidgetComponent const & grid_widget) {
    for (BaseWidget const & child : grid_widget.Children) {
        DebugRender(child);
    }
}

void DebugRender([[maybe_unused]] nlc::aabb2 const render_space,
                 ButtonWidgetComponent const & button_widget) {
    static u8 status = 0;
    static constexpr u8 period = 60;
    static constexpr u8 size = static_cast<u8>(ButtonWidgetStatus::SIZE) * period;
    DebugRender(button_widget.StatusWidgets[status / period]);
    status = static_cast<u8>((status + 1) % size);
}

void DebugRender(nlc::aabb2 const, TextureWidgetComponent const &) {}

#endif
