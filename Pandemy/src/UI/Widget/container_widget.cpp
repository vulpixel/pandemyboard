#include "UI/Widget/container_widget.hpp"
#include "UI/Widget/base_widget_component.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>

#include <typeinfo>

usize const ContainerWidgetComponent::LocalTypeHash = typeid(ContainerWidgetComponent).hash_code();

auto ContainerWidgetCompDescriptor::CreateInstance(nlc::allocator & alloc) const
    -> nlc::unique_ptr<BaseWidgetComponent> {
    auto container = nlc::make_unique<ContainerWidgetComponent>(alloc, alloc, *this);

    PopulateContainerWidget(*container, nlc::make_span(Children), alloc);
    return (container);
}

static void CopyContainer(nlc::allocator & alloc,
                          ContainerWidgetComponent const & source,
                          ContainerWidgetComponent & target) {
    target.Children.clear();
    target.Children.reserve(source.Children.size());
    for (auto const & child : source.Children) {
        target.Children.append_no_grow(DuplicateWidget(alloc, child));
    }
}

ContainerWidgetComponent::ContainerWidgetComponent(nlc::allocator & alloc,
                                                   ContainerWidgetCompDescriptor const & descriptor)
    : BaseWidgetComponent(LocalTypeHash,
                          { [](nlc::aabb2 screen_space, BaseWidgetComponent & source) {
                              source.AssertIsSameType<ContainerWidgetComponent>();
                              return (UpdateLayout(screen_space,
                                                   static_cast<ContainerWidgetComponent &>(source)));
                          } },
                          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
                              source.AssertIsSameType<ContainerWidgetComponent>();
                              Render(position, static_cast<ContainerWidgetComponent &>(source));
                          } },
                          { [](nlc::allocator & allocator,
                               BaseWidgetComponent const & source,
                               BaseWidgetComponent & target) {
                              target.AssertIsSameType<ContainerWidgetComponent>();
                              source.AssertCopyIsSameType<ContainerWidgetComponent>();
                              CopyContainer(allocator,
                                            static_cast<ContainerWidgetComponent const &>(source),
                                            static_cast<ContainerWidgetComponent &>(target));
                          } }
#ifndef NDEBUG
                          ,
                          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
                              source.AssertIsSameType<ContainerWidgetComponent>();
                              DebugRender(position, static_cast<ContainerWidgetComponent &>(source));
                          } }
#endif
                          )
    , Descriptor(descriptor)
    , Children(alloc) {
}
ContainerWidgetComponent::ContainerWidgetComponent(ContainerWidgetComponent && rhs)
    : BaseWidgetComponent(LocalTypeHash,
                          nlc::move(rhs.LayoutUpdater),
                          nlc::move(rhs.Renderer),
                          nlc::move(rhs.CopyComponentData)
#ifndef NDEBUG
                              ,
                          nlc::move(rhs.DebugRenderer)
#endif
                              )
    , Descriptor(nlc::move(rhs.Descriptor))
    , Children(nlc::move(rhs.Children)) {
}
