#include "UI/Widget/texture_widget.hpp"
#include "UI/Widget/base_widget_component.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/optional.hpp>

#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/report_error_to_user.hpp>
#include <nlc/fundamentals/string_stream.hpp>

#include <typeinfo>

usize const TextureWidgetComponent::LocalTypeHash = typeid(TextureWidgetComponent).hash_code();

auto TextureWidgetCompDescriptor::CreateInstance(nlc::allocator & alloc) const
    -> nlc::unique_ptr<BaseWidgetComponent> {
    return (nlc::make_unique<TextureWidgetComponent>(alloc, alloc, *this));
}

static auto CopyTextureComponent([[maybe_unused]] nlc::allocator & alloc,
                                 TextureWidgetComponent const & source,
                                 TextureWidgetComponent & target) {
    target.TextureView = source.TextureView;
    target.Rotation = source.Rotation;
}

TextureWidgetComponent::TextureWidgetComponent(nlc::allocator &,
                                               TextureWidgetCompDescriptor const & descriptor)
    : BaseWidgetComponent(
          LocalTypeHash,
          { [](nlc::aabb2 screen_space, BaseWidgetComponent & source) {
              source.AssertIsSameType<TextureWidgetComponent>();
              return (UpdateLayout(screen_space, static_cast<TextureWidgetComponent &>(source)));
          } },
          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
              source.AssertIsSameType<TextureWidgetComponent>();
              Render(position, static_cast<TextureWidgetComponent &>(source));
          } },
          { [](nlc::allocator & allocator, BaseWidgetComponent const & source, BaseWidgetComponent & target) {
              target.AssertIsSameType<TextureWidgetComponent>();
              source.AssertCopyIsSameType<TextureWidgetComponent>();
              CopyTextureComponent(allocator,
                                   static_cast<TextureWidgetComponent const &>(source),
                                   static_cast<TextureWidgetComponent &>(target));
          } }
#ifndef NDEBUG
          ,
          { [](nlc::aabb2 position, BaseWidgetComponent & source) {
              source.AssertIsSameType<TextureWidgetComponent>();
              DebugRender(position, static_cast<TextureWidgetComponent &>(source));
          } }
#endif
          )
    , Descriptor(descriptor)
    , TextureView(descriptor.Texture)
    , Layer(static_cast<float>(descriptor.Layer)) {
}
