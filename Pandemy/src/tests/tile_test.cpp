#include <nlc/test.hpp>

#include <nlc/fundamentals/allocator/standard.hpp>

#include <nlc/dialect/pair.hpp>
#include <nlc/dialect/string_view.hpp>

#include "Gameplay/Tiles/tile_descriptor.hpp"

nlc_test("RotateTile") {
    // simple rotation
    {
        constexpr TileDescriptor TestDesc { TileOpening::Up };
        TileInstance TestTile(TestDesc);

        // rotation clockwise
        nlc_check(TestTile.CurrentOpenings == TileOpening::Up);
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Right);
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Down);
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Left);
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Up);

        // forward and back
        RotateTile(TestTile, true);
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Up);

        // rotate counter clockwise
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Left);
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Down);
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Right);
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == TileOpening::Up);
    }
    // double rotation
    {
        constexpr TileOpening UpRight = static_cast<TileOpening>(
            static_cast<u8>(TileOpening::Up) | static_cast<u8>(TileOpening::Right));
        constexpr TileOpening DownRight = static_cast<TileOpening>(
            static_cast<u8>(TileOpening::Down) | static_cast<u8>(TileOpening::Right));
        constexpr TileOpening DownLeft = static_cast<TileOpening>(
            static_cast<u8>(TileOpening::Down) | static_cast<u8>(TileOpening::Left));
        constexpr TileOpening UpLeft = static_cast<TileOpening>(static_cast<u8>(TileOpening::Up) |
                                                                static_cast<u8>(TileOpening::Left));
        constexpr TileDescriptor TestDesc { UpRight };
        TileInstance TestTile(TestDesc);

        // rotation clockwise
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == DownRight);
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == DownLeft);
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == UpLeft);
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == UpRight);

        // forward and back
        RotateTile(TestTile, true);
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == UpRight);

        // rotate counter clockwise
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == UpLeft);
        RotateTile(TestTile, false);
        RotateTile(TestTile, false);
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == UpRight);
    }
    // no rotation
    {
        constexpr TileDescriptor TestDesc { TileOpening::None };
        TileInstance TestTile(TestDesc);

        // rotation clockwise
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == TileOpening::None);

        // rotate counter clockwise
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == TileOpening::None);
    }
    // all rotation
    {
        constexpr TileDescriptor TestDesc { TileOpening::All };
        TileInstance TestTile(TestDesc);

        // rotation clockwise
        RotateTile(TestTile, true);
        nlc_check(TestTile.CurrentOpenings == TileOpening::All);

        // rotate counter clockwise
        RotateTile(TestTile, false);
        nlc_check(TestTile.CurrentOpenings == TileOpening::All);
    }
}

static auto BoolToString(bool const value) -> nlc::string_view {
    return (value) ? nlc::string_view::from_c_str("true") : nlc::string_view::from_c_str("false");
}

template<typename... Types> static constexpr auto MakeOpening(Types... openings) -> TileOpening {
    auto const flags = (0 | ... | static_cast<u8>(openings));
    return (static_cast<TileOpening>(flags));
}

nlc_test("IsFlag") {
    nlc::standard_allocator alloc;
    static constexpr nlc::pair<TileOpening, bool> test_case[] = {
        { TileOpening::None, false },
        { TileOpening::Up, true },
        { TileOpening::Right, true },
        { TileOpening::Down, true },
        { TileOpening::Left, true },
        { TileOpening::All, false },
        { MakeOpening(TileOpening::Up, TileOpening::Down), false },
        { MakeOpening(TileOpening::Left, TileOpening::Right), false },
        { MakeOpening(TileOpening::Up, TileOpening::Left), false },
        { MakeOpening(TileOpening::Down, TileOpening::Right), false },
    };

    for (auto const & [opening, expected_result] : test_case) {
        bool const result = IsFlag(opening);
        nlc_check_msg(result == expected_result,
                      OpeningToString(opening, alloc),
                      " got ",
                      BoolToString(result),
                      ", expected ",
                      BoolToString(expected_result));
    }
}

nlc_test("IsOpeningSet_&_IsTileOpen") {
    struct TestInput {
        TileOpening Openings;
        TileOpening Flag;
    };
    nlc::standard_allocator alloc;
    static constexpr nlc::pair<TestInput, bool> test_case[] = {
        { TestInput { MakeOpening(), TileOpening::Up }, false },
        { TestInput { MakeOpening(TileOpening::Up), TileOpening::Up }, true },
        { TestInput { MakeOpening(TileOpening::Up), TileOpening::Down }, false },
        { TestInput { MakeOpening(TileOpening::Up, TileOpening::Down), TileOpening::Down }, true },
        { TestInput { MakeOpening(TileOpening::All), TileOpening::Right }, true },
        { TestInput { MakeOpening(TileOpening::Left, TileOpening::Right), TileOpening::Down }, false },
        { TestInput { MakeOpening(TileOpening::Left, TileOpening::Right), TileOpening::Left }, true },
        { TestInput { MakeOpening(TileOpening::Up, TileOpening::Left, TileOpening::Right),
                      TileOpening::Down },
          false },
        { TestInput { MakeOpening(TileOpening::Up, TileOpening::Left, TileOpening::Down),
                      TileOpening::Down },
          true },
    };
    for (auto const & [inputs, expected_result] : test_case) {
        bool result = IsOpeningSet(inputs.Openings, inputs.Flag);
        nlc_check_msg(result == expected_result,
                      "IsOpeningSet:\nWith Openings ",
                      OpeningToString(inputs.Openings, alloc),
                      " testing flag ",
                      OpeningToString(inputs.Flag, alloc),
                      " got ",
                      BoolToString(result),
                      ", expected ",
                      BoolToString(expected_result));

        TileDescriptor const TestDesc { inputs.Openings };
        TileInstance const TestTile(TestDesc);

        result = IsTileOpen(TestTile, inputs.Flag);
        nlc_check_msg(result == expected_result,
                      "IsTileOpen:\nWith Openings ",
                      OpeningToString(inputs.Openings, alloc),
                      " testing flag ",
                      OpeningToString(inputs.Flag, alloc),
                      " got ",
                      BoolToString(result),
                      ", expected ",
                      BoolToString(expected_result));
    }
}

nlc_test("IsOpeningSet_X_IsFlag") {
    struct TestInput {
        TileOpening Openings;
        TileOpening Flag;
    };
    nlc::standard_allocator alloc;
    static constexpr TestInput test_case[] = {
        { MakeOpening(TileOpening::Up), TileOpening::None },
        { MakeOpening(TileOpening::Up), MakeOpening(TileOpening::Down, TileOpening::Up) },
        { MakeOpening(TileOpening::Up, TileOpening::Down),
          MakeOpening(TileOpening::Up, TileOpening::Down) },
        { MakeOpening(TileOpening::All), MakeOpening(TileOpening::Left, TileOpening::Right) },
    };
    for (auto const & inputs : test_case) {
        nlc_expect_assert_msg(IsOpeningSet(inputs.Openings, inputs.Flag),
                              "With Openings ",
                              OpeningToString(inputs.Openings, alloc),
                              " testing flag ",
                              OpeningToString(inputs.Flag, alloc));
    }
}
