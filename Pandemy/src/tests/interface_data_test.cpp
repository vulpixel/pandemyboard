#include <nlc/test.hpp>

#include "UI/Controllers/card_display_controller.hpp"
#include "UI/Controllers/main_menu_controller.hpp"
#include "UI/Controllers/tile_placement_controller.hpp"
#include "UI/Widget/base_widget.hpp"
#include "UI/interface_resources.hpp"
#include "UI/interface_resources_loader.hpp"
#include "UI/menu_data.hpp"
#include "UI/menu_loader.hpp"

#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/function_view.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/scoped.hpp>
#include <nlc/fundamentals/string_stream.hpp>

#include <win_input/inputs.hpp>
#include <win_input/window.hpp>

#include <graphic_engine/graphic_engine.hpp>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

static auto create_window(nlc::allocator & alloc, u32_2 const window_size)
    -> nlc::scoped<GLFWwindow *> {
    Inputs::init(alloc);

    nlc_check_msg(glfwInit(), "couldn't init render context");

    auto window = create_window_with_context(window_size.x, window_size.y, "PandemyBoard");

    nlc_check_msg(*window != nullptr, "couldn't create window");

    ge::InitInfos const init_info { *window };
    ge::init(init_info);

    return window;
}

static auto release_window(nlc::scoped<GLFWwindow *> & window) -> void {
    ge::deinit();
    window = { nullptr, [](auto) {} };
    glfwTerminate();
    Inputs::deinit();
}

using validator_function = nlc::function_view<bool(mdf::MenuData const &, nlc::string_stream &)>;

static auto validate_menu(validator_function validate_function,
                          nlc::string_view menu_file,
                          nlc::string_view menu_name,
                          nlc::span<nlc::string_view const> dependencies) -> void {
    nlc::standard_allocator alloc;
    auto window = create_window(alloc, { 1, 1 });
    defer { release_window(window); };

    nlc::string_stream error_stream(alloc);
    nlc::optional<InterfaceResources> interface_resources =
        mdf::interface_resources_load(alloc, error_stream);
    nlc_check_msg(interface_resources != nlc::null, error_stream.c_str());

    nlc::optional<mdf::MenuData> menu =
        mdf::menu_loader(alloc, menu_file, menu_name, dependencies, *interface_resources, error_stream);

    nlc_check_msg(menu != nlc::null, error_stream.c_str());

    nlc_check_msg(validate_function(*menu, error_stream), error_stream.c_str());
}

nlc_test("MainMenuLoadTesting") {
    validator_function validator = [](auto const &, auto &) -> bool { return (true); };
#ifndef NDEBUG
    validator = &MainMenuController::Validate;
#endif
    validate_menu(validator, "data/UI/Menu/MainMenu.mdf", "MainMenu", {});
}

nlc_test("CardDisplayLoadTesting") {
    validator_function validator = [](auto const &, auto &) -> bool { return (true); };
#ifndef NDEBUG
    validator = &CardDisplayController::Validate;
#endif
    validate_menu(validator, "data/UI/Menu/CardDisplayMenu.mdf", "CardDisplayMenu", {});
}

nlc_test("TilePlacementTesting") {
    validator_function validator = [](auto const &, auto &) -> bool { return (true); };
#ifndef NDEBUG
    validator = &TilePlacementController::Validate;
#endif
    static constexpr nlc::string_view tile_dependencies[] = { "TileTemplate", "DraggedTileTemplate" };
    validate_menu(validator, "data/UI/Menu/TilePlacementMenu.mdf", "TilePlacementMenu", tile_dependencies);
}
