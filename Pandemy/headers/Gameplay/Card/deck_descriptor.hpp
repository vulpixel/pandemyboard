// deck descriptor
#pragma once

#include "Gameplay/Card/card_descriptor.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/meta/basics.hpp>

namespace nlc {
class allocator;
}

struct DeckDescriptor {
    nlc::string Name;
    nlc::vector<CardDescriptor> Cards;

    DeckDescriptor(nlc::allocator & alloc)
        : Name(alloc, { nullptr, 0 })
        , Cards(alloc) {}
    DeckDescriptor(nlc::string && name, nlc::vector<CardDescriptor> && cards)
        : Name(nlc::move(name))
        , Cards(nlc::move(cards)) {}
    DeckDescriptor(DeckDescriptor && rhs)
        : Name(nlc::move(rhs.Name))
        , Cards(nlc::move(rhs.Cards)) {}
    ~DeckDescriptor() = default;

    auto operator=(DeckDescriptor && rhs) -> DeckDescriptor & {
        if (&rhs != this) {
            Name = nlc::move(rhs.Name);
            Cards = nlc::move(rhs.Cards);
        }
        return (*this);
    }
};
