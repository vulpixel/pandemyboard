// global description for a card
//
#pragma once

#include "Gameplay/Card/Components/ability_desc.hpp"
#include "Gameplay/Card/Components/character_desc.hpp"
#include "Gameplay/Card/Components/event_desc.hpp"
#include "Gameplay/Card/Components/guild_improvement_desc.hpp"
#include "Gameplay/Card/Components/item_desc.hpp"

#include "Graphic/texture_metadata.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/variant.hpp>
#include <nlc/fundamentals/string.hpp>

#include <graphic_engine/ids.hpp>

struct CardDescriptor {
    using CardContentType =
        nlc::variant<AbilityDescriptor, CharacterDescriptor, EventDescriptor, ItemDescriptor, GuildImprovementDescriptor>;
    nlc::string Name;
    nlc::string Description;
    Graphic::TextureMetadata Texture;

    CardContentType Content;

    i32 Cost = 0;
};
