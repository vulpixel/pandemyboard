#pragma once

#include "Gameplay/Tiles/tile_enum.hpp"

struct TileDescriptor {
    TileOpening Openings;
};

struct TileInstance {
    TileDescriptor const & Descriptor;

    TileOpening CurrentOpenings;

    TileInstance(TileInstance const & rhs) = default;
    TileInstance(TileDescriptor const & desc)
        : Descriptor(desc)
        , CurrentOpenings(desc.Openings) {}
};

auto RotateTile(TileInstance & tile, bool rotate_clockwise) -> void;
auto IsTileOpen(TileInstance const & tile, TileOpening const direction) -> bool;
