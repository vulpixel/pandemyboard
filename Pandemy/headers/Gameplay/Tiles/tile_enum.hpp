#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/fundamentals/hash.hpp>
#include <nlc/fundamentals/string.hpp>

namespace nlc {
class allocator;
}

enum class TileOpening : u8 {
    None = 0b0000,
    Up = 0b0001,
    Right = 0b0010,
    Down = 0b0100,
    Left = 0b1000,
    All = Up | Right | Down | Left,
};

[[nodiscard]] inline auto hash(TileOpening const opening) -> nlc::hash_result {
    return (nlc::hash(static_cast<u8>(opening)));
}

auto OpeningToString(TileOpening const & opening, nlc::allocator & allocator) -> nlc::string;
auto IsFlag(TileOpening const opening) -> bool;
auto IsOpeningSet(TileOpening const openings, TileOpening const flag) -> bool;
