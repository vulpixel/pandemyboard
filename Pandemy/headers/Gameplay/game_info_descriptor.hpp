#pragma once

#include "Gameplay/Card/deck_descriptor.hpp"
#include "Gameplay/Tiles/tile_descriptor.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/buffer.hpp>

#include <nlc/containers/vector.hpp>

#include <nlc/meta/basics.hpp>

namespace nlc {
class allocator;
}

struct GameInfoDescriptor {
    nlc::vector<DeckDescriptor> Decks;
    nlc::vector<TileDescriptor> TileBag;
    nlc::allocator & Allocator;

    GameInfoDescriptor(nlc::allocator & alloc,
                       nlc::vector<DeckDescriptor> && decks,
                       nlc::vector<TileDescriptor> && tile_bag)
        : Decks(nlc::move(decks))
        , TileBag(nlc::move(tile_bag))
        , Allocator(alloc) {}
    GameInfoDescriptor(GameInfoDescriptor && rhs)
        : Decks(nlc::move(rhs.Decks))
        , TileBag(nlc::move(rhs.TileBag))
        , Allocator(rhs.Allocator) {}
};
