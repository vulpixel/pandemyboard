#pragma once

#include "Gameplay/Tiles/tile_descriptor.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/maths/vec2.hpp>

#include <nlc/dialect/arithmetic_types.hpp>

class Board {
  public:
    Board(nlc::allocator & allocator, u8_2 const size);
    Board(Board && rhs);

    auto operator=(Board && rhs) -> Board &;

    auto operator()(u8 const x, u8 const y) const -> nlc::optional<TileInstance> const &;
    auto operator()(u8_2 const xy) const -> nlc::optional<TileInstance> const & {
        return (*this)(xy.x, xy.y);
    }

    auto CanPlaceTile(TileInstance const & tile, u8_2 const position) const -> bool;
    auto PlaceTile(TileInstance const & tile, u8_2 const position) -> void;
    auto RemoveTile(u8_2 const position) -> TileInstance;

    auto GetSize() const -> u8_2 { return (Size); }

  private:
    nlc::vector<nlc::optional<TileInstance>> Tiles;
    u8_2 Size;
};
