#pragma once

#include "base_widget.hpp"

#include <nlc/maths/geometry2d/aabb2.hpp>

void Render(BaseWidget const & widget);

// comp render
struct TextWidgetComponent;
void Render(nlc::aabb2 const render_space, TextWidgetComponent const & text_widget);
struct ContainerWidgetComponent;
void Render(nlc::aabb2 const render_space, ContainerWidgetComponent const & container_widget);
struct GridWidgetComponent;
void Render(nlc::aabb2 const render_space, GridWidgetComponent const & grid_widget);
struct ButtonWidgetComponent;
void Render(nlc::aabb2 const render_space, ButtonWidgetComponent const & button_widget);
struct TextureWidgetComponent;
void Render(nlc::aabb2 const render_space, TextureWidgetComponent const & texture_widget);
#ifndef NDEBUG
void DebugRender(BaseWidget const & Widget);

// comp render
void DebugRender(nlc::aabb2 const render_space, TextWidgetComponent const & text_widget);
void DebugRender(nlc::aabb2 const render_space, ContainerWidgetComponent const & container_widget);
void DebugRender(nlc::aabb2 const render_space, GridWidgetComponent const & grid_widget);
void DebugRender(nlc::aabb2 const render_space, ButtonWidgetComponent const & button_widget);
void DebugRender(nlc::aabb2 const render_space, TextureWidgetComponent const & texture_widget);
#endif
