#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

enum class HorizontalLayoutOrdering : u8 {
    NoOrder = 0,
    LeftToRight,
    RightToLeft,
};

enum class VerticalLayoutOrdering : u8 {
    NoOrder = 0,
    TopToBottom,
    BottomToTop,
};

enum class HorizontalAlignment : u8 {
    Center = 0,
    Left,
    Right,
};

enum class VerticalAlignment : u8 {
    Center = 0,
    Top,
    Bottom,
};
