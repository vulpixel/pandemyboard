#pragma once

#include "base_widget.hpp"
#include "base_widget_component.hpp"

namespace nlc {
class allocator;
}

struct BaseWidget;

struct DraggableWidgetCompDescriptor : public BaseWidgetCompDescriptor {
    auto CreateInstance(nlc::allocator & alloc) const -> nlc::unique_ptr<BaseWidgetComponent> override;
};

struct DraggableWidgetComponent : public BaseWidgetComponent {
    static usize const LocalTypeHash;

    DraggableWidgetComponent(nlc::allocator & alloc, DraggableWidgetCompDescriptor const & descriptor);
};
