#pragma once

#include "alignment.hpp"
#include "base_widget_component.hpp"
#include "layout.hpp"
#include "widget_renderer.hpp"
#include "widget_updater.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/maths/vec2.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/is_same.hpp>

namespace ge {
struct Actor;
}

struct BaseWidget;

struct ContainerWidgetCompDescriptor : public BaseWidgetCompDescriptor {
    HorizontalLayoutOrdering HorizontalOrder = HorizontalLayoutOrdering::NoOrder;
    VerticalLayoutOrdering VerticalOrder = VerticalLayoutOrdering::NoOrder;
    ListLayout Layout = ListLayout::Horizontal;
    FillPolicy Fill = FillPolicy::Fill;
    nlc::optional<BaseWidgetDescriptor> PaddingDescriptor;
    nlc::vector<BaseWidgetDescriptor> Children;

    ContainerWidgetCompDescriptor(nlc::allocator & alloc)
        : BaseWidgetCompDescriptor()
        , Children(alloc) {}

    virtual ~ContainerWidgetCompDescriptor() = default;

    auto CreateInstance(nlc::allocator & alloc) const -> nlc::unique_ptr<BaseWidgetComponent> override;
};

struct ContainerWidgetComponent : public BaseWidgetComponent {
    static usize const LocalTypeHash;

    ContainerWidgetCompDescriptor const & Descriptor;

    nlc::vector<BaseWidget> Children;

    ContainerWidgetComponent(nlc::allocator & alloc, ContainerWidgetCompDescriptor const & descriptor);
    ContainerWidgetComponent(ContainerWidgetComponent && rhs);
    virtual ~ContainerWidgetComponent() = default;
};

template<class T>
auto PopulateContainerWidget(ContainerWidgetComponent & container,
                             nlc::span<T const> const children,
                             nlc::allocator & alloc) -> void {
    bool const has_padding = (container.Descriptor.PaddingDescriptor != nlc::null);
    container.Children.reserve((has_padding) ? children.size() * 2 : children.size());
    for (auto const & child : children) {
        if constexpr (nlc::meta::is_same<T, BaseWidgetDescriptor>)
            container.Children.append_no_grow(child.CreateInstance(alloc));
        else
            container.Children.append_no_grow(child->CreateInstance(alloc));

        if (has_padding && container.Children.size() < children.size() * 2 - 1)
            container.Children.append_no_grow(
                container.Descriptor.PaddingDescriptor->CreateInstance(alloc));
    }
}
