#pragma once

#include "base_widget_component.hpp"

#include <nlc/algo/find.hpp>
#include <nlc/containers/vector.hpp>
#include <nlc/dialect/expected.hpp>
#include <nlc/dialect/pointer.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/dialect/variant.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>
#include <nlc/maths/geometry2d/aabb2.hpp>
#include <nlc/maths/vec2.hpp>
#include <nlc/meta/id.hpp>
#include <nlc/meta/numeric_limits.hpp>

struct BaseWidget;

struct BaseWidgetDescriptor {
    enum class ScreenSpace {
        Relative = 0,
        Absolute = 1,
    };
    nlc::string Name;

    nlc::aabb2 ScreenArea;

    ScreenSpace PositionSpace = ScreenSpace::Relative;
    ScreenSpace SizeSpace = ScreenSpace::Relative;

    nlc::vector<nlc::unique_ptr<BaseWidgetCompDescriptor>> Components;

    BaseWidgetDescriptor() = delete;
    BaseWidgetDescriptor(nlc::allocator & alloc)  // dummy
        : Name(alloc, nlc::string_view())
        , Components(alloc) {}
    BaseWidgetDescriptor(nlc::allocator & alloc,
                         nlc::string_view widget_name,
                         nlc::aabb2 screen_area,
                         ScreenSpace position_space,
                         ScreenSpace size_space)
        : Name(alloc, widget_name)
        , ScreenArea(screen_area)
        , PositionSpace(position_space)
        , SizeSpace(size_space)
        , Components(alloc) {}
    BaseWidgetDescriptor(BaseWidgetDescriptor && rhs)
        : Name(nlc::move(rhs.Name))
        , ScreenArea(rhs.ScreenArea)
        , PositionSpace(rhs.PositionSpace)
        , SizeSpace(rhs.SizeSpace)
        , Components(nlc::move(rhs.Components)) {}
    virtual ~BaseWidgetDescriptor() = default;

    BaseWidgetDescriptor & operator=(BaseWidgetDescriptor && rhs) {
        if (&rhs != this) {
            Name = nlc::move(rhs.Name);
            ScreenArea = rhs.ScreenArea;
            PositionSpace = rhs.PositionSpace;
            SizeSpace = rhs.SizeSpace;
            Components = nlc::move(rhs.Components);
        }
        return (*this);
    }

    auto CreateInstance(nlc::allocator & alloc) const -> BaseWidget;
};

struct BaseWidget {
  public:
    struct InternalWidgetId final {
        using underlying_type = u32;
        static constexpr underlying_type invalid_value = nlc::meta::limits<underlying_type>::max;
        static constexpr underlying_type default_value = nlc::meta::limits<underlying_type>::max;
    };
    using ID = nlc::meta::id<InternalWidgetId>;

  private:
    ID UniqueId;
    static ID CurrentId;

  public:
    BaseWidgetDescriptor const * Descriptor;

    nlc::string Name;

    nlc::aabb2 ScreenArea;

    bool IsDisplayable = true;
    bool IsStatic = true;

    nlc::vector<nlc::unique_ptr<BaseWidgetComponent>> Components;

    BaseWidget() = delete;
    BaseWidget(nlc::allocator & alloc)
        : UniqueId(ID::invalid())
        , Descriptor(nullptr)
        , Name(alloc, nullptr, 0)
        , Components(alloc) {}
    BaseWidget(nlc::allocator & alloc, BaseWidgetDescriptor const & descriptor)
        : UniqueId(CurrentId)
        , Descriptor(&descriptor)
        , Name(alloc, descriptor.Name)
        , ScreenArea(descriptor.ScreenArea)
        , Components(alloc) {
        CurrentId = ID { CurrentId.value() + 1 };
    }
    BaseWidget(BaseWidget && rhs)
        : UniqueId(rhs.UniqueId)
        , Descriptor(rhs.Descriptor)
        , Name(nlc::move(rhs.Name))
        , ScreenArea(rhs.ScreenArea)
        , IsDisplayable(rhs.IsDisplayable)
        , IsStatic(rhs.IsStatic)
        , Components(nlc::move(rhs.Components)) {
        rhs.UniqueId = ID::invalid();
    }

    auto operator=(BaseWidget && rhs) -> BaseWidget & {
        if (&rhs != this) {
            UniqueId = rhs.UniqueId;
            rhs.UniqueId = ID::invalid();
            Descriptor = rhs.Descriptor;
            Name = nlc::move(rhs.Name);
            ScreenArea = rhs.ScreenArea;
            IsDisplayable = rhs.IsDisplayable;
            IsStatic = rhs.IsStatic;
            Components = nlc::move(rhs.Components);
        }

        return (*this);
    }

    auto GetId() const -> ID { return (UniqueId); }
};

// functions
struct ErrorWidgetNotFound {};

struct ErrorComponentNotFound {};

auto MoveWidget(BaseWidget & widget_to_move, f32_2 new_position) -> void;

auto DuplicateWidget(nlc::allocator & alloc, BaseWidget const & source) -> BaseWidget;

// Finds
template<typename T>
auto FindComponentInDescriptor(BaseWidgetDescriptor const & widget) -> T const * {
    auto const * const ret = nlc::find_if(nlc::make_span(widget.Components), [&](auto const & comp) {
        nlc_assert_msg((comp.get() != nullptr), "null component in ", widget.Name.c_str());
        return (dynamic_cast<T const *>(comp.get()) != nullptr);
    });
    if (ret == nullptr)
        return (nullptr);
    return (static_cast<T const *>(ret->get()));
}

template<typename T>
inline auto FindWidgetInList(nlc::span<T> const widgets, nlc::string_view const searched_name) -> T * {
    return (nlc::find_if(widgets, [searched_name](auto const & desc) {
        return (desc.Name == searched_name);
    }));
}

template<typename T> auto FindComponentInWidget(BaseWidget const & widget) -> T * {
    nlc::unique_ptr<BaseWidgetComponent> const * const ret =
        nlc::find_if(nlc::make_span(widget.Components), [&](auto const & comp) {
            nlc_assert_msg((comp.get() != nullptr), "null component in ", widget.Name.c_str());
            return (comp->template IsSameType<T>());
        });
    if (ret == nullptr)
        return (nullptr);
    return (static_cast<T *>(ret->get()));
}

template<typename U, typename T>
auto FindComponentInNamedWidget(nlc::span<T> const widgets, nlc::string_view search_name)
    -> nlc::expected<nlc::pointer<U>, ErrorWidgetNotFound, ErrorComponentNotFound> {
    T * const widget = FindWidgetInList(widgets, search_name);
    if (widget == nullptr)
        return (ErrorWidgetNotFound {});
    U * const component = FindComponentInWidget<U>(*widget);
    if (component == nullptr)
        return (ErrorComponentNotFound {});
    return (nlc::pointer { component });
}

template<typename U, typename T>
auto FindComponentInNamedDescriptor(nlc::span<T> const widgets, nlc::string_view search_name)
    -> nlc::expected<nlc::pointer<U const>, ErrorWidgetNotFound, ErrorComponentNotFound> {
    T * const widget = FindWidgetInList(widgets, search_name);
    if (widget == nullptr)
        return (ErrorWidgetNotFound {});
    U const * const component = FindComponentInDescriptor<U>(*widget);
    if (component == nullptr)
        return (ErrorComponentNotFound {});
    return (nlc::pointer { component });
}

inline auto FindWidgetInListFromId(BaseWidget::ID const target_id,
                                   nlc::span<BaseWidget const> const widget_list)
    -> BaseWidget const * {
    return (nlc::find_if(widget_list,
                         [target_id](auto const & widget) { return (widget.GetId() == target_id); }));
}

inline auto FindWidgetInListFromId(BaseWidget::ID const target_id,
                                   nlc::span<BaseWidget> const widget_list) -> BaseWidget * {
    return (nlc::find_if(widget_list,
                         [target_id](auto const & widget) { return (widget.GetId() == target_id); }));
}
