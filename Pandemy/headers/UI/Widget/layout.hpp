#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

enum class ListLayout : u8 {
    Horizontal,
    Vertical,
};

enum class FillPolicy : u8 {
    Fill,
    FitChildrenSize,
};
