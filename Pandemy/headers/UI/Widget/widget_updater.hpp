#pragma once

#include "base_widget.hpp"
#include "layout_sizes.hpp"

#include <nlc/maths/geometry2d/aabb2.hpp>
#include <nlc/maths/vec2.hpp>

auto UpdateLayout(nlc::aabb2 const render_space, BaseWidget & widget) -> LayoutSizes;

// comp render
struct TextWidgetComponent;
auto UpdateLayout(nlc::aabb2 const render_space, TextWidgetComponent & text_widget) -> LayoutSizes;
struct ContainerWidgetComponent;
auto UpdateLayout(nlc::aabb2 const render_space, ContainerWidgetComponent & container_widget)
    -> LayoutSizes;
struct GridWidgetComponent;
auto UpdateLayout(nlc::aabb2 const render_space, GridWidgetComponent & grid_widget) -> LayoutSizes;
struct ButtonWidgetComponent;
auto UpdateLayout(nlc::aabb2 const render_space, ButtonWidgetComponent & button_widget) -> LayoutSizes;
struct TextureWidgetComponent;
auto UpdateLayout(nlc::aabb2 const render_space, TextureWidgetComponent & texture_widget)
    -> LayoutSizes;
