#pragma once

#include "alignment.hpp"
#include "base_widget_component.hpp"
#include "layout.hpp"
#include "widget_renderer.hpp"
#include "widget_updater.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/optional.hpp>

namespace nlc {
class allocator;
}

struct GridWidgetCompDescriptor : public BaseWidgetCompDescriptor {
    ListLayout MainDimension = ListLayout::Horizontal;
    VerticalLayoutOrdering VOrdering = VerticalLayoutOrdering::NoOrder;
    HorizontalLayoutOrdering HOrdering = HorizontalLayoutOrdering::NoOrder;
    FillPolicy Fill = FillPolicy::Fill;
    usize MaxColumns = 0;
    usize MaxLines = 0;

    nlc::optional<BaseWidgetDescriptor> VerticalPaddingDescriptor;
    nlc::optional<BaseWidgetDescriptor> HorizontalPaddingDescriptor;

    nlc::vector<BaseWidgetDescriptor> Children;

    GridWidgetCompDescriptor(nlc::allocator & alloc,
                             ListLayout const dimension,
                             VerticalLayoutOrdering const v_ordering,
                             HorizontalLayoutOrdering const h_ordering,
                             FillPolicy const fill,
                             usize const max_columns,
                             usize const max_lines)
        : BaseWidgetCompDescriptor()
        , MainDimension(dimension)
        , VOrdering(v_ordering)
        , HOrdering(h_ordering)
        , Fill(fill)
        , MaxColumns(max_columns)
        , MaxLines(max_lines)
        , Children(alloc) {}

    virtual ~GridWidgetCompDescriptor() = default;

    auto CreateInstance(nlc::allocator & alloc) const -> nlc::unique_ptr<BaseWidgetComponent> override;
};

struct GridWidgetComponent : public BaseWidgetComponent {
    static usize const LocalTypeHash;

    GridWidgetCompDescriptor const & Descriptor;

    nlc::vector<BaseWidget> Children;
    nlc::vector<BaseWidget::ID> PaddingIds;

    auto GetElementCount() -> usize { return (Children.size() - PaddingIds.size()); }
    GridWidgetComponent(nlc::allocator & alloc, GridWidgetCompDescriptor const & descriptor);
    GridWidgetComponent(GridWidgetComponent && rhs);
    virtual ~GridWidgetComponent() = default;
};

auto GridElementIsPadding(GridWidgetComponent const & grid, BaseWidget const & element) -> bool;

template<class T>
auto PopulateGridWidget(GridWidgetComponent & grid,
                        nlc::span<T const> const children,
                        nlc::allocator & alloc) -> void;

#include "grid_widget.inl"
