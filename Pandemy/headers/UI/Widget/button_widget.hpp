#pragma once

#include "base_widget.hpp"
#include "base_widget_component.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/buffer.hpp>
#include <nlc/meta/basics.hpp>

struct BaseWidget;

enum class ButtonWidgetStatus : u8 {
    Idle,
    Selected,
    Pressed,
    Disabled,
    SIZE,
};

struct ButtonWidgetCompDescriptor : public BaseWidgetCompDescriptor {
    BaseWidgetDescriptor const IdleWidget;
    BaseWidgetDescriptor const SelectedWidget;
    BaseWidgetDescriptor const PressedWidget;
    BaseWidgetDescriptor const DisabledWidget;

    ButtonWidgetStatus DefaultStatus;

    ButtonWidgetCompDescriptor(BaseWidgetDescriptor && idle,
                               BaseWidgetDescriptor && selected,
                               BaseWidgetDescriptor && pressed,
                               BaseWidgetDescriptor && disabled,
                               ButtonWidgetStatus default_status)
        : BaseWidgetCompDescriptor()
        , IdleWidget(nlc::move(idle))
        , SelectedWidget(nlc::move(selected))
        , PressedWidget(nlc::move(pressed))
        , DisabledWidget(nlc::move(disabled))
        , DefaultStatus(default_status) {}

    virtual ~ButtonWidgetCompDescriptor() = default;

    auto CreateInstance(nlc::allocator & alloc) const -> nlc::unique_ptr<BaseWidgetComponent> override;
};

struct ButtonWidgetComponent : public BaseWidgetComponent {
    static usize const LocalTypeHash;

    ButtonWidgetCompDescriptor const & Descriptor;

    nlc::buffer<BaseWidget, static_cast<u8>(ButtonWidgetStatus::SIZE)> StatusWidgets;

    ButtonWidgetStatus CurrentStatus = ButtonWidgetStatus::Idle;

    nlc::allocator * Allocator;

    ButtonWidgetComponent(nlc::allocator & alloc, ButtonWidgetCompDescriptor const & descriptor);
    ButtonWidgetComponent(ButtonWidgetComponent && rhs);
    virtual ~ButtonWidgetComponent();
};

// UTILS
auto UpdateButtonText(ButtonWidgetComponent & button, nlc::string_view new_text) -> void;
