#pragma once

#include <nlc/dialect/range.hpp>

template<class T>
auto InstantiateChildren(nlc::span<T const> const children, nlc::allocator & alloc)
    -> nlc::vector<BaseWidget> {
    auto instances { nlc::vector<BaseWidget>::create_and_reserve(alloc, children.size()) };
    for (auto const & child : children) {
        if constexpr (nlc::meta::is_same<T, BaseWidgetDescriptor>)
            instances.append_no_grow(child.CreateInstance(alloc));
        else
            instances.append_no_grow(child->CreateInstance(alloc));
    }
    return (instances);
}

template<class T>
auto PopulateGridWidget(GridWidgetComponent & grid,
                        nlc::span<T const> const children,
                        nlc::allocator & alloc) -> void {
    nlc::vector<BaseWidget> instanced_children = InstantiateChildren<T>(children, alloc);
    bool const has_vertical_padding = (grid.Descriptor.VerticalPaddingDescriptor != nlc::null);
    bool const has_horizontal_padding = (grid.Descriptor.HorizontalPaddingDescriptor != nlc::null);
    bool const has_padding = (has_vertical_padding || has_horizontal_padding);
    if (has_padding == false) {
        grid.Children = nlc::move(instanced_children);
        return;
    }
    grid.Children.reserve(children.size() * 2 - 1);
    grid.PaddingIds.reserve(children.size() - 1);
    usize current_child = 0u;
    switch (grid.Descriptor.MainDimension) {
        case ListLayout::Vertical: {
            break;
        }
        case ListLayout::Horizontal: {
            usize const lines = (instanced_children.size() + grid.Descriptor.MaxColumns - 1) /
                                grid.Descriptor.MaxColumns;
            for (auto current_line : nlc::range(0u, lines)) {
                auto element_count =
                    nlc::min(instanced_children.size() - current_child, grid.Descriptor.MaxColumns);
                for (auto current_element : nlc::range(0u, element_count)) {
                    grid.Children.append_no_grow(nlc::move(instanced_children[current_child++]));
                    if (has_horizontal_padding && current_element < element_count - 1) {
                        auto const & padding = grid.Children.append_no_grow(
                            grid.Descriptor.HorizontalPaddingDescriptor->CreateInstance(alloc));
                        grid.PaddingIds.append_no_grow(padding.GetId());
                    }
                }
                if (has_vertical_padding && current_line < lines - 1) {
                    auto const & padding = grid.Children.append_no_grow(
                        grid.Descriptor.VerticalPaddingDescriptor->CreateInstance(alloc));
                    grid.PaddingIds.append_no_grow(padding.GetId());
                }
            }
            break;
        }
    }
    nlc_assert(current_child == instanced_children.size());
}
