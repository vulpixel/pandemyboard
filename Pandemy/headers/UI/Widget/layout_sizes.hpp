#pragma once

#include <nlc/maths/vec2.hpp>

struct LayoutSizes {
    f32_2 const ConstrainedSize;
    f32_2 const OptimalSize;
};
