#pragma once

#include "layout_sizes.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/function.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>
#include <nlc/meta/basics.hpp>

#include <nlc/maths/geometry2d/aabb2.hpp>
#include <nlc/maths/vec2.hpp>

struct BaseWidgetComponent;
namespace ge {
struct Actor;
}

struct BaseWidgetCompDescriptor {
    virtual auto CreateInstance(nlc::allocator & alloc) const
        -> nlc::unique_ptr<BaseWidgetComponent> = 0;
    virtual ~BaseWidgetCompDescriptor() = default;
};

struct BaseWidgetComponent {
    nlc::function<LayoutSizes(nlc::aabb2 const, BaseWidgetComponent & source)> LayoutUpdater;
    nlc::function<void(nlc::aabb2 const, BaseWidgetComponent & source)> Renderer;
    nlc::function<void(nlc::allocator &, BaseWidgetComponent const &, BaseWidgetComponent & target)> CopyComponentData;
#ifndef NDEBUG
    nlc::function<void(nlc::aabb2 const, BaseWidgetComponent & source)> DebugRenderer;
#endif

    template<class T> auto AssertCopyIsSameType() const -> void {
        nlc_assert_msg(IsSameType<T>(), "Copy component must operate on same component type");
    }
    template<class T> auto AssertIsSameType() const -> void {
        nlc_assert_msg(IsSameType<T>(), "Source component is not the same type.");
    }
    template<class T> auto IsSameType() const -> bool { return T::LocalTypeHash == TypeHash; }

    virtual ~BaseWidgetComponent() = default;

  protected:
    BaseWidgetComponent() = delete;
#ifdef NDEBUG
    BaseWidgetComponent(
        usize const type_hash,
        nlc::function<LayoutSizes(nlc::aabb2 const, BaseWidgetComponent &)> layout_func,
        nlc::function<void(nlc::aabb2 const, BaseWidgetComponent &)> render_func,
        nlc::function<void(nlc::allocator &, BaseWidgetComponent const &, BaseWidgetComponent &)> copy_function)
        : LayoutUpdater(nlc_fwd(layout_func))
        , Renderer(nlc_fwd(render_func))
        , CopyComponentData(nlc_fwd(copy_function))
        , TypeHash(type_hash) {}
    BaseWidgetComponent(BaseWidgetComponent && rhs)
        : LayoutUpdater(nlc::move(rhs.LayoutUpdater))
        , Renderer(nlc::move(rhs.Renderer))
        , CopyComponentData(nlc::move(rhs.CopyComponentData))
        , TypeHash(rhs.TypeHash) {}
#else
    BaseWidgetComponent(
        usize const type_hash,
        nlc::function<LayoutSizes(nlc::aabb2 const, BaseWidgetComponent &)> layout_func,
        nlc::function<void(nlc::aabb2, BaseWidgetComponent &)> render_func,
        nlc::function<void(nlc::allocator &, BaseWidgetComponent const &, BaseWidgetComponent &)> copy_function,
        nlc::function<void(nlc::aabb2, BaseWidgetComponent &)> debug_render_func)
        : LayoutUpdater(nlc_fwd(layout_func))
        , Renderer(nlc_fwd(render_func))
        , CopyComponentData(nlc_fwd(copy_function))
        , DebugRenderer(nlc_fwd(debug_render_func))
        , TypeHash(type_hash) {}
    BaseWidgetComponent(BaseWidgetComponent && rhs)
        : LayoutUpdater(nlc::move(rhs.LayoutUpdater))
        , Renderer(nlc::move(rhs.Renderer))
        , CopyComponentData(nlc::move(rhs.CopyComponentData))
        , DebugRenderer(nlc::move(rhs.DebugRenderer))
        , TypeHash(rhs.TypeHash) {}
#endif

  private:
    usize const TypeHash;
};
