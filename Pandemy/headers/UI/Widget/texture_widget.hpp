#pragma once

#include "Graphic/texture_metadata.hpp"
#include "base_widget_component.hpp"
#include "widget_renderer.hpp"
#include "widget_updater.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/maths/units/angle_units.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/numeric_limits.hpp>

namespace ge {
struct Actor;
}

namespace nlc {
class allocator;
class string_stream;
}  // namespace nlc

struct TextureWidgetComponent;

enum class TextureFillMode : u8 {
    KeepRatio,
    ResizeToFit,
    SIZE,
};

enum class TextureLayer : u8 {
    Background = 0,
    Foreground = 1,
    AlwaysOnTop = 2,
    MAX = nlc::meta::limits<u8>::max,
};

struct TextureWidgetCompDescriptor final : public BaseWidgetCompDescriptor {
    nlc::string Path;
    nlc::optional<Graphic::TextureMetadata> Texture;
    TextureFillMode FillMode = TextureFillMode::KeepRatio;
    TextureLayer Layer = TextureLayer::Background;

    TextureWidgetCompDescriptor(nlc::allocator & alloc,
                                nlc::string_view const & path,
                                TextureFillMode fill_mode,
                                TextureLayer layer,
                                nlc::string_stream & error_stream)
        : BaseWidgetCompDescriptor()
        , Path(alloc, path)
        , Texture()
        , FillMode(fill_mode)
        , Layer(layer) {
        if (path.is_empty() == false) {
            Texture.set(alloc, path, error_stream);
        }
    }

    TextureWidgetCompDescriptor(TextureWidgetCompDescriptor && rhs)
        : BaseWidgetCompDescriptor()
        , Path(nlc::move(rhs.Path))
        , Texture(nlc::move(rhs.Texture))
        , FillMode(rhs.FillMode)
        , Layer(rhs.Layer) {}

    ~TextureWidgetCompDescriptor() final = default;

    auto CreateInstance(nlc::allocator & alloc) const -> nlc::unique_ptr<BaseWidgetComponent> final;
};

struct TextureWidgetComponent final : public BaseWidgetComponent {
    static usize const LocalTypeHash;
    TextureWidgetCompDescriptor const & Descriptor;

    Graphic::TextureView TextureView;

    nlc::radians Rotation = 0.0_rad;

    float Layer = 0.0f;

    TextureWidgetComponent(nlc::allocator & alloc, TextureWidgetCompDescriptor const & descriptor);

    TextureWidgetComponent(TextureWidgetComponent && rhs)
        : BaseWidgetComponent(LocalTypeHash,
                              nlc::move(rhs.LayoutUpdater),
                              nlc::move(rhs.Renderer),
                              nlc::move(rhs.CopyComponentData)
#ifndef NDEBUG
                                  ,
                              nlc::move(rhs.DebugRenderer)
#endif
                                  )
        , Descriptor(nlc::move(rhs.Descriptor))
        , TextureView(rhs.TextureView)
        , Rotation(rhs.Rotation)
        , Layer(rhs.Layer) {
    }

    auto SetLayer(TextureLayer new_layer) -> void { Layer = static_cast<float>(new_layer); }
    auto SetSpecificLayer(float new_layer) -> void { Layer = new_layer; }
};
