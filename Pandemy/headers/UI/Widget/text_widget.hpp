#pragma once

#include "base_widget_component.hpp"
#include "widget_renderer.hpp"
#include "widget_updater.hpp"

#include "alignment.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/maths/vec2.hpp>
#include <nlc/maths/vec4.hpp>
#include <nlc/meta/basics.hpp>

#include <graphic_engine/ids.hpp>

namespace ge {
struct Actor;
}

struct TextWidgetComponent;

struct TextWidgetCompDescriptor : public BaseWidgetCompDescriptor {
    static inline constexpr f32_4 DefaultColor = { 1.0f, 1.0f, 1.0f, 1.0f };
    static inline constexpr float DefaultFontSize = 20.0f;
    static inline constexpr HorizontalAlignment DefaultHorizontalAlignment =
        HorizontalAlignment::Center;
    static inline constexpr VerticalAlignment DefaultVerticalAlignment = VerticalAlignment::Center;

    nlc::string Text;
    f32_4 const Color = DefaultColor;
    ge::FontId const FontId;
    float const FontSize = DefaultFontSize;
    HorizontalAlignment const HorizontalAlign = DefaultHorizontalAlignment;
    VerticalAlignment const VerticalAlign = DefaultVerticalAlignment;

    TextWidgetCompDescriptor(nlc::allocator & alloc,
                             nlc::string_view const & widget_text,
                             f32_4 const widget_color,
                             ge::FontId const font_id,
                             float widget_font_size,
                             HorizontalAlignment h_align,
                             VerticalAlignment v_align)
        : BaseWidgetCompDescriptor()
        , Text(alloc, widget_text)
        , Color(widget_color)
        , FontId(font_id)
        , FontSize(widget_font_size)
        , HorizontalAlign(h_align)
        , VerticalAlign(v_align) {}

    TextWidgetCompDescriptor(TextWidgetCompDescriptor && rhs)
        : BaseWidgetCompDescriptor()
        , Text(nlc::move(rhs.Text))
        , Color(rhs.Color)
        , FontId(rhs.FontId)
        , FontSize(rhs.FontSize)
        , HorizontalAlign(rhs.HorizontalAlign)
        , VerticalAlign(rhs.VerticalAlign) {}

    virtual ~TextWidgetCompDescriptor() = default;

    auto CreateInstance(nlc::allocator & alloc) const -> nlc::unique_ptr<BaseWidgetComponent> override;
};

struct TextWidgetComponent : public BaseWidgetComponent {
    static usize const LocalTypeHash;
    TextWidgetCompDescriptor const & Descriptor;
    nlc::string Text;
    f32_4 Color = TextWidgetCompDescriptor::DefaultColor;
    float FontSize = TextWidgetCompDescriptor::DefaultFontSize;

    TextWidgetComponent(nlc::allocator & alloc, TextWidgetCompDescriptor const & descriptor);

    TextWidgetComponent(TextWidgetComponent && rhs)
        : BaseWidgetComponent(LocalTypeHash,
                              nlc::move(rhs.LayoutUpdater),
                              nlc::move(rhs.Renderer),
                              nlc::move(rhs.CopyComponentData)
#ifndef NDEBUG
                                  ,
                              nlc::move(rhs.DebugRenderer)
#endif
                                  )
        , Descriptor(nlc::move(rhs.Descriptor))
        , Text(nlc::move(rhs.Text))
        , Color(rhs.Color)
        , FontSize(rhs.FontSize) {
    }

    virtual ~TextWidgetComponent() = default;
};
