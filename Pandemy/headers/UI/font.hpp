#pragma once

#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/meta/basics.hpp>

#include <graphic_engine/ids.hpp>

namespace nlc {
class allocator;
}

struct Font {
    nlc::string Name;
    ge::FontId const Id;

    Font(nlc::allocator & alloc, nlc::string_view const & name, ge::FontId const id)
        : Name(alloc, name)
        , Id(id) {}

    Font(Font && rhs)
        : Name(nlc::move(rhs.Name))
        , Id(rhs.Id) {}

    ~Font() = default;
};
