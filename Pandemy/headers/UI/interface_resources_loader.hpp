#pragma once

#include <nlc/dialect/optional.hpp>

namespace nlc {
class allocator;
class string_stream;
}  // namespace nlc

struct InterfaceResources;

namespace mdf {
auto interface_resources_load(nlc::allocator & allocator, nlc::string_stream & error_stream)
    -> nlc::optional<InterfaceResources>;
}
