#pragma once

#include "UI/Widget/base_widget.hpp"

#include <nlc/dialect/pointer.hpp>
#include <nlc/fundamentals/allocator.hpp>

struct CardDescriptor;

struct TextWidgetComponent;
struct TextureWidgetComponent;

namespace nlc {
class string_stream;
}

class CardWidgetController {
  public:
#ifndef NDEBUG
    static auto Validate(BaseWidgetDescriptor const & descriptor, nlc::string_stream & error_stream)
        -> bool;
#endif

    CardWidgetController(nlc::allocator & alloc,
                         TextWidgetComponent & text_name_comp,
                         TextWidgetComponent & text_attack_comp,
                         TextWidgetComponent & text_defense_comp,
                         TextureWidgetComponent & texture_comp,
                         CardDescriptor const * const descriptor = nullptr);

    auto SetDescriptor(CardDescriptor const & descriptor) -> void;

  private:
    nlc::allocator & Allocator;
    TextWidgetComponent & NameTextComponent;
    TextWidgetComponent & AttackTextComponent;
    TextWidgetComponent & DefenseTextComponent;
    TextureWidgetComponent & TextureComponent;
    CardDescriptor const * Descriptor;
};

auto MakeCardWidgetController(nlc::allocator & alloc, BaseWidget & card_widget)
    -> nlc::unique_ptr<CardWidgetController>;
