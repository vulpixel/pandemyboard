#pragma once

#include "UI/Controllers/menu_page.hpp"
#include "UI/Widget/base_widget.hpp"
#include "UI/menu_data.hpp"
#include "app_status.hpp"

#include <nlc/dialect/buffer.hpp>
#include <nlc/dialect/pair.hpp>
#include <nlc/fundamentals/function.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>

struct ContainerWidgetComponent;
struct GameInfoDescriptor;
struct InterfaceResources;
class BaseController;

class MenuFlowController {
  public:
    struct MenuPageInfo {
        mdf::MenuData MenuDescriptor;
        nlc::function<nlc::unique_ptr<BaseController>(nlc::allocator &,
                                                      GameInfoDescriptor const &,
                                                      InterfaceResources const &,
                                                      BaseWidget &,
                                                      nlc::span<BaseWidgetDescriptor const> const)>
            ControllerInstanciator;

        MenuPageInfo(nlc::allocator & alloc);
        MenuPageInfo(MenuPageInfo && rhs) = default;

        auto operator=(MenuPageInfo && rhs) -> MenuPageInfo & = default;
    };
    using MenuPageDescriptors = nlc::buffer<MenuPageInfo, static_cast<usize>(MenuPage::MAX)>;

    MenuFlowController(nlc::allocator & alloc,
                       ContainerWidgetComponent & root_container,
                       GameInfoDescriptor const & game_info,
                       InterfaceResources const & interface_resources,
                       AppStatus & app_status);
    ~MenuFlowController();

    auto Init(MenuPageDescriptors & menu_pages) -> void;
    auto Update() -> void;

    auto SetDescriptors(MenuPageDescriptors & menu_pages) -> void;
    auto SetMenu(MenuPage page) -> void;

    auto CheckInitialized() -> void;

  private:
    AppStatus & ApplicationStatus;
    GameInfoDescriptor const & GameInfo;
    InterfaceResources const & InterfaceRes;
    nlc::unique_ptr<BaseController> CurrentController = nullptr;

    MenuPageDescriptors MenuDescriptors;

    ContainerWidgetComponent & RootContainer;
    nlc::allocator & Allocator;

#ifndef NDEBUG
    bool bInitialized = false;
#endif
};
