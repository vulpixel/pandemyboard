#pragma once

enum class MenuPage {
    MainMenu = 0,
    TilePlacement,
    CardDisplay,
    Exit,
    MAX = Exit,
};
