#pragma once

#include "UI/Controllers/menu_page.hpp"

#include <nlc/containers/vector.hpp>

#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/pointer.hpp>

#include <nlc/fundamentals/time.hpp>

namespace nlc {
class allocator;
}

class BaseController {
  public:
    BaseController(nlc::allocator & alloc)
        : LastInputTime(nlc::time::now())
        , ActiveControllers(alloc) {}
    BaseController(BaseController && rhs) = default;
    virtual ~BaseController() = default;

    auto operator=(BaseController && rhs) -> BaseController & = default;

    virtual auto ProcessInput() -> void {
        for (auto & controller : ActiveControllers) {
            controller->ProcessInput();
        }
    }
    auto GetTargetPage() const -> nlc::optional<MenuPage> const & { return (TargetPage); }

    auto ShouldDiscardInput() const -> bool {
        return (nlc::time::now() - LastInputTime < InputFrequency);
    }
    auto AcknowInput() -> void { LastInputTime = nlc::time::now(); }

  private:
    static constexpr nlc::time::duration InputFrequency = 200_ms;

    nlc::time::duration LastInputTime;

  protected:
    nlc::optional<MenuPage> TargetPage;
    nlc::vector<nlc::pointer<BaseController>> ActiveControllers;
};
