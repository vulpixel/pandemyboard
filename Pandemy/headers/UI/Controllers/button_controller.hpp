#pragma once

#include "UI/Controllers/base_controller.hpp"

#include "UI/Widget/button_widget.hpp"

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/pair.hpp>
#include <nlc/dialect/pointer.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/function.hpp>

#include <win_input/axis_input.hpp>
#include <win_input/coords2.hpp>
#include <win_input/simple_input.hpp>

struct ButtonWidgetComponent;
struct BaseWidget;

namespace nlc {
class string_stream;
}

class ButtonController : public BaseController {
  private:
    struct ButtonAndAction {
        BaseWidget const & Widget;
        nlc::pointer<ButtonWidgetComponent> Button;
        nlc::function<void()> Action;
    };

  public:
    struct WidgetToFunction {
        BaseWidget const & Widget;
        nlc::function<void()> Action;
    };

#ifndef NDEBUG
    static auto Validate(BaseWidgetDescriptor const & descriptor, nlc::string_stream & error_stream)
        -> bool;
#endif
    static auto Create(nlc::allocator & alloc, nlc::span<WidgetToFunction> actions, Inputs::AxisId input)
        -> ButtonController;

    ButtonController(nlc::allocator & alloc,
                     nlc::span<WidgetToFunction> button_data,
                     Inputs::AxisId up_down_id);
    ButtonController(ButtonController && rhs) = default;
    virtual ~ButtonController();

    auto operator=(ButtonController && rhs) -> ButtonController & = default;

    auto ProcessInput() -> void override;
    auto ProcessMouse() -> void;
    auto ProcessMouseAction(Inputs::Action action) -> void;
    auto ProcessAction(Inputs::Action action) -> void;

  private:
    auto SetCurrentStatus(ButtonWidgetStatus status) -> bool;
    auto GetCurrentWidget() -> BaseWidget const & {
        nlc_assert_msg(CurrentlySelected != nlc::null, "Calling get current on a non-selected button.");
        return (ButtonData[*CurrentlySelected].Widget);
    }
    auto GetCurrentComponent() -> ButtonWidgetComponent & {
        nlc_assert_msg(CurrentlySelected != nlc::null, "Calling get current on a non-selected button.");
        return (*(ButtonData[*CurrentlySelected].Button));
    }
    auto GetCurrentAction() const -> nlc::function<void()> const & {
        nlc_assert_msg(CurrentlySelected != nlc::null, "Calling get current on a non-selected button.");
        return (ButtonData[*CurrentlySelected].Action);
    }

    auto HasSelected() -> bool { return (CurrentlySelected != nlc::null); }

    nlc::vector<ButtonAndAction> ButtonData;

    nlc::optional<usize> CurrentlySelected = nlc::null;

    Inputs::AxisId UpDownId = Inputs::AxisId::invalid();

    WindowCoords2 LastMousePos;
};
