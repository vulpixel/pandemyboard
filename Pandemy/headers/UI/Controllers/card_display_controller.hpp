#pragma once

#include "UI/Controllers/base_controller.hpp"

#include "UI/Controllers/button_controller.hpp"
#include "UI/Controllers/card_widget_controller.hpp"

#include "UI/Widget/base_widget.hpp"

#include "Gameplay/Card/deck_descriptor.hpp"

#include <nlc/containers/vector.hpp>

#include <nlc/dialect/span.hpp>

#include <nlc/fundamentals/unique_ptr.hpp>

#include <win_input/axis_input.hpp>
#include <win_input/input_set.hpp>
#include <win_input/simple_input.hpp>

struct BaseWidget;
struct ContainerWidgetComponent;
struct GameInfoDescriptor;
struct InterfaceResources;

namespace nlc {
class allocator;
class string_stream;
}  // namespace nlc
namespace mdf {
struct MenuData;
}

class CardDisplayController : public BaseController {
  public:
#ifndef NDEBUG
    static auto Validate(mdf::MenuData const & menu_descriptor, nlc::string_stream & error_stream)
        -> bool;
#endif
    static auto CreateBackButtonController(CardDisplayController & controller,
                                           nlc::allocator & alloc,
                                           BaseWidget & back_button_widget) -> ButtonController;
    static auto CreateDeckTabsController(nlc::allocator & alloc,
                                         ContainerWidgetComponent & deck_tabs,
                                         nlc::span<DeckDescriptor const> current_deck,
                                         BaseWidgetDescriptor const & deck_tab_template,
                                         CardDisplayController & controller,
                                         Inputs::AxisId input) -> ButtonController;

    CardDisplayController(nlc::allocator & alloc,
                          nlc::span<BaseWidget> const cards,
                          usize focused_card_index,
                          ContainerWidgetComponent & deck_tabs,
                          nlc::span<DeckDescriptor const> decks,
                          BaseWidget & back_button);
    CardDisplayController(CardDisplayController && rhs) = delete;
    ~CardDisplayController();

    virtual auto ProcessInput() -> void final;

    auto GoToMainMenu() -> void;

  private:
    auto SwitchDeckIndex(usize index) -> void;
    auto SetCurrentCard(usize index) -> void;

    auto ProcessAction(Inputs::Action action) -> void;
    auto ProcessMouseAction(Inputs::Action action) -> void;

  private:
    // inputs
    Inputs::InputSetId CardDisplaySet = Inputs::InputSetId::invalid();
    Inputs::AxisId CardLeftRightId = Inputs::AxisId::invalid();
    Inputs::AxisId DeckLeftRightId = Inputs::AxisId::invalid();
    Inputs::SimpleInputId ActionId = Inputs::SimpleInputId::invalid();
    Inputs::SimpleInputId ClickId = Inputs::SimpleInputId::invalid();

    // deck
    nlc::span<DeckDescriptor const> DeckDesc;
    usize CurrentCardId = 0;

    usize CurrentDeckId = 0;

    // buttons
    BaseWidget & BackButtonWidget;
    ButtonController Buttons;

    // Tabs
    BaseWidgetDescriptor const & DeckTabTemplate;
    ContainerWidgetComponent & DeckTabs;
    ButtonController DeckTabsButtons;

    // card
    nlc::span<BaseWidget> const AllCards;
    nlc::vector<nlc::unique_ptr<CardWidgetController>> CardControllers;
    usize FocusedCardIndex = 0;

    // alloc
    nlc::allocator & Allocator;
};

auto MakeCardDisplayController(nlc::allocator & alloc,
                               GameInfoDescriptor const & game_info,
                               InterfaceResources const & interface_resources,
                               BaseWidget & menu_widget,
                               nlc::span<BaseWidgetDescriptor const> const dependencies)
    -> nlc::unique_ptr<CardDisplayController>;
