#pragma once

#include "UI/Controllers/base_controller.hpp"
#include "UI/Widget/base_widget.hpp"

#include <nlc/containers/vector.hpp>

#include <win_input/simple_input.hpp>

struct BaseWidgetDescriptor;
struct BaseWidget;
struct ContainerWidgetComponent;

namespace nlc {
class string_stream;
class allocator;
}  // namespace nlc

class DragDropController : public BaseController {
  public:
    DragDropController(nlc::allocator & alloc,
                       ContainerWidgetComponent & dummy_container,
                       nlc::vector<BaseWidget> const & draggables,
                       nlc::function<BaseWidget(BaseWidget const &)> && OnDrag,
                       nlc::function<void(BaseWidget &&, f32_2)> && OnDrop);
    virtual ~DragDropController();

#ifndef NDEBUG
    static auto Validate() -> bool { return (true); }
#endif
    static auto Create(nlc::allocator & alloc,
                       ContainerWidgetComponent & dummy_container,
                       nlc::vector<BaseWidget> const & draggables,
                       nlc::function<BaseWidget(BaseWidget const &)> && OnDrag,
                       nlc::function<void(BaseWidget &&, f32_2)> && OnDrop) -> DragDropController {
        return (
            DragDropController { alloc, dummy_container, draggables, nlc_fwd(OnDrag), nlc_fwd(OnDrop) });
    }

    auto ProcessInput() -> void override;
    auto ProcessMouse() -> void;
    auto ProcessMouseAction(Inputs::Action action) -> void;

  private:
    nlc::vector<BaseWidget> const & DraggableWidgets;

    ContainerWidgetComponent & DummyContainer;
    BaseWidget * Dummy;

    nlc::function<BaseWidget(BaseWidget const &)> OnDragCallback;
    nlc::function<void(BaseWidget &&, f32_2)> OnDropCallback;
};
