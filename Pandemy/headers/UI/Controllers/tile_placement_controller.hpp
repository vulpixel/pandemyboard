#pragma once

#include "Gameplay/Tiles/tile_descriptor.hpp"
#include "UI/Controllers/base_controller.hpp"
#include "UI/Controllers/button_controller.hpp"
#include "UI/Controllers/drag_drop_controller.hpp"

#include <nlc/fundamentals/unique_ptr.hpp>

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/pair.hpp>

#include <win_input/input_set.hpp>
#include <win_input/simple_input.hpp>

struct GameInfoDescriptor;
struct TileDescriptor;

struct BaseWidget;
struct BaseWidgetDescriptor;
struct ContainerWidgetComponent;
struct GridWidgetComponent;
struct InterfaceResources;

namespace nlc {
class allocator;
class string_stream;
}  // namespace nlc

namespace mdf {
struct MenuData;
}

class TilePlacementController : public BaseController {
  private:
    using WidgetAndTile = nlc::pair<BaseWidget::ID, TileInstance>;

    struct DragInfo {
        usize const BagIndex;
        BaseWidget::ID const WidgetId;
        TileInstance Tile;
    };

  public:
#ifndef NDEBUG
    static auto Validate(mdf::MenuData const & menu_descriptor, nlc::string_stream & error_stream)
        -> bool;
#endif
    static auto CreateBackButtonController(TilePlacementController & controller,
                                           nlc::allocator & alloc,
                                           BaseWidget & back_button) -> ButtonController;

    TilePlacementController(nlc::allocator & alloc,
                            InterfaceResources const & interface_resources,
                            nlc::span<TileDescriptor const> tiles,
                            BaseWidget & back_button,
                            BaseWidget & board_widget,
                            GridWidgetComponent & tile_bag,
                            BaseWidgetDescriptor const & tile_template,
                            BaseWidgetDescriptor const & dragged_tile_template);
    TilePlacementController(TilePlacementController && rhs) = delete;
    ~TilePlacementController();

    virtual auto ProcessInput() -> void final;
    auto ProcessMouseAction(Inputs::Action action) -> void;
    auto GoToMainMenu() -> void;

    auto BeginDrag(BaseWidget const & target) -> BaseWidget;
    auto EndDrag(BaseWidget && dropped, f32_2 position) -> void;

  private:
    auto FillTileBag(nlc::span<TileDescriptor const> tiles) -> void;

  private:
    Inputs::InputSetId TilePlacementSet = Inputs::InputSetId::invalid();
    Inputs::SimpleInputId ClickId = Inputs::SimpleInputId::invalid();
    Inputs::AxisId RotationId;
    nlc::allocator & Alloc;
    InterfaceResources const & InterfaceRes;
    nlc::vector<WidgetAndTile> WidgetToTile;
    nlc::optional<DragInfo> CurrentDrag;

    BaseWidget & BoardWidget;
    ContainerWidgetComponent & Board;
    GridWidgetComponent & TileBag;
    BaseWidgetDescriptor const & TileTemplate;
    BaseWidgetDescriptor const & DraggedTileTemplate;

    ButtonController BackButtonController;

    DragDropController DragController;
};

auto MakeTilePlacementController(nlc::allocator & alloc,
                                 GameInfoDescriptor const & game_info,
                                 InterfaceResources const & interface_resources,
                                 BaseWidget & menu_widget,
                                 nlc::span<BaseWidgetDescriptor const> const dependencies)
    -> nlc::unique_ptr<TilePlacementController>;
