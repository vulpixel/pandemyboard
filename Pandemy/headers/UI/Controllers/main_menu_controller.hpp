#pragma once

#include "UI/Controllers/base_controller.hpp"

#include "UI/Controllers/button_controller.hpp"

#include "UI/Widget/base_widget.hpp"

#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>

#include <win_input/axis_input.hpp>
#include <win_input/input_set.hpp>
#include <win_input/simple_input.hpp>

struct BaseWidget;
struct GameInfoDescriptor;
struct InterfaceResources;

namespace {
class string_stream;
}

namespace mdf {
struct MenuData;
}

class MainMenuController : public BaseController {
  public:
#ifndef NDEBUG
    static auto Validate(mdf::MenuData const & menu_descriptor, nlc::string_stream & error_stream)
        -> bool;
#endif

    static auto CreateButtonController(MainMenuController & controller,
                                       nlc::allocator & alloc,
                                       Inputs::AxisId input) -> ButtonController;
    MainMenuController(nlc::allocator & alloc,
                       BaseWidget & start_widget,
                       BaseWidget & card_display_widget,
                       BaseWidget & options_widget,
                       BaseWidget & exit_widget);
    ~MainMenuController();

    virtual auto ProcessInput() -> void final;

    auto GoToNext() -> void;
    auto GoToCardDisplay() -> void;
    auto GoToExit() -> void;

  private:
    auto ProcessAction(Inputs::Action action) -> void;
    auto ProcessMouseAction(Inputs::Action action) -> void;

  private:
    // inputs
    Inputs::InputSetId MainMenuSet = Inputs::InputSetId::invalid();
    Inputs::AxisId UpDownId = Inputs::AxisId::invalid();
    Inputs::SimpleInputId ActionId = Inputs::SimpleInputId::invalid();
    Inputs::SimpleInputId ClickId = Inputs::SimpleInputId::invalid();

    // buttons
    BaseWidget & StartWidget;
    BaseWidget & CardDisplayWidget;
    BaseWidget & OptionsWidget;
    BaseWidget & ExitWidget;
    ButtonController Buttons;
};

auto MakeMainMenuController(nlc::allocator & alloc,
                            GameInfoDescriptor const & game_info,
                            InterfaceResources const & interface_resources,
                            BaseWidget & menu_widget,
                            nlc::span<BaseWidgetDescriptor const> const dependencies)
    -> nlc::unique_ptr<MainMenuController>;
