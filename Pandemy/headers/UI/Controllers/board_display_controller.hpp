#pragma once

#include <Gameplay/Board/board.hpp>

class BoardDisplayController {
  public:
    BoardDisplayController(nlc::allocator & alloc, u8_2 size, BaseWidget & board_widget);

    auto ExtractBoard() -> Board;

    auto PlaceTile(nlc::pair<BaseWidget, TileInstance> graphic_tile, f32_2 const screen_position)
        -> bool;

  private:
    auto FindPositionForTile(f32_2 const screen_position) -> u8_2;

  private:
    Board DungeonBoard;

    BaseWidget & BoardWidget;
    ContainerWidgetComponent & BoardContainer;
};
