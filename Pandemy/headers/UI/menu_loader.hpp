#pragma once

#include "Widget/base_widget.hpp"

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>

namespace nlc {
class allocator;
class string_stream;
}  // namespace nlc

struct InterfaceResources;

namespace mdf {
struct MenuData;
auto menu_loader(nlc::allocator & alloc,
                 nlc::string_view const filename,
                 nlc::string_view const root_name,
                 nlc::span<nlc::string_view const> const object_names,
                 InterfaceResources const & interface_resources,
                 nlc::string_stream & error_stream) -> nlc::optional<MenuData>;
}  // namespace mdf
