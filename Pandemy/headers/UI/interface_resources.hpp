#pragma once

#include <nlc/containers/hashmap.hpp>
#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/buffer.hpp>
#include <nlc/maths/units/angle_units.hpp>
#include <nlc/meta/basics.hpp>

#include "Gameplay/Tiles/tile_enum.hpp"
#include "Graphic/texture_metadata.hpp"
#include "UI/font.hpp"

struct InterfaceResources {
    struct TileTextureAndRotation {
        Graphic::TextureView Texture;
        nlc::radians Rotation;
    };

    nlc::vector<Font> Fonts;
    usize DefaultFontIndex;

    using TileTextureCache = nlc::vector<nlc::pair<nlc::hash_result, Graphic::TextureMetadata>>;
    TileTextureCache TileTextures;
    using TileTextureViews =
        nlc::buffer<TileTextureAndRotation, static_cast<u8>(TileOpening::All) + 1>;
    TileTextureViews OpeningToTextures;

    InterfaceResources(nlc::vector<Font> && fonts,
                       usize default_font_index,
                       TileTextureCache && tile_cache,
                       TileTextureViews && tile_textures)
        : Fonts(nlc::move(fonts))
        , DefaultFontIndex(default_font_index)
        , TileTextures(nlc::move(tile_cache))
        , OpeningToTextures(nlc::move(tile_textures)) {}

    InterfaceResources(InterfaceResources && rhs)
        : Fonts(nlc::move(rhs.Fonts))
        , DefaultFontIndex(rhs.DefaultFontIndex)
        , TileTextures(nlc::move(rhs.TileTextures))
        , OpeningToTextures(nlc::move(rhs.OpeningToTextures)) {}

    ~InterfaceResources() = default;
};
