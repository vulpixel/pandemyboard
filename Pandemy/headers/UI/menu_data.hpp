#pragma once

#include "Widget/base_widget.hpp"

#include <nlc/containers/vector.hpp>

namespace mdf {
struct MenuData {
    BaseWidgetDescriptor RootDescriptor;
    nlc::vector<BaseWidgetDescriptor> Dependencies;
};
}  // namespace mdf
