#pragma once

#include "Widget/base_widget.hpp"

#include <nlc/dialect/optional.hpp>
#include <mdf/types.hpp>

namespace nlc {
class allocator;
class string_stream;
}  // namespace nlc

struct InterfaceResources;

namespace mdf {
auto load_widget(mdf::Rhs const & widget_def,
                 nlc::allocator & allocator,
                 InterfaceResources const & interface_resources,
                 nlc::string_stream & error_stream) -> nlc::optional<BaseWidgetDescriptor>;
}  // namespace mdf
