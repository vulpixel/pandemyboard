#pragma once

enum class AppStatus { OK = 0, Restart, Exit, Error };
