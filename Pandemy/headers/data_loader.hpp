#pragma once

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/optional.hpp>

#include <mdf/types.hpp>

#include "headers/Gameplay/Card/deck_descriptor.hpp"

namespace nlc {
class allocator;
class string_stream;
}  // namespace nlc

struct TileDescriptor;

namespace mdf {

auto load_card(mdf::Object const & card_def,
               nlc::allocator & allocator,
               nlc::string_stream & error_stream) -> nlc::optional<CardDescriptor>;

auto load_deck_array(nlc::allocator & alloc,
                     mdf::Object const & root,
                     nlc::string_view const deck_name,
                     nlc::string_stream & error_stream) -> nlc::optional<nlc::vector<DeckDescriptor>>;

auto load_tile_bag(mdf::Rhs const & tile_bag_def,
                   nlc::allocator & allocator,
                   nlc::string_stream & error_stream) -> nlc::optional<nlc::vector<TileDescriptor>>;

}  // namespace mdf
