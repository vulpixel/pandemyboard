#pragma once

#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/string_view.hpp>

#include <nlc/maths/vec2.hpp>

#include <graphic_engine/ids.hpp>

namespace nlc {
class allocator;
class string_stream;
}  // namespace nlc

namespace Graphic {
struct TextureMetadata {
    ge::TextureId TextureId = ge::TextureId::invalid();
    ge::AnimationFrameId AnimId = ge::AnimationFrameId::invalid();

    f32_2 TextureSize;
    float AspectRatio;

    TextureMetadata(nlc::allocator & alloc, nlc::string_view path, nlc::string_stream & error_stream);
    TextureMetadata(TextureMetadata && rhs)
        : TextureId(rhs.TextureId)
        , AnimId(rhs.AnimId)
        , TextureSize(rhs.TextureSize)
        , AspectRatio(rhs.AspectRatio) {
        rhs.TextureId = ge::TextureId::invalid();
        rhs.AnimId = ge::AnimationFrameId::invalid();
    }
    ~TextureMetadata();

    auto IsValid() const -> bool { return (TextureId.is_valid() && AnimId.is_valid()); }
};

struct TextureView {
    ge::AnimationFrameId AnimId = ge::AnimationFrameId::invalid();

    f32_2 TextureSize;
    float AspectRatio;

    TextureView() = delete;
    TextureView(TextureMetadata const & metadata)
        : AnimId(metadata.AnimId)
        , TextureSize(metadata.TextureSize)
        , AspectRatio(metadata.AspectRatio) {}
    TextureView(nlc::optional<TextureMetadata> const & metadata)
        : AnimId { (metadata != nlc::null) ? metadata->AnimId : ge::AnimationFrameId::invalid() }
        , TextureSize { (metadata != nlc::null) ? metadata->TextureSize : f32_2 {} }
        , AspectRatio { (metadata != nlc::null) ? metadata->AspectRatio : 0.0f } {}

    TextureView(TextureView const & rhs) = default;

    auto IsValid() const -> bool { return (AnimId.is_valid()); }

    auto operator=(TextureView const & rhs) -> TextureView & {
        new (this) TextureView(rhs);
        return (*this);
    }
    auto operator=(TextureMetadata const & rhs) -> TextureView & {
        new (this) TextureView(rhs);
        return (*this);
    }
};
}  // namespace Graphic
